import React, { useEffect } from 'react';
// import { Platform } from 'react-native';
import messaging from '@react-native-firebase/messaging';
// import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification from 'react-native-push-notification';
// import firebase from '@react-native-firebase/app'

const NotifService = () => {
    useEffect(() => {
        requestPermission()
        createChannel()
        PushNotification.getChannels(function (channel_ids) {
            console.log({ channel_ids }); // ['channel_id_1']
        });
        messaging().onMessage(async remoteMessage => {
            console.log({ remoteMessage })
        });

        messaging().onNotificationOpenedApp(remoteMessage => {
            console.log({ remoteMessage });
        });

        messaging()
            .getInitialNotification()
            .then(remoteMessage => {
                if (remoteMessage) {
                    console.log(
                        'Notification caused app to open from quit state:',
                        remoteMessage.notification,
                    );
                }
            });

    }, []);

    const createChannel = () => {
        PushNotification.createChannel(
            {
                channelId: "padiraharja-notif", // (required)
                channelName: "Transaction", // (required)
                channelDescription: "Notification for transaction", // (optional) default: undefined.
                soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
                importance: 4, // (optional) default: 4. Int value of the Android notification importance
                vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
            },
            (created) => console.log(`createChannel returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
        );
    }

    const requestPermission = async () => {

        await messaging().requestPermission({
            alert: true,
            announcement: false,
            badge: true,
            carPlay: true,
            provisional: false,
            sound: true,
        });
    }

    return null
}

export default NotifService;