import Clipboard from '@react-native-community/clipboard';
import Toast from 'react-native-simple-toast';

export const autoCurency = (val) => {
    if (val == 0) {
        return 'Rp. ' + val
    }
    if (val) {
        const num = parseInt(val).toFixed(0)
        const rp = new Intl.NumberFormat('id-ID', {
        }).format(num)

        return 'Rp. ' + rp
    }
    return
}

export const deCurency = (num) => {
    return (
        parseInt(num.toString().replace(/[^0-9$]/g, ''))
    )
}

export const capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

export const saveToClipboard = async (v) => {
    await Clipboard.setString(v.toString())
    Toast.show(`${v} Berhasil disalin`, Toast.LONG)
}

export const autoCorectPhone = (v) => {
    let phone = v.replace(/(\+62|-| )/g, '')
    if (phone.length > 0 && phone[0] != 0) {
        return `0${phone}`
    }
    return phone
}

export const getBankLogo = (v) => {
    switch (v) {
        case 'BCA':
            return require('_assets/icons/bca.png')
        case 'BRI':
            return require('_assets/icons/bri.png')
        case 'BNI':
            return require('_assets/icons/bni.png')
        case 'BNI Syariah':
            return require('_assets/icons/bni-syariah.png')
        case 'MANDIRI':
            return require('_assets/icons/mandiri.png')
        default:
            return null
    }
}

export const getProviderId = (data, key) => {
    const provider = data.filter(v => v.name.toUpperCase() == key.toUpperCase())
    if (provider.length > 0) {
        return provider[0].id
    }
    return null
}

export const autoProvider = (phone) => {
    if (phone.length >= 4) {
        phone = phone.slice(0, 4);
        if (
            phone == '0811' ||
            phone == '0812' ||
            phone == '0813' ||
            phone == '0821' ||
            phone == '0822' ||
            phone == '0823' ||
            phone == '0852' ||
            phone == '0853'
        ) {
            return { name: 'TELKOMSEL', logo: require('_assets/icons/telkomsel.png') }
        }
        else if (
            phone == '0851'
        ) {
            return { name: 'BY.U', logo: require('_assets/icons/telkomsel.png') }
        }
        else if (
            phone == '0855' ||
            phone == '0856' ||
            phone == '0857' ||
            phone == '0858' ||
            phone == '0814' ||
            phone == '0815' ||
            phone == '0816'
        ) {
            return { name: 'INDOSAT', logo: require('_assets/icons/indosat.png') }

        }
        else if (
            phone == '0838' ||
            phone == '0831' ||
            phone == '0832' ||
            phone == '0833' ||
            phone == '0817' ||
            phone == '0818' ||
            phone == '0819' ||
            phone == '0859' ||
            phone == '0877' ||
            phone == '0878'
        ) {
            return { name: 'XL', logo: require('_assets/icons/xl.png') }
        }
        else if (
            phone == '0895' ||
            phone == '0896' ||
            phone == '0897' ||
            phone == '0898' ||
            phone == '0899'
        ) {
            return { name: 'THREE', logo: require('_assets/icons/three.png') }

        }
        else if (
            phone == '0881' ||
            phone == '0882' ||
            phone == '0883' ||
            phone == '0884' ||
            phone == '0885' ||
            phone == '0886' ||
            phone == '0887' ||
            phone == '0888' ||
            phone == '0889'
        ) {
            return { name: 'SMARTFREN', logo: require('_assets/icons/smartfren.png') }

        }
        else {
            return {}
        }
    }
}

export const autoProviderData = (phone) => {
    if (phone.length >= 4) {
        phone = phone.slice(0, 4);
        if (
            phone == '0811' ||
            phone == '0812' ||
            phone == '0813' ||
            phone == '0821' ||
            phone == '0822' ||
            phone == '0823' ||
            phone == '0852' ||
            phone == '0853'
        ) {
            return { name: 'TELKOMSEL', logo: require('_assets/icons/telkomsel.png') }
        }
        else if (
            phone == '0855' ||
            phone == '0856' ||
            phone == '0857' ||
            phone == '0858' ||
            phone == '0814' ||
            phone == '0815' ||
            phone == '0816'
        ) {
            return { name: 'INDOSAT', logo: require('_assets/icons/indosat.png') }

        }
        else if (
            phone == '0817' ||
            phone == '0818' ||
            phone == '0819' ||
            phone == '0859' ||
            phone == '0877' ||
            phone == '0878'
        ) {
            return { name: 'XL', logo: require('_assets/icons/xl.png') }
        }
        else if (
            phone == '0838' ||
            phone == '0831' ||
            phone == '0832' ||
            phone == '0833'
        ) {
            return { name: 'AXIS', logo: require('_assets/icons/xl.png') }
        }
        else if (
            phone == '0895' ||
            phone == '0896' ||
            phone == '0897' ||
            phone == '0898' ||
            phone == '0899'
        ) {
            return { name: 'THREE', logo: require('_assets/icons/three.png') }

        }
        else if (
            phone == '0881' ||
            phone == '0882' ||
            phone == '0883' ||
            phone == '0884' ||
            phone == '0885' ||
            phone == '0886' ||
            phone == '0887' ||
            phone == '0888' ||
            phone == '0889'
        ) {
            return { name: 'SMARTFREN', logo: require('_assets/icons/smartfren.png') }

        }
        else {
            return {}
        }
    }
}