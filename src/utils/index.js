import { autoCurency, deCurency, autoProvider, autoProviderData, capitalizeFirstLetter, saveToClipboard, autoCorectPhone, getBankLogo, getProviderId } from './helper';

export {
    deCurency,
    autoCurency,
    autoProvider,
    autoProviderData,
    saveToClipboard,
    autoCorectPhone,
    getBankLogo,
    getProviderId,
    capitalizeFirstLetter
}