import React, { useState, useRef } from 'react';
import { Text, StatusBar, View, FlatList, StyleSheet, TouchableHighlight, Dimensions } from 'react-native';
import { Divider } from '_atoms';
import { color, style } from '_styles';
import { connect } from 'react-redux';
import { TopUpBalance, TopUpHistory } from '_organisms';
import ViewPager from '@react-native-community/viewpager';

const TopUp = (props) => {

    const menuRef = useRef(null)
    const pagerRef = useRef(null)

    const [selected, setSelected] = useState(0)

    const scrollTo = (index) => {
        pagerRef.current.setPage(index)
        menuRef.current.scrollToIndex({ index })
    }

    const renderMenu = ({ item, index }) => {
        const isSelected = selected == index
        return (
            <View>
                <TouchableHighlight onPress={() => scrollTo(index)} underlayColor='transparent' style={[styles.menu, { backgroundColor: isSelected ? color.primary : '#fff' }]}>
                    <Text style={{ color: isSelected ? '#fff' : color.primary, fontWeight: 'bold' }}>{item.name}</Text>
                </TouchableHighlight>
            </View>
        )
    }

    return (
        <View style={style.container}>
            <StatusBar barStyle='dark-content' />
            <View style={{ flex: 0 }}>
                <FlatList
                    data={menu}
                    ref={menuRef}
                    keyExtractor={item => item.name}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    renderItem={renderMenu} />
            </View>
            <Divider />
            <ViewPager ref={pagerRef} onPageScroll={v => setSelected(v.nativeEvent.position)} style={styles.viewPager} initialPage={parseInt(selected)} >
                <TopUpBalance key='1' navigate={({ data, paymentMethod }) => props.navigation.navigate('TopUpInvoice', { data, paymentMethod })} />
                <TopUpHistory key='2' />
            </ViewPager>
        </View>
    )
}

const { width } = Dimensions.get('screen')

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    history: {
        alignItems: 'center'
    },
    title: {
        fontWeight: 'bold',
        color: color.g800,
        marginBottom: 10
    },
    date: {
        fontSize: 12,
        color: color.g500
    },
    price: {
        fontWeight: 'bold',
        color: color.secondary
    },
    status: {
        fontWeight: 'bold',
        marginBottom: 10,
    },
    viewPager: {
        flex: 1,
        flexDirection: 'row'
    },
    menu: {
        margin: 10,
        padding: 7,
        borderRadius: 20,
        width: width / 2 - 20,
        alignItems: 'center',
        borderWidth: 2,
        borderColor: color.primary
    }
})

const menu = [
    {
        name: 'Isi Saldo',
    },
    {
        name: 'Riwayat',
    }
]

const mapStateToProps = state => {
    return {
        user: state.user.data
    }
}

export default connect(mapStateToProps)(TopUp);