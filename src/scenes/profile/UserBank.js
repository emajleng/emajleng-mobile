import React, { useEffect, useState, useLayoutEffect } from 'react';
import { StyleSheet, Text, Dimensions, View, TouchableOpacity, ScrollView, Alert, FlatList, Image } from 'react-native';
import { Button, HeaderTransparent, Input, ButtonText } from '_atoms'
import { color, style } from '_styles';
import { autoCurency } from '_utils';
import Icon from 'react-native-vector-icons/Ionicons'
import axios from 'axios';

const UserBank = (props) => {

    const [banks, setBanks] = useState([])

    useLayoutEffect(() => {
        props.navigation.setOptions({
            headerRight: () => (
                <Icon name='add' size={30} color='#fff' style={{ paddingRight: 10 }} onPress={() => props.navigation.navigate('AddUserBank')} />
            ),
        });
    }, [props.navigation]);

    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', () => getProduct())
        return unsubscribe;
    }, [props.navigation])

    const getProduct = () => {
        axios.get(`/user_bank`).then(res => {
            console.log({ res })
            setBanks(res.data.data)
        })
    }

    const deleteProduct = (id) => {
        axios.delete(`/user_bank/${id}`)
            .then(res => {
                alert('Bank dihapus')
                getProduct()
            })
    }

    const renderProduct = ({ item }) => {
        return (
            <View style={[style.shadow, { marginVertical: 10, margin: 5, borderRadius: 5, paddingBottom: 10 }]}>
                <TouchableOpacity style={[styles.productList]}>
                    <View style={{ flex: 1 }}>
                        <Text style={styles.productNameText}>{item.account_name}</Text>
                    </View>
                    <View style={{ alignItems: 'flex-end', flex: 1 }}>
                        <Text style={styles.productNameText}>{item.bank_name}</Text>
                        <Text style={styles.productNameText}>{item.account_number}</Text>
                    </View>
                </TouchableOpacity>
                <ButtonText title={"Hapus Bank"} color={color.failed} onPress={() => deleteProduct(item.id)} />
            </View>
        )
    }

    return (
        <ScrollView style={style.container}>
            {/* <HeaderTransparent title='Daftar Produk' iconName='md-add' add onPress={() => props.navigation.navigate('AddProduct')} /> */}
            <View style={style.body}>
                <FlatList
                    data={banks}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={renderProduct}
                />
            </View>
        </ScrollView>
    )
}

const { width, height } = Dimensions.get('screen')
const styles = StyleSheet.create({
    header: {
        backgroundColor: color.primary,
        height: Dimensions.get('screen').height / 8,
        justifyContent: 'center',
        paddingTop: Platform.OS == 'ios' && isIphoneX() ? 20 : 0
    },
    productList: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10
    },
    productNameText: {
        color: color.g900,
        fontWeight: 'bold'
    },
    tglText: {
        fontSize: 10,
        color: color.g600,
        marginBottom: 2
    },
    priceText: {
        color: color.secondary,
        marginBottom: 2
    },
    text: {
        fontSize: 12,
        color: color.g600
    }
})

export default UserBank;