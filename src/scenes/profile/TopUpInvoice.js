import React from 'react';
import { Text, View, Image, SafeAreaView, ScrollView, StyleSheet, Linking } from 'react-native';
import { Divider, Button, ButtonText } from '_atoms'
import { autoCurency, saveToClipboard, getBankLogo } from '_utils';
import { useBackHandler } from '@react-native-community/hooks'
import { color } from '_styles';
import { connect, batch } from 'react-redux';
import moment from 'moment';
import 'moment/min/locales';


const TopUpInvoice = (props) => {

    useBackHandler(() => {
        props.navigation.popToTop()
        return true
    })

    const comfirmPayment = () => {
        const line1 = 'Konfirmasi pembayaran TopUp AidiPay. '
        const line2 = `Pembayaran melalui: *${paymentMethod.bank_name}* sebesar *${autoCurency(data.amount + data.unique_code)}*`
        Linking.openURL(`whatsapp://send?text=${line1}${line2}&phone=${props.information.customer_service}`)
    }

    const { data, paymentMethod } = props.route.params
    const amount = data.amount + data.unique_code
    const amountLength = amount.toString().length
    console.log({ data })
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ScrollView style={{ backgroundColor: 'white', padding: 10 }}>
                <View style={styles.infoBox}>
                    <Text style={styles.infoText}>Pastikan untuk tidak menginformasikan bukti dan data pembayaran <Text style={{ fontWeight: 'bold' }}>kepada pihak manapun</Text> kecuali AidiPay</Text>
                </View>
                <View style={{ paddingVertical: 10 }}>
                    <Text style={styles.title}>SEGERA LAKUKAN PEMBAYARAN SEBELUM</Text>
                    <Text style={styles.time}>{moment(data.payment_expired).locale('id').format('dddd, DD MMMM YYYY, HH:mm')}</Text>
                </View>
                <Divider />
                <View style={{ paddingVertical: 10 }}>
                    <Text style={styles.title}>Transfer ke nomor rekening</Text>
                    <View style={styles.billNumber}>
                        <Image style={{ width: 100, height: 50 }} source={getBankLogo(paymentMethod.bank_name)} resizeMode='contain' />
                        <Text style={styles.value}>{paymentMethod.account_number}</Text>
                    </View>
                    <Text style={{ color: color.g700, marginBottom: 5 }}>a/n {paymentMethod.account_name}</Text>
                    <ButtonText title='Salin Nomor Rekening' textSize={12} onPress={() => saveToClipboard(paymentMethod.account_number)} />
                </View>
                <Divider />
                <View style={{ paddingVertical: 10 }}>
                    <Text style={[styles.title, { fontWeight: 'bold' }]}>Jumlah yg harus dibayar</Text>
                    <Text style={styles.amount}>{autoCurency(amount.toString().slice(0, amountLength - 3))}<Text style={{ color: color.secondary }}>.{amount.toString().slice(amountLength - 3, amountLength)}</Text></Text>
                    <View style={styles.arrow} />
                </View>
                <View style={styles.toolTip}>
                    <Text style={[styles.toolTipText, { fontWeight: 'bold' }]}>Transfer hingga 3 digit terakhir</Text>
                    <Text style={styles.toolTipText}>Perbedaan angka akan menghambat verifikasi.</Text>
                </View>
                <View>
                    <ButtonText title='Salin Jumlah' textSize={12} onPress={() => saveToClipboard(amount)} />
                </View>
            </ScrollView>
            <View style={{ backgroundColor: '#fff' }}>
                {/* {data.status == 'PENDING' && <Button style={{ margin: 10 }} outline title='Konfirmasi Pembayaran' onPress={comfirmPayment} />} */}
                <Button style={{ margin: 10 }} title='Selesai' onPress={() => props.navigation.popToTop()} />
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    infoBox: {
        borderWidth: 2,
        margin: 10,
        padding: 10,
        alignItems: 'center',
        borderColor: color.p100,
        backgroundColor: color.p50
    },
    infoText: {
        fontSize: 12
    },
    title: {
        fontSize: 14,
        color: color.g900,
    },
    value: {
        fontSize: 16,
        fontWeight: 'bold',
        color: color.g900
    },
    billNumber: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    time: {
        fontSize: 18,
        color: color.g700,
        fontWeight: 'bold',
        marginTop: 5
    },
    amount: {
        fontSize: 18,
        fontWeight: 'bold',
        color: color.g700
    },
    toolTip: {
        padding: 10,
        marginTop: -20,
        marginBottom: 10,
        backgroundColor: color.g700,
        borderRadius: 5
    },
    arrow: {
        width: 0,
        height: 0,
        borderLeftWidth: 15,
        borderRightWidth: 15,
        borderBottomWidth: 25,
        borderStyle: 'solid',
        backgroundColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: color.g700,
    },
    toolTipText: {
        color: '#fff'
    }
})

const mapStateToProps = state => {
    return {
        user: state.user.data,
        information: state.user.information,
    }
}

export default connect(mapStateToProps)(TopUpInvoice);