import React, { useState } from 'react';
import { StyleSheet, Dimensions, View, ScrollView, Alert } from 'react-native';
import { Button, HeaderTransparent, Input } from '_atoms'
import { color, style } from '_styles';
import axios from 'axios';

const AddUserBank = (props) => {

    const [data, setData] = useState({})
    const [isLoading, setisLoading] = useState(false)



    const addUserBank = () => {
        setisLoading(true)
        axios.post(`/user_bank`, data)
            .finally(() => setisLoading(false))
            .then(res => {
                console.log({ res })
                Alert.alert('Suksess', 'Tambah Bank Berhasil', [
                    {
                        text: 'OK',
                        onPress: () => props.navigation.goBack()
                    }
                ])
            })
    }

    return (
        <>
            <ScrollView style={[style.container, { padding: 10 }]}>
                <View style={[style.body, { paddingTop: 20 }]}>
                    <Input label="Nama Pemilik Rekening" onChangeText={(account_name => setData(v => ({ ...v, account_name })))} />
                    <Input label="Nama Bank" placeholder='Contoh: BRI' onChangeText={(bank_name => setData(v => ({ ...v, bank_name })))} />
                    <Input label="Nomor Rekening" keyboardType='numeric' onChangeText={(account_number => setData(v => ({ ...v, account_number })))} />
                    <Button title='Tambah' onPress={() => addUserBank()} loading={isLoading} disable={isLoading} />
                </View>
            </ScrollView>
        </>
    )
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: color.primary,
        height: Dimensions.get('screen').height / 8,
        justifyContent: 'center',
        paddingTop: Platform.OS == 'ios' && isIphoneX() ? 20 : 0
    },
    body: {
        paddingTop: 10
    }
})

export default AddUserBank;