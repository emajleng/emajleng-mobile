import React, { useEffect, useState } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { View, Dimensions, StyleSheet, ScrollView, StatusBar, Text, TouchableOpacity } from 'react-native';
import { Button, Card, LineVertical, HeaderProfile } from '_atoms';
import { CardProfile, ProfileMenu, ProfilMenuMerchant } from '_molecules';
import { setLoading, saveToken, setUser } from '_states/actions/user';
import { connect, batch } from 'react-redux';
import axios from 'axios';
import { color } from '_styles';
import { UserProfiles, MerchantProfiles } from '_organisms';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { TabActions } from '@react-navigation/native';
import { Header } from 'react-native/Libraries/NewAppScreen';

const Tab = createMaterialTopTabNavigator();

const profile = (props) => {

    const merchant = props.merchant.name
    const [toko, setToko] = useState({})
    console.log({ toko })

    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', () => getMerchant())
        return unsubscribe;
    }, [props.navigation])

    const getMerchant = () => {
        axios.get(`/user/merchant`)
            .then(res => {
                console.log({ res })
                setToko(res.data.data)
            }).catch(err => {
                console.log({ err })
                setToko(null)
            })
    }

    const onLogout = () => {
        AsyncStorage.clear().then(() => {
            batch(() => {
                props.dispatch(saveToken(null))
                props.dispatch(setLoading(true))
            })
        })
        axios.post(`/auth/logout`)
    }

    const addProduct = () => {
        axios.post(``, { data }).finally(() => {

        })
    }
    return (
        <>
            <HeaderProfile title='Konfigurasi Profil' colorText={color.g700} color={color.g100} />
            <ScrollView style={{ backgroundColor: color.g100 }}>
                <Tab.Navigator
                    tabBarOptions={{ activeTintColor: color.primary, inactiveTintColor: color.g700, indicatorStyle: { backgroundColor: color.primary } }}>
                    <Tab.Screen name="Akun Pembeli" component={UserProfiles} />
                    <Tab.Screen name="Akun Toko" component={MerchantProfiles} />
                </Tab.Navigator>

            </ScrollView>
        </>
    )
}

const { width, height } = Dimensions.get('screen')
const styles = StyleSheet.create({
    container: {
        padding: 10,
    },
    button: {
        bottom: 0,
        position: 'relative',
        marginTop: 200
    },
    logout: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10
    },
    logoutText: {
        fontSize: 15,
        color: color.failed
    },
    version: {
        alignSelf: 'center'
    }
})

const Umenu = [
    {
        name: 'Profil',
        icon: 'create-outline',
        route: 'UserProfile'
    },
    {
        name: 'Kebijalan Privasi',
        icon: 'document',
        route: 'PrivacyPolice'
    },
    {
        name: 'Riwayat Transaksi',
        icon: 'stopwatch-outline',
        route: 'TransactionListUser'
    },
    {
        name: 'Tambah Bank',
        icon: 'stopwatch-outline',
        route: 'UserBank'
    }
]

const Mmenu = [
    {
        name: 'Produk',
        icon: 'list-outline',
        route: 'product'
    },
    {
        name: 'Edit Toko',
        icon: 'create-outline',
        route: 'UpdateMerchant'
    },
    {
        name: 'Transaksi Toko',
        icon: 'stopwatch-outline',
        route: 'TransactionListMerchant'
    },
    {
        name: 'Alamat Pengiriman',
        icon: 'list-outline',
        route: 'AddOriginAddress'
    }
]

const mapStateToProps = state => {
    return {
        ava: state.user.data.profile_image,
        user: state.user.data.user,
        pin: state.user.data.pin,
        credential: state.user.data.credential,
        isAuthLoading: state.user.isAuthLoading,
        merchant: state.user.merchant,
    }
}

export default connect(mapStateToProps)(profile);
