import React, { useState, useEffect } from 'react'
import { ScrollView, View, StyleSheet, Dimensions, Platform, Text, TouchableOpacity, Image, Alert } from 'react-native'
import Toast from 'react-native-simple-toast'
import { connect } from 'react-redux'
import { Button, HeaderTransparent, Input, ButtonText } from '_atoms'
import { color, style } from '_styles'
import { SelectSearch, Loading } from '_molecules';
import axios from 'axios';
import { set } from 'react-native-reanimated'
import { installReactHook } from 'react-native/Libraries/Performance/Systrace'



const AddOriginAddress = (props) => {

    const [data, setData] = useState({})
    const [prov, setProv] = useState([])
    const [city, setCity] = useState([])
    const [district, setDistrict] = useState([])
    const [toko, setToko] = useState({})
    const [loading, setLoading] = useState(false)
    const [merchant, setMerchant] = useState({})
    const [isLoading, setisLoading] = useState(false)

    console.log({ toko })


    useEffect(() => {
        getProv()
        getOriginAddress()
    }, [])

    const getOriginAddress = () => {
        setisLoading(true)
        axios.get(`/raja-ongkir/origin-address`)
            .finally(() => setisLoading(false))
            .then(res => { setToko(res.data.data) })
            .catch(err => { setToko(null) })
    }

    const getProv = () => {
        setisLoading(true)
        axios.get(`/raja-ongkir/province`)
            .finally(() => setisLoading(false))
            .then(res => {
                // console.log({ res })
                setProv(res.data.data.map(({ province_id: value, province: label }) => ({ value, label })))
            })

    }
    const getCity = (id) => {
        console.log({ id })
        setisLoading(true)
        axios.get(`/raja-ongkir/city/${id}`)
            .finally(() => setisLoading(false))
            .then(res => {
                // console.log({ res })
                setCity(res.data.data.map(({ city_id: value, city_name: label }) => ({ value, label })))
            })
    }
    const getDistrict = (id) => {
        setisLoading(true)
        axios.get(`/raja-ongkir/subdistrict/${id}`)
            .finally(() => setisLoading(false))
            .then(res => {
                // console.log({ res })
                setDistrict(res.data.data.map(({ subdistrict_id: value, subdistrict_name: label }) => ({ value, label })))
            })
    }

    const createMerchant = () => {
        axios.post(`/raja-ongkir/origin-address`, merchant)
            .then(res => {
                console.log({ res })
                Alert.alert('Suksess', 'Tambah Alamat Pengiriman Berhasil', [
                    {
                        text: 'OK',
                        onPress: () => props.navigation.goBack()
                    }
                ])
            }).catch(err => {
                console.log({ err })
                Alert.alert('Gagal', err.message[
                    {
                        text: 'OK',
                        onPress: () => props.navigation.goBack()
                    }
                ])
            })
    }

    return (
        <ScrollView style={style.container}>
            <HeaderTransparent title='Alamat Pengiriman Toko' />
            <View style={[style.body, { padding: 10 }]}>
                {
                    !toko ?
                        <>
                            <View style={{ borderColor: color.failed, borderWidth: 2, width: 200, padding: 5, alignSelf: 'center', marginVertical: 5, borderRadius: 5 }}>
                                <Text>Anda belum Membuat Alamat Pengiriman, Silakan Buat sekarang !</Text>
                            </View>
                            <SelectSearch
                                data={prov}
                                value={merchant.province_id}
                                label='Provinsi'
                                title='Pilih Provinsi'
                                onSelect={(val => { setMerchant(v => ({ ...v, province_id: val.value })); getCity(val.value) })}
                            />
                            <SelectSearch
                                data={city}
                                value={merchant.city_id}
                                label='Kota'
                                title='Pilih Kota'
                                onSelect={(val => { setMerchant(v => ({ ...v, city_id: val.value })); getDistrict(val.value) })}
                            />
                            <SelectSearch
                                data={district}
                                value={merchant.subdistrict_id}
                                label='Kecamatan'
                                title='Pilih Kecamatan'
                                onSelect={(val => { setMerchant(v => ({ ...v, subdistrict_id: val.value })) })}
                            />
                            <Input
                                label='Alamat'
                                placeholder='Nama jalan, Nomor Rumah, Rt.Rw'
                                iconLeft='person-outline'
                                placeholder={user.address}
                                onChangeText={(address => setMerchant(v => ({ ...v, address })))}
                            />
                            <Button title='Simpan' onPress={() => createMerchant()} />
                        </>
                        :
                        <>
                            <Input
                                label='Propinsi'
                                value={toko.province?.province}
                            />
                            <Input
                                label='Kabupaten / Kota'
                                value={toko.city?.city_name}
                            />
                            <Input
                                label='Kecamatan'
                                value={toko.subdistrict?.subdistrict_name}
                            />
                            <Input
                                label='Alamat Lengkap'
                                value={toko.address}
                            />

                            <Button title="Ganti Alamat Pengiriman Toko" onPress={() => props.navigation.navigate('UpdateOriginAddress', { data: toko })} />
                        </>
                }
            </View>
            <Loading isLoading={isLoading} />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: color.primary,
        height: Dimensions.get('screen').height / 8,
        justifyContent: 'center',
        paddingTop: Platform.OS == 'ios' && isIphoneX() ? 20 : 0
    },
})

const mapStateToProps = state => {
    return {
        user: state.user.data,
        userBirthDate: state.user.birthDate,
    }
}

export default connect(mapStateToProps)(AddOriginAddress)