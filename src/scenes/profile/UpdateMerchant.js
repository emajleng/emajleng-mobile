import React, { useState, useEffect } from 'react'
import { ScrollView, View, StyleSheet, Dimensions, Platform, Text, TouchableOpacity, Image, Alert } from 'react-native'
import Toast from 'react-native-simple-toast'
import { connect } from 'react-redux'
import { Button, HeaderTransparent, Input, ButtonText } from '_atoms'
import { color, style } from '_styles'
import { SelectSearch, Loading } from '_molecules';
import Icon from 'react-native-vector-icons/Ionicons';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import { getMerchant, getUser } from '_states/actions/user';


const UpdateMerchant = (props) => {

    const [data, setData] = useState({})
    const [prov, setProv] = useState([])
    const [city, setCity] = useState([])
    const [district, setDistrict] = useState([])
    const [village, setVillage] = useState([])
    const [toko, setToko] = useState({})
    const [merchant, setMerchant] = useState({})
    const [isLoading, setisLoading] = useState(false)
    const [image, setImage] = useState({})

    useEffect(() => {
        getProv()
        getMerchant()
        console.log({ toko })
    }, [])

    const getMerchant = () => {
        axios.get(`/user/merchant`)
            .then(res => {
                console.log({ res })
                setToko(res.data.data)
            }).catch(err => {
                console.log({ err })
                setToko(null)
            })
    }

    const getProv = () => {
        setisLoading(true)
        axios.get(`/public/province`)
            .finally(() => setisLoading(false))
            .then(res => {
                console.log({ res })
                setProv(res.data.data.map(({ id: value, name: label }) => ({ value, label })))
            })

    }
    const getCity = (id) => {
        setisLoading(true)
        axios.get(`/public/city/${id}`)
            .finally(() => setisLoading(false))
            .then(res => {
                console.log({ res })
                setCity(res.data.data.map(({ id: value, name: label }) => ({ value, label })))
            })
    }
    const getDistrict = (id) => {
        setisLoading(true)
        axios.get(`/public/district/${id}`)
            .finally(() => setisLoading(false))
            .then(res => {
                console.log({ res })
                setDistrict(res.data.data.map(({ id: value, name: label }) => ({ value, label })))
            })
    }
    const getVillage = (id) => {
        setisLoading(true)
        axios.get(`/public/village/${id}`)
            .finally(() => setisLoading(false))
            .then(res => {
                console.log({ res })
                setVillage(res.data.data.map(({ id: value, name: label }) => ({ value, label })))
            })
    }
    const selectImage = (type) => {
        const options = {
            quality: 0.8,
            maxWidth: 1280,
            maxHeight: 1280,
            storageOptions: {
                skipBackup: true
            }
        }
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
                console.log('caceled')
            } else if (response.error) {
                Toast.show(response.error, Toast.LONG)
                console.log('ImagePicker Error: ', response.error);
            } else {
                console.log({ response })
                let filename = response.uri.split('/').pop();
                setImage({ uri: response.uri, name: response.fileName ?? filename, type: response.type })
            }
        })
    }

    const deleteImage = item => {
        const img = image.filter(x => x.uri != item.uri)
        setImage(img)
    }

    const createMerchant = () => {
        setisLoading(true)
        const formData = new FormData()
        formData.append('name', merchant.name ? merchant.name : toko.name)
        formData.append('address', merchant.address ? merchant.address : toko.address)
        formData.append('province_id', merchant.province_id ? merchant.province_id : toko.province.id)
        formData.append('city_id', merchant.city_id ? merchant.city_id : toko.city.id)
        formData.append('district_id', merchant.district_id ? merchant.district_id : toko.village.district_id)
        formData.append('village_id', merchant.village_id ? merchant.village_id : toko.village.id)
        formData.append('latitude', '13784')
        formData.append('longitude', '387242')
        if (image.uri) {
            formData.append('logo', image)
        }
        axios.post(`/user/merchant/update`, formData)
            .finally(() => { setisLoading(false) })
            .then(res => {
                console.log({ res })
                Alert.alert('Suksess', 'Edit Toko Berhasil', [
                    {
                        text: 'OK',
                        onPress: () => props.navigation.goBack()
                    }
                ])
            }).catch(err => {
                console.log({ err })
                Alert.alert('Gagal', err.message[
                    {
                        text: 'OK',
                        onPress: () => props.navigation.goBack()
                    }
                ])
                props.dispatch(getMerchant())
                props.dispatch(getUser())
            })
    }

    return (
        <>
            <HeaderTransparent title='Edit Informasi Toko' />
            <ScrollView style={style.container}>
                <View style={[style.body, { padding: 10 }]}>
                    <Input label='Nama Toko' onChangeText={(name => setMerchant(v => ({ ...v, name })))} material placeholder={toko.name} />
                    <SelectSearch
                        data={prov}
                        value={merchant.province_id}
                        label='Provinsi'
                        title='Pilih Provinsi'
                        placeholder={toko.province?.name}
                        onSelect={(val => { setMerchant(v => ({ ...v, province_id: val.value })); getCity(val.value) })}
                    />
                    <SelectSearch
                        data={city}
                        value={merchant.city_id}
                        label='Kota'
                        title='Pilih Kota'
                        placeholder={toko.city?.name}
                        onSelect={(val => { setMerchant(v => ({ ...v, city_id: val.value })); getDistrict(val.value) })}
                    />
                    <SelectSearch
                        data={district}
                        value={merchant.district_id}
                        label='Kecamatan'
                        title='Pilih Kecamatan'
                        placeholder={toko.district?.name}
                        onSelect={(val => { setMerchant(v => ({ ...v, district_id: val.value })); getVillage(val.value) })}
                    />
                    <SelectSearch
                        data={village}
                        value={merchant.village_id}
                        label='Kelurahan'
                        title='Pilih Kelurahan'
                        placeholder={toko.village?.name}
                        onSelect={(val => setMerchant(v => ({ ...v, village_id: val.value })))}
                    />
                    <View style={{ height: 200, paddingTop: 20 }}>
                        <ButtonText title="Klik ini untuk Ganti Foto Toko" color={color.primary} onPress={() => selectImage()} />
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: 250, alignSelf: 'center', paddingTop: 20 }}>
                            <View style={{ alignItems: 'center' }}>
                                <Text>Foto toko</Text>
                                <Image source={{ uri: toko.logo }} style={{ width: 100, height: 100, marginRight: 5, marginTop: 5 }} />
                            </View>
                            {
                                image.uri &&
                                <View style={{ alignItems: 'center' }} >
                                    <Text style={{ color: color.secondary }}>Foto Baru</Text>
                                    <Image source={{ uri: image.uri }} style={{ width: 100, height: 100, marginRight: 5, marginTop: 5 }} />
                                </View>
                            }
                        </View>
                    </View>
                    <Button title='Simpan Perubahan' onPress={() => createMerchant()} />
                </View>
                <Loading isLoading={isLoading} />
            </ScrollView>
        </>
    )
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: color.primary,
        height: Dimensions.get('screen').height / 8,
        justifyContent: 'center',
        paddingTop: Platform.OS == 'ios' && isIphoneX() ? 20 : 0
    },
})

const mapStateToProps = state => {
    return {
        user: state.user.data,
        merchant: state.user.merchant
    }
}

export default connect(mapStateToProps)(UpdateMerchant)