import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, TouchableHighlight, View, StatusBar, ScrollView, Alert } from 'react-native';
import { Button, ButtonText, Input, HeaderTransparent } from '_atoms';
import { SelectSearch, Loading } from '_molecules';
import { color, style } from '_styles';
import axios from 'axios';
import { connect } from 'react-redux';
import { getMerchant, getUser } from '_states/actions/user';

const UpdateProfile = (props) => {

    const [loading, setLoading] = useState(false)
    const [security, setSecurity] = useState(true)
    const [isLoading, setisLoading] = useState(false)
    const [email, setEmail] = useState({})
    const [password, setPassword] = useState({})
    const [data, setData] = useState({})
    const [prov, setProv] = useState([])
    const [city, setCity] = useState([])
    const [district, setDistrict] = useState([])
    const [village, setVillage] = useState([])
    const user = props.user
    console.log({ user })

    useEffect(() => {
        getProv()
    }, [])

    const handleChangeText = (type, val) => {
        const x = data
        x[type] = val
        setData(x)
    }

    const getProv = () => {
        setisLoading(true)
        axios.get(`/public/province`)
            .finally(() => setisLoading(false))
            .then(res => {
                console.log({ res })
                setProv(res.data.data.map(({ id: value, name: label }) => ({ value, label })))
            })

    }
    const getCity = (id) => {
        setisLoading(true)
        axios.get(`/public/city/${id}`)
            .finally(() => setisLoading(false))
            .then(res => {
                console.log({ res })
                setCity(res.data.data.map(({ id: value, name: label }) => ({ value, label })))
            })
    }
    const getDistrict = (id) => {
        setisLoading(true)
        axios.get(`/public/district/${id}`)
            .finally(() => setisLoading(false))
            .then(res => {
                console.log({ res })
                setDistrict(res.data.data.map(({ id: value, name: label }) => ({ value, label })))
            })
    }
    const getVillage = (id) => {
        setisLoading(true)
        axios.get(`/public/village/${id}`)
            .finally(() => setisLoading(false))
            .then(res => {
                console.log({ res })
                setVillage(res.data.data.map(({ id: value, name: label }) => ({ value, label })))
            })
    }

    const toLogin = () => {
        const phones = '+62' + (data.phone ? data.phone : user.phone)
        setLoading(true)
        axios.put(`/user`, {
            name: data.name ? data.name : user.name,
            phone: phones,
            email: data.email ? data.email : user.email,
            address: data.address ? data.address : user.address,
            village_id: data.village_id ? data.village_id : user.village ? user.village.id : '000',
            district_id: data.district_id ? data.district_id : user.district.id,
            city_id: data.city_id ? data.city_id : user.city.id,
            postal_code: data.postal_code ? data.postal_code : user.postal_code,
            province_id: data.province_id ? data.province_id : user.province.id,
        }).finally(() => {
            setLoading(false)
        })
            .then(res => {
                Alert.alert('Berhasil', 'Selamat, kamu sudah berhasil Menggubah data profil. Selamat bertransaksi :)', [
                    {
                        text: 'OK',
                        onPress: () => props.navigation.popToTop()
                    }
                ])
                props.dispatch(getMerchant())
                props.dispatch(getUser())

            })
    }

    return (
        <>
            <HeaderTransparent title='Edit Profil' />
            <ScrollView style={style.container}>
                <View style={[style.body, { padding: 16 }]}>
                    <Input
                        label='Nama Lengkap'
                        iconLeft='person-outline'
                        placeholder={user.name}
                        onChangeText={(name => setData(v => ({ ...v, name })))}
                    />
                    <Input
                        label='Nomor Telpon'
                        iconLeft='phone-portrait-sharp'
                        keyboardType='phone-pad'
                        prefix='+62'
                        value={data.phone}
                        onChangeText={(phone => phone != '0' && setData(v => ({ ...v, phone })))}
                    />
                    <Input
                        label='Email '
                        iconLeft='mail-sharp'
                        keyboardType='email-address'
                        placeholder={user.email}
                        onChangeText={(email => setData(v => ({ ...v, email })))}

                    />
                    <Input
                        label='Alamat'
                        iconLeft='person-outline'
                        placeholder={user.address}
                        onChangeText={(address => setData(v => ({ ...v, address })))}
                    />
                    <SelectSearch
                        data={prov}
                        value={data.province_id}
                        label='Provinsi'
                        title='Pilih Provinsi'
                        placeholder={user.province.name}
                        onSelect={(val => { setData(v => ({ ...v, province_id: val.value })); getCity(val.value) })}
                    />
                    <SelectSearch
                        data={city}
                        value={data.city_id}
                        label='Kota'
                        title='Pilih Kota'
                        placeholder={user.city.name}
                        onSelect={(val => { setData(v => ({ ...v, city_id: val.value })); getDistrict(val.value) })}
                    />
                    <SelectSearch
                        data={district}
                        value={data.district_id}
                        label='Kecamatan'
                        title='Pilih Kecamatan'
                        placeholder={user.district.name}
                        onSelect={(val => { setData(v => ({ ...v, district_id: val.value })); getVillage(val.value) })}
                    />
                    <SelectSearch
                        data={village}
                        value={data.village_id}
                        label='Kelurahan'
                        title='Pilih Kelurahan'
                        placeholder={user.village != null ? user.village.name : 'Kelurahan'}
                        onSelect={(val => setData(v => ({ ...v, village_id: val.value })))}
                    />
                    <Button
                        title='Simpan'
                        loading={loading}
                        disabled={loading || !email || password.length < 8}
                        onPress={toLogin}
                    />
                    <Loading isLoading={isLoading} />
                </View>
            </ScrollView>
        </>
    )
}

const styles = StyleSheet.create({
    container: {

    }
})

const mapStateToProps = state => {
    return {
        user: state.user.data,
    }
}

export default connect(mapStateToProps)(UpdateProfile)