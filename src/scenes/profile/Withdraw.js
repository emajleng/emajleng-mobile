import React, { useState, useRef, useEffect } from 'react';
import { Text, StatusBar, View, FlatList, StyleSheet, TouchableHighlight, Dimensions } from 'react-native';
import { Divider, Button } from '_atoms';
import { color, style } from '_styles';
import { connect } from 'react-redux';
import { WithdrawBalance, WithdrawHistory } from '_organisms';
import ViewPager from '@react-native-community/viewpager';
import axios from 'axios';

const Withdraw = (props) => {

    const menuRef = useRef(null)
    const pagerRef = useRef(null)
    const [rekening, setrekening] = useState({})
    console.log({ rekening })
    const [selected, setSelected] = useState(0)

    useEffect(() => {
        getRekening()
    }, [])

    const getRekening = () => {
        axios.get(`/user_bank`)
            .then(res => {
                console.log({ res })
                setrekening(res.data.data)
            })
    }
    const scrollTo = (index) => {
        pagerRef.current.setPage(index)
        menuRef.current.scrollToIndex({ index })
    }

    const renderMenu = ({ item, index }) => {
        const isSelected = selected == index
        return (
            <View>
                <TouchableHighlight onPress={() => scrollTo(index)} underlayColor='transparent' style={[styles.menu, { backgroundColor: isSelected ? color.primary : '#fff' }]}>
                    <Text style={{ color: isSelected ? '#fff' : color.primary, fontWeight: 'bold' }}>{item.name}</Text>
                </TouchableHighlight>
            </View>
        )
    }

    return (
        <View style={style.container}>
            <StatusBar barStyle='dark-content' />
            {
                rekening[0]?.id ?
                    <>
                        <View style={{ flex: 0 }}>
                            <FlatList
                                data={menu}
                                ref={menuRef}
                                keyExtractor={item => item.name}
                                horizontal
                                showsHorizontalScrollIndicator={false}
                                renderItem={renderMenu} />
                        </View>
                        <Divider />
                        <ViewPager ref={pagerRef} onPageScroll={v => setSelected(v.nativeEvent.position)} style={styles.viewPager} initialPage={parseInt(selected)} >
                            <WithdrawBalance key='1' navigate={({ data, paymentMethod }) => props.navigation.navigate('TopUpInvoice', { data, paymentMethod })} />
                            <WithdrawHistory key='2' />
                        </ViewPager>
                    </> :
                    <View style={{ padding: 20 }}>
                        <View style={{ borderColor: color.failed, marginTop: 20, borderWidth: 2, width: 250, padding: 5, alignSelf: 'center', marginVertical: 5, borderRadius: 5 }}>
                            <Text>Anda belum Memasukan rekening, Silakan Masukan sekarang !</Text>
                        </View>
                        <Button title="Masukan Rekening" onPress={() => props.navigation.navigate('UserBank')} />
                    </View>
            }
        </View>
    )
}

const { width } = Dimensions.get('screen')

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    history: {
        alignItems: 'center'
    },
    title: {
        fontWeight: 'bold',
        color: color.g800,
        marginBottom: 10
    },
    date: {
        fontSize: 12,
        color: color.g500
    },
    price: {
        fontWeight: 'bold',
        color: color.secondary
    },
    status: {
        fontWeight: 'bold',
        marginBottom: 10,
    },
    viewPager: {
        flex: 1,
        flexDirection: 'row'
    },
    menu: {
        margin: 10,
        padding: 7,
        borderRadius: 20,
        width: width / 2 - 20,
        alignItems: 'center',
        borderWidth: 2,
        borderColor: color.primary
    }
})

const menu = [
    {
        name: 'Tarik Saldo',
    },
    {
        name: 'Riwayat',
    }
]

const mapStateToProps = state => {
    return {
        user: state.user.data
    }
}

export default connect(mapStateToProps)(Withdraw);