import React, { useState, useEffect } from 'react'
import { ScrollView, View, StyleSheet, Dimensions, Platform, Text, TouchableOpacity, Image, Alert } from 'react-native'
import Toast from 'react-native-simple-toast'
import { connect } from 'react-redux'
import { Button, HeaderTransparent, Input, ButtonText } from '_atoms'
import { color, style } from '_styles'
import { SelectSearch, Loading } from '_molecules';
import Icon from 'react-native-vector-icons/Ionicons';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import { set } from 'react-native-reanimated'


const MerchantProfiles = (props) => {

    const [data, setData] = useState({})
    const [prov, setProv] = useState([])
    const [city, setCity] = useState([])
    const [district, setDistrict] = useState([])
    const [village, setVillage] = useState([])
    const [loading, setLoading] = useState(false)
    const [merchant, setMerchant] = useState({})
    const [isLoading, setisLoading] = useState(false)
    const [image, setImage] = useState({})
    const [imag, setImag] = useState({})

    useEffect(() => {
        getProv()
    }, [])

    const getProv = () => {
        setisLoading(true)
        axios.get(`/public/province`)
            .finally(() => setisLoading(false))
            .then(res => {
                console.log({ res })
                setProv(res.data.data.map(({ id: value, name: label }) => ({ value, label })))
            })

    }
    const getCity = (id) => {
        setisLoading(true)
        axios.get(`/public/city/${id}`)
            .finally(() => setisLoading(false))
            .then(res => {
                console.log({ res })
                setCity(res.data.data.map(({ id: value, name: label }) => ({ value, label })))
            })
    }
    const getDistrict = (id) => {
        setisLoading(true)
        axios.get(`/public/district/${id}`)
            .finally(() => setisLoading(false))
            .then(res => {
                console.log({ res })
                setDistrict(res.data.data.map(({ id: value, name: label }) => ({ value, label })))
            })
    }
    const getVillage = (id) => {
        setisLoading(true)
        axios.get(`/public/village/${id}`)
            .finally(() => setisLoading(false))
            .then(res => {
                console.log({ res })
                setVillage(res.data.data.map(({ id: value, name: label }) => ({ value, label })))
            })
    }

    const selectImage = (type) => {
        const options = {
            quality: 0.8,
            maxWidth: 1280,
            maxHeight: 1280,
            storageOptions: {
                skipBackup: true
            }
        }
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
                console.log('caceled')
            } else if (response.error) {
                Toast.show(response.error, Toast.LONG)
                console.log('ImagePicker Error: ', response.error);
            } else {
                console.log({ response })
                let filename = response.uri.split('/').pop();
                // const img = [...image, { uri: response.uri, name: response.fileName ?? filename, type: response.type }]
                setImage({ uri: response.uri, name: response.fileName ?? filename, type: response.type })
                // console.log({ img })
            }
        })
    }

    const selectImag = (type) => {
        const options = {
            quality: 0.8,
            maxWidth: 1280,
            maxHeight: 1280,
            storageOptions: {
                skipBackup: true
            }
        }
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
                console.log('caceled')
            } else if (response.error) {
                Toast.show(response.error, Toast.LONG)
                console.log('ImagePicker Error: ', response.error);
            } else {
                console.log({ response })
                let filename = response.uri.split('/').pop();
                // const img = [...image, { uri: response.uri, name: response.fileName ?? filename, type: response.type }]
                setImag({ uri: response.uri, name: response.fileName ?? filename, type: response.type })
                // console.log({ img })
            }
        })
    }

    const deleteImage = item => {
        const img = image.filter(x => x.uri != item.uri)
        setImage(img)
    }

    const createMerchant = () => {
        setisLoading(true)
        const formData = new FormData()
        formData.append('name', merchant.name)
        formData.append('address', merchant.address)
        formData.append('province_id', merchant.province_id)
        formData.append('city_id', merchant.city_id)
        formData.append('district_id', merchant.district_id)
        formData.append('village_id', merchant.village_id)
        formData.append('latitude', '13784')
        formData.append('longitude', '387242')
        formData.append('logo', image)
        formData.append('siup_image', imag)
        axios.post(`/user/merchant`, formData)
            .finally(() => { setisLoading(false) })
            .then(res => {
                console.log({ res })
                Alert.alert('Suksess', 'Toko Berhasil Dibuat', [
                    {
                        text: 'OK',
                        onPress: () => props.navigation.popToTop()
                    }
                ])
            })
    }

    return (
        <>
            <HeaderTransparent title='Buat Toko' />
            <ScrollView style={style.container}>
                <View style={[style.body, { padding: 16 }]}>
                    <Input label='Nama Toko' onChangeText={(name => setMerchant(v => ({ ...v, name })))} material />
                    <Input label='Alamat Toko' onChangeText={(address => setMerchant(v => ({ ...v, address })))} material />
                    <SelectSearch
                        data={prov}
                        value={merchant.province_id}
                        label='Provinsi'
                        title='Pilih Provinsi'
                        onSelect={(val => { setMerchant(v => ({ ...v, province_id: val.value })); getCity(val.value) })}
                    />
                    <SelectSearch
                        data={city}
                        value={merchant.city_id}
                        label='Kota'
                        title='Pilih Kota'
                        onSelect={(val => { setMerchant(v => ({ ...v, city_id: val.value })); getDistrict(val.value) })}
                    />
                    <SelectSearch
                        data={district}
                        value={merchant.district_id}
                        label='Kecamatan'
                        title='Pilih Kecamatan'
                        onSelect={(val => { setMerchant(v => ({ ...v, district_id: val.value })); getVillage(val.value) })}
                    />
                    <SelectSearch
                        data={village}
                        value={merchant.village_id}
                        label='Kelurahan'
                        title='Pilih Kelurahan'
                        onSelect={(val => setMerchant(v => ({ ...v, village_id: val.value })))}
                    />
                    <View style={{ height: 200, paddingTop: 20 }}>
                        <ButtonText title="Pilih Foto Toko" color={color.primary} onPress={() => selectImage()} />
                        {/* <TouchableOpacity style={styles.delete} onPress={() => deleteImage(item)}>
                            <Icon name='close' size={20} />
                        </TouchableOpacity> */}
                        <Image source={{ uri: image.uri }} style={{ width: 80, height: 80, marginRight: 5 }} />
                    </View>
                    <View style={{ height: 200, paddingTop: 20 }}>
                        <ButtonText title="Upload SIUP" color={color.primary} onPress={() => selectImag()} />
                        {/* <TouchableOpacity style={styles.delete} onPress={() => deleteImage(item)}>
                            <Icon name='close' size={20} />
                        </TouchableOpacity> */}
                        <Image source={{ uri: imag.uri }} style={{ width: 80, height: 80, marginRight: 5 }} />
                    </View>
                    <Button title='Buat Toko' onPress={() => createMerchant()} />
                </View>
                <Loading isLoading={isLoading} />
            </ScrollView>
        </>
    )
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: color.primary,
        height: Dimensions.get('screen').height / 8,
        justifyContent: 'center',
        paddingTop: Platform.OS == 'ios' && isIphoneX() ? 20 : 0
    },
})

const mapStateToProps = state => {
    return {
        user: state.user.data,
        userBirthDate: state.user.birthDate,
    }
}

export default connect(mapStateToProps)(MerchantProfiles)