import React, { useEffect, useRef, useState } from 'react';
import { Dimensions, FlatList, StyleSheet, Text, StatusBar, View, Animated, RefreshControl } from 'react-native';
import { Slider } from '_molecules';
import { FooterLog, HeaderHome, ButtonIndicator, Input, InputSearch, LineVertical } from '_atoms';
import { CardInfo, Menu, ProductList, ShopList, LoadingSmall } from '_molecules';
import { HomeProductList } from '_organisms';
import { color, style } from '_styles';
import { connect } from 'react-redux';
import axios from 'axios';
import { ScrollView } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';


const Home = (props) => {

    const searchRef = useRef(null)
    const productRef = useRef(null)
    const [selected, setSelected] = useState('')
    const [isLoading, setisLoading] = useState(false)
    const [page, setPage] = useState({})
    const [id, setId] = useState('')
    const animatedValue = new Animated.Value(0);
    const [visibleLog, setVisibleLog] = useState(true)
    const [productAll, setProductAll] = useState([])
    const [marketProduct, setMarketProduct] = useState([])
    const [categoryProduct, setCategoryProduct] = useState([])
    const [activeMenu, setActiveMenu] = useState({})
    const [banner, setBanner] = useState([])

    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', () => {
            newProduct(),
                getCategory,
                productMarket(),
                ProductCategory(category[0].id),
                activ()
            getBanner()
        })
        return unsubscribe;
    }, [props.navigation])

    const getBanner = () => {
        axios.get(`/public/banner`)
            .then(res => {
                console.log(res)
                setBanner(res.data.data)
            })
            .catch(err => { console.log(err) })
    }

    const getCategory = () => {
        axios.get(`/public/category`)
            .then(res => {
                console.log({ jj: res })
            })
    }
    const navigateToSreachScreen = () => {
        props.navigation.navigate('SearchScreen')
        searchRef.current.blur()
    }

    const handlePress = (v) => {
        if (props.token) {
            props.navigation.navigate(v.route, { item: v })
            return
        }
        props.navigation.navigate('Login')
    }

    const handleChangeCategory = (item, index) => {
        setisLoading(true)
        setSelected(item.id)
        ProductCategory(item.id)
        productRef.current?.scrollToIndex({ index })
    }

    const renderMenu = ({ item, index }) => {
        const isActive = selected == item.id
        return (
            <ButtonIndicator
                title={item.menu}
                stripColor={'#fff'}
                colors={isActive ? [color.primary, '#FFC10D'] : ['#fff', '#fff']}
                strip={isActive}
                border={isActive ? null : 1}
                textColor={isActive ? '#fff' : color.g500}
                style={{ borderColor: isActive ? null : color.g300 }}
                onPress={() => { handleChangeCategory(item, index) }} />
        )
    }

    const newProduct = () => {
        axios.get(`product/product/all`)
            .then(res => {
                setProductAll(res.data.data)
            })
    }

    const productMarket = () => {
        axios.get(`/product/category/3`)
            .then(res => {
                setMarketProduct(res.data.data)
            })
    }
    const ProductCategory = (id) => {
        axios.get(`/product/category/${id}?page=0`)
            .finally(() => { setisLoading(false) })
            .then(res => {
                console.log({ res })
                setCategoryProduct(res.data.data)
                setPage(res.data.paging)
                setId(id)
            })
    }
    const ProductCategoryMore = (pages) => {
        console.log({ pages })
        const ids = id
        axios.get(`/product/category/${ids}?page=${pages}`)
            .finally(() => { setisLoading(false) })
            .then(res => {
                console.log({ pageings: res })
                setCategoryProduct(categoryProduct.concat(res.data.data))
                setPage(res.data.paging)
            })
    }

    const loadMore = () => {
        if (parseInt(page.page) + 1 <= page.total_page) {
            setisLoading(true)
            ProductCategoryMore(parseInt(page.page) + 1)
        }
    }

    const activ = () => {
        setActiveMenu(category)
        setSelected(category[0].id)
    }
    const headerComponent = () => {
        return (
            <ScrollView >
                <Slider slider={banner} />
                <CardInfo balance={props.user.balance} navigate={(v) => handlePress(v)} />
                {/* <Menu onPress={handlePress} /> */}
                <LineVertical width={20} height={20} />
                <ProductList title='Produk Pasar Tradisional' data={marketProduct} onSeeAll={() => props.navigation.navigate('AllProductCategories', { data: marketProduct, key: 1 })} />
                <ProductList title='Produk Terbaru' data={productAll} onSeeAll={() => props.navigation.navigate('AllProductCategories', { data: productAll, key: 2 })} containerStyle={{ marginTop: 20 }} />
                {/* <ShopList title='Toko Terbaru' data={merchant} onSeeAll={handlePress} /> */}
                <View style={{ margin: 10 }}>
                    <FlatList
                        data={activeMenu}
                        ref={productRef}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={renderMenu}
                        onScrollToIndexFailed={info => {
                            const wait = new Promise(resolve => setTimeout(resolve, 2000));
                            wait.then(() => {
                                productRef.current?.scrollToIndex({ index: info.index });
                            });
                        }}
                    />
                </View>

            </ScrollView>
        )
    }

    const createChat = () => {
        props.navigation.navigate('chat')
    }

    return (
        <View style={style.container}>
            <StatusBar translucent backgroundColor='transparent' barStyle="dark-content" />
            <HeaderHome animated={animatedValue} >
                <InputSearch inputRef={searchRef} placeholder="Cari di Padi Raharja" onFocus={navigateToSreachScreen} />
                <Icon name='chatbubble-ellipses-outline' onPress={() => createChat()} color={color.g100} size={35} style={styles.icon} />
            </HeaderHome>
            <HomeProductList
                data={categoryProduct}
                category={category}
                selected={selected}
                loadMore={() => loadMore()}
                headerComponent={headerComponent()}
                onScroll={Animated.event(
                    [{ nativeEvent: { contentOffset: { y: animatedValue } } }],
                    {
                        useNativeDriver: false
                    },
                )}

            />
            {
                !props.token &&
                <FooterLog text='Masuk dulu untuk belanja' textButton=' Masuk, yuk!' onPress={() => props.navigation.navigate('Login')} closePress={() => setVisibleLog(false)} />
            }
            <LoadingSmall isLoading={isLoading} />
        </View>
    )
}

const styles = StyleSheet.create({
    header: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingBottom: 8,
    },
    img: {
        height: 50,
        width: 50,
        borderRadius: 25,
        backgroundColor: color.g400
    },
    wrapper: {
        top: -20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 16
    }
})

const { width, height } = Dimensions.get('screen')

const category = [
    {
        id: '1',
        menu: 'Pakaian'
    },
    {
        id: '2',
        menu: 'Makanan & Minuman'
    },
    {
        id: '3',
        menu: 'Pasar Tradisional'

    }
]

const mapStateToProps = state => {
    return {
        user: state.user.data,
        token: state.user.token,
        banner: state.user.banner
    }
}

export default connect(mapStateToProps)(Home)