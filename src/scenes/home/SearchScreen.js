import React from 'react';
import { View, Text, ScrollView } from 'react-native';
import { HeaderSearch } from '_atoms';
import { ProductSearchList, Loading } from '_molecules';
import axios from 'axios';
import { style } from '_styles';

class SearchScreen extends React.Component {
    constructor(props) {
        super(props),
            this.state = {
                loading: false,
                data: [],
                title: 'Silakan Cari Produk Pilihanmu'

            }
    }

    getData = (key) => {
        this.setState({ loading: true })
        axios.get(`/product/product/all?name=${key}`)
            .finally(() => {
                this.setState({ loading: false })
            }).then(res => {
                console.log({ res })
                this.setState({ data: res.data.data })
            }).then(error => {
                this.setState({ title: 'Produk yang anda cari tidak ditemukan' })
            })
    }

    render() {
        const { data, loading, title } = this.state
        console.log({ data })
        return (
            <ScrollView style={style.container}>
                <HeaderSearch onSubmitEditing={this.getData} />
                <ProductSearchList data={data} title={title} />
                <Loading isLoading={loading} />
            </ScrollView>
        )
    }
}

export default (SearchScreen);