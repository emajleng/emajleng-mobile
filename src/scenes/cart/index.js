import React, { sestate } from 'react';
import { View, Text, StatusBar, StyleSheet, FlatList, Dimensions, Image, Alert } from 'react-native';
import { color, style } from '_styles';
import axios from 'axios';
import { autoCurency } from '_utils';
import Icon from 'react-native-vector-icons/Ionicons';
import { Button } from '_atoms';
import { Loading } from '_molecules'
import InputSpinner from "react-native-input-spinner";


class cart extends React.Component {
    constructor(props) {
        super(props),
            this.state = {
                data: [],
                loading: false
            }
    }

    componentDidMount() {
        this.setState({ loading: true })
        this.getCart()
        this.focusListener = this.props.navigation.addListener('focus', () => {
            this.getCart()
        })
    }

    getCart = () => {
        this.setState({ loading: true })
        axios.get(`/cart`)
            .then(res => {
                const data = res.data.data
                this.setState({ data })
                console.log(res)
                this.setState({ loading: false })
            }).catch(err => {
                console.log(err)
            })
    }

    updateCart = (item, num) => {
        axios.put(`/cart/${item.item.id}`, {
            quantity: item.num
        }).then(res => {
            this.getCart()
        }).catch(err => {
            alert('ga oke')
        })
    }

    deleteFromCart = (id) => {
        Alert.alert(
            'Hapus Produk dari keranjang',
            ' ', [
            {
                text: 'tidak', onPress: () => { }
            },
            {
                text: 'Hapus', onPress: () => {
                    axios.delete(`/cart/${id}`)
                        .then(res => {
                            this.getCart()
                        })
                }
            }
        ])

    }

    // renderEmptyList = () => {
    //     return (
    //         <View style={styles.emptyView}>
    //             <Image source={require('_assets/images/sorry.png')} style={styles.logo} resizeMode='contain' />
    //             <Text style={{ fontWeight: 'bold', color: color.g700 }}>Keranjang Anda Kosong</Text>
    //         </View>
    //     )
    // }

    renderCart = ({ item, index }) => {
        console.log({ item })
        return (
            <View>
                <View style={styles.cartContainer}>
                    <Text style={styles.productName}> Produk {item.product.merchant_name}</Text>
                    <View style={styles.productView}>
                        <Image source={{ uri: item.product.photo }} style={styles.productImage} resizeMode='contain' />
                        <View>
                            <Text style={styles.productName}>{item.product.name}</Text>
                            <Text style={styles.priceText}>{autoCurency(item.product.price)}</Text>

                            <Text style={styles.productStock}>{item.quantity} PCS</Text>
                        </View>
                    </View>
                    <View style={styles.icon}>
                        <Icon name={'trash-bin-sharp'} color={color.g400} size={25} onPress={() => this.deleteFromCart(item.id)} />
                        <Button title='Beli' style={{ marginLeft: 10, marginRight: 5, height: 35, backgroundColor: color.secondary }} onPress={() => this.props.navigation.navigate('BuyProduct', { data: item, quantity: item.quantity, cart: item.id })} />
                    </View>
                </View>
                <View style={{ height: 6, flex: 1, backgroundColor: color.g200 }}></View>

            </View>
        )
    }

    render() {
        const { data, loading } = this.state;
        return (
            <>
                <View style={style.body}>
                    {/* <StatusBar translucent={false} backgroundColor={color.primary} barStyle='light-content' /> */}
                    {/* <HeaderForCart title='Keranjang' /> */}
                    <FlatList
                        data={data}
                        keyExtractor={(item, index) => item.toString()}
                        renderItem={this.renderCart}
                    // ListEmptyComponent={this.renderEmptyList}
                    />
                    <Loading isLoading={loading} />
                </View>
            </>
        )
    }
}

const { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
    Container: {
        marginBottom: 10,
        flex: 1
    },
    logo: {
        width: width - 100,
        height: height / 2,
        alignSelf: 'center',
    },
    emptyView: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    cartContainer: {
        padding: 10,
        flex: 1
    },
    productView: {
        padding: 10,
        flex: 1,
        flexDirection: 'row',
    },
    productImage: {
        width: 70,
        height: 70,
        borderRadius: 10,
        marginRight: 10
    },
    productName: {
        fontSize: 16,
        fontWeight: 'bold',
        color: color.g900,
        marginBottom: 5
    },
    priceText: {
        color: color.failed,
        fontWeight: 'bold'
    },
    productStock: {
        fontSize: 12,
        color: color.g900
    },
    icon: {
        alignSelf: 'flex-end',
        position: 'absolute',
        marginTop: 90,
        flexDirection: 'row',
        alignItems: 'center'
    },
    updateText: {
        alignSelf: 'center',
        color: color.secondary,
        fontWeight: 'bold'
    },
    input: {
        height: 25,
        width: 80,
        alignSelf: 'center',
        marginRight: 10,
        marginLeft: 10
    },
    inputStyles: {
        padding: 0,
        alignSelf: 'center'
    },
    buttonStyles: {
        height: 25,
        width: 25
    }
})

export default cart;