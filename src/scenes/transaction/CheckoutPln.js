import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, Dimensions } from 'react-native';
import axios from 'axios';
import { Button, Divider, Card } from '_atoms';
import { color } from '_styles';
import { Denom } from '_molecules';
import { ModalPin } from '_organisms'
import { getMerchant } from '_states/actions/user';
import { connect } from 'react-redux';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';

const CheckoutPln = (props) => {

    const { data } = props.route.params
    const [loading, setLoading] = useState(false)
    const [loadingDenom, setLoadingDenom] = useState(false)
    const [modalPin, setModalPin] = useState(false)
    const [denom, setDenom] = useState([])
    const [selected, setSelected] = useState({})

    useEffect(() => {
        getDenom()
    }, [])

    const onComplete = (pin) => {
        setModalPin(false)
        setTimeout(() => {
            pay(pin)
        }, 500)
    }

    const parseObjToArray = (v) => {
        const arr = []
        Object.keys(v).map(key => {
            arr.push({ name: [key], value: v[key] })
        })
        return arr
    }

    const getDenom = () => {
        setLoadingDenom(true)
        axios.get(`/public/provider?name=PLN`)
            .finally(() => {
                setLoadingDenom(false)
            })
            .then(res => {
                setDenom(res.data.data.denom)
            })
    }

    const pay = () => {
        setLoading(true)
        const segmentPower = data.segment_power.split(' /')
        axios.post(`/transaction-ppob/topup`,
            {
                customer_no: data.customer_no,
                denom_id: selected.id,
                type: "PLN",
                customer_name: data.name,
                tarif: segmentPower[0],
                voltage: segmentPower[1]
            }
            // ,
            // {
            //     headers: { 'Authorization-Pin': pin }
            // }
        ).finally(() => {
            setLoading(false)
        }).then(res => {
            props.navigation.navigate('InvoicePln', { data: res.data.data, product: selected, type: 'PREPAID' })
            props.dispatch(getMerchant())
        })
    }


    const renderData = ({ item }) => {
        return (
            <View style={{ flexDirection: 'row', paddingVertical: 5 }}>
                <View style={{ flex: 1 }}>
                    <Text style={styles.name}>{item.name}</Text>
                </View>
                <View style={{ flex: 2 }}>
                    <Text style={[styles.value]}>{item.value}</Text>
                </View>
            </View>
        )
    }

    return (
        <View style={{ flex: 1, backgroundColor: color.bglight }}>
            <View style={{ flex: 1 }}>
                <Text style={styles.title}>Detail Transaksi</Text>
                <View style={{ padding: 10 }}>
                    <FlatList
                        data={parseObjToArray(data)}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={renderData} />
                </View>
                <Divider />
                <Text style={styles.title}>Pilih Nominal</Text>
                <SkeletonContent
                    containerStyle={styles.skeletonContainer}
                    isLoading={loadingDenom}
                    layout={[styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton]}
                >
                    {
                        <Denom data={denom} onSelect={(v) => setSelected(v)} selected={selected} />
                    }
                </SkeletonContent>
                <Button title='Bayar' loading={loading} disabled={loading || !selected.id} style={{ margin: 10, marginBottom: 20 }} onPress={() => pay()} />
            </View>
            {/* <ModalPin visible={modalPin} toggle={() => setModalPin(false)} onComplete={onComplete} /> */}
        </View>
    );
}

const { width } = Dimensions.get('screen')

const styles = StyleSheet.create({
    title: {
        margin: 10,
        fontWeight: 'bold',
        color: color.g900
    },
    name: {
        color: color.g700,
        marginBottom: 5
    },
    flexRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        margin: 5
    },
    value: {
        color: color.g700,
        fontWeight: 'bold',
        textAlign: 'right',
        marginBottom: 5
    },
    skeleton: {
        width: width / 2 - 10,
        height: 60,
        margin: 5
    },
    skeletonContainer: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
    }
})

const mapStateToProps = state => {
    return {
        user: state.user.data,
    }
}

export default connect(mapStateToProps)(CheckoutPln);