import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { useBackHandler } from '@react-native-community/hooks';
import { color, style } from '_styles';
import { Button, HeaderTransparent } from '_atoms';
import { ModalScene } from '_molecules';
import { autoCurency } from '_utils';
import moment from 'moment';
import 'moment/min/locales';
import axios from 'axios';
import { FlatList, TouchableHighlight, TouchableOpacity } from 'react-native-gesture-handler';

const DetailTlMerchant = (props) => {

    const { data, payment } = props.route.params
    console.log({ data })
    // console.log({ payment })
    const [norek, setNorek] = useState({})
    console.log({ norek })
    const [isVisible, setIsVisible] = useState(false)

    useBackHandler(() => {
        props.navigation.popToTop()
        return true
    })

    const Accepted = () => {
        axios.post(`/transaction/status/process/${data.id}`)
            .then(res => {
                alert('pesanan kamu terima, silakan proses pesanan untuk pengiriman')
                props.navigation.popToTop()
            })
    }

    const AnAccepted = () => {
        axios.post(`/transaction/status/reject/${data.id}`)
            .then(res => {
                alert('pesanan kamu tolak')
                props.navigation.popToTop()
            })
    }

    const sendProduct = () => {
        props.navigation.navigate('AirwayBill', { data: data })
    }

    return (
        <>
            <HeaderTransparent title={data.status} />
            <View style={styles.container}>
                {
                    data.status == 'REJECTED' ?
                        <Icon name='close-circle-outline' color={color.failed} size={100} />
                        :
                        <Icon name='checkmark-circle-outline' color={color.success} size={100} />
                }
                <Text style={[styles.title, { color: data.status == 'REJECTED' ? color.failed : color.success }]}>{data.status}</Text>
                {
                    data.status == 'WAITING_PAYMENT' &&
                    <>
                        <Text style={styles.desc}>Lanjutkan pembayaran dan kirim foto bukti pembayaran. "Untuk Keamanan, jangan menunjukan bukti pembayaran kepihak manapun selain pihak PadiRaharja melalui aplikasi"</Text>
                        <Text>Total Pembayaran : {autoCurency((parseInt(data.amount) + parseInt(data.unique_code)))}</Text>
                    </>
                }
                {
                    data.status == 'WAITING_PAYMENT' &&
                    <View style={{ padding: 5, borderColor: color.secondary, borderWidth: 2, borderRadius: 5, marginVertical: 10, width: width - 10 }}>
                        <Text>Segera Lakukan Pembayaran sebelum :  </Text>
                        <Text>       {moment(data.created_at).locale('id').add(3, 'hours').format('dddd, DD MMMM YYYY, HH:mm')}</Text>
                        <Text style={{ marginTop: 10 }}>Lakukan Pembayaran ke : </Text>
                        <View style={{ padding: 10, borderRadius: 5, borderColor: color.primary, borderWidth: 1, margin: 5 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text>Bank</Text>
                                <Text>{data.payment_method.bank_name}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text>Nomer Rekenig</Text>
                                <Text>{data.payment_method.account_number}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text>Atas Nama </Text>
                                <Text>{data.payment_method.account_name}</Text>
                            </View>
                        </View>
                    </View>
                }
                {
                    data.status == 'REQUESTED' &&
                    <View style={{ width: width - 10 }}>
                        <View style={{ borderColor: color.secondary, borderWidth: 2, borderRadius: 5, padding: 5, marginTop: 10 }}>
                            <Text>Pesanan Masuk: </Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>id Transaksi</Text>
                                <Text>{data.id}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Nama Pembeli</Text>
                                <Text>{data.user.name}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>No Telpon Pembeli</Text>
                                <Text>{data.user.phone}</Text>
                            </View>
                            <View style={{ padding: 5 }}>
                                <Text>Alamat Pengiriman :</Text>
                                <View style={{ padding: 5, borderRadius: 5, borderColor: color.primary, borderWidth: 1 }}>
                                    <Text>{data.user.address} , kecamatan : {data.destination_details.subdistrict_name}, Kabupaten/Kota : {data.destination_details.city}, propinsi : {data.destination_details.province}</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Nama Produk</Text>
                                <Text>{data.detail[0].product.name}</Text>

                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Jumlah Beli</Text>
                                <Text>{data.detail[0].quantity}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Harga Produk</Text>
                                <Text>{autoCurency(data.detail[0].product.price)}</Text>

                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Ongkos Kirim</Text>
                                <Text>{autoCurency(data.postal_fee)}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Total Pembayaran</Text>
                                <Text>{autoCurency(data.amount)}</Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', padding: 10, justifyContent: 'space-between', width: 280, alignSelf: 'center' }}>
                            < Button title="Tolak Pesanan" onPress={() => AnAccepted()} />
                            < Button title="Terima Pesanan" onPress={() => Accepted()} />
                        </View>
                    </View>
                }
                {
                    data.status == 'PROCESSED' &&
                    <View style={{ width: width - 10 }}>
                        <View style={{ borderColor: color.secondary, borderWidth: 2, borderRadius: 5, padding: 5, marginTop: 10 }}>
                            <Text>Pesanan Diproses: </Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>id Transaksi</Text>
                                <Text>{data.id}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Nama Pembeli</Text>
                                <Text>{data.user.name}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>No Telpon Pembeli</Text>
                                <Text>{data.user.phone}</Text>
                            </View>
                            <View style={{ padding: 5 }}>
                                <Text>Alamat Pengiriman :</Text>
                                <View style={{ padding: 5, borderRadius: 5, borderColor: color.primary, borderWidth: 1 }}>
                                    <Text>{data.user.address} , kecamatan : {data.destination_details.subdistrict_name}, Kabupaten/Kota : {data.destination_details.city}, propinsi : {data.destination_details.province}</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Nama Produk</Text>
                                <Text>{data.detail[0].product.name}</Text>

                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Jumlah Beli</Text>
                                <Text>{data.detail[0].quantity}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Harga Produk</Text>
                                <Text>{autoCurency(data.detail[0].product.price)}</Text>

                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Ongkos Kirim</Text>
                                <Text>{autoCurency(data.postal_fee)}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Total Pembayaran</Text>
                                <Text>{autoCurency(data.amount)}</Text>
                            </View>
                        </View>
                        <View style={{ padding: 10, width: 280, alignSelf: 'center' }}>
                            < Button title="Kirim Pesanan" onPress={() => sendProduct()} />
                        </View>
                    </View>
                }
                {
                    data.status == 'DELIVERING' &&
                    <View style={{ width: width - 10 }}>
                        <View style={{ borderColor: color.secondary, borderWidth: 2, borderRadius: 5, padding: 5, marginTop: 10 }}>
                            <Text>Pesanan Sedang Diantar: </Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>id Transaksi</Text>
                                <Text>{data.id}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Nama Pembeli</Text>
                                <Text>{data.user.name}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>No Telpon Pembeli</Text>
                                <Text>{data.user.phone}</Text>
                            </View>
                            <View style={{ padding: 5 }}>
                                <Text>Alamat Pengiriman :</Text>
                                <View style={{ padding: 5, borderRadius: 5, borderColor: color.primary, borderWidth: 1 }}>
                                    <Text>{data.user.address} , kecamatan : {data.destination_details.subdistrict_name}, Kabupaten/Kota : {data.destination_details.city}, propinsi : {data.destination_details.province}</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Nama Produk</Text>
                                <Text>{data.detail[0].product.name}</Text>

                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Jumlah Beli</Text>
                                <Text>{data.detail[0].quantity}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Harga Produk</Text>
                                <Text>{autoCurency(data.detail[0].product.price)}</Text>

                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Ongkos Kirim</Text>
                                <Text>{autoCurency(data.postal_fee)}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Total Pembayaran</Text>
                                <Text>{autoCurency(data.amount)}</Text>
                            </View>
                        </View>
                    </View>
                }
                {
                    data.status == 'FINISHED' &&
                    <View style={{ width: width - 10 }}>
                        <View style={{ borderColor: color.secondary, borderWidth: 2, borderRadius: 5, padding: 5, marginTop: 10 }}>
                            <Text>Pesanan Telah sampai: </Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>id Transaksi</Text>
                                <Text>{data.id}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Nama Pembeli</Text>
                                <Text>{data.user.name}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>No Telpon Pembeli</Text>
                                <Text>{data.user.phone}</Text>
                            </View>
                            <View style={{ padding: 5 }}>
                                <Text>Alamat Pengiriman :</Text>
                                <View style={{ padding: 5, borderRadius: 5, borderColor: color.primary, borderWidth: 1 }}>
                                    <Text>{data.user.address} , kecamatan : {data.destination_details.subdistrict_name}, Kabupaten/Kota : {data.destination_details.city}, propinsi : {data.destination_details.province}</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Nama Produk</Text>
                                <Text>{data.detail[0].product.name}</Text>

                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Jumlah Beli</Text>
                                <Text>{data.detail[0].quantity}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Harga Produk</Text>
                                <Text>{autoCurency(data.detail[0].product.price)}</Text>

                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Ongkos Kirim</Text>
                                <Text>{autoCurency(data.postal_fee)}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Total Pembayaran</Text>
                                <Text>{autoCurency(data.amount)}</Text>
                            </View>
                        </View>
                    </View>
                }
                {
                    data.status == 'REJECTED' &&
                    <View style={{ width: width - 10 }}>
                        <View style={{ borderColor: color.secondary, borderWidth: 2, borderRadius: 5, padding: 5, marginTop: 10 }}>
                            <Text>Pesanan Anda Batalkan </Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>id Transaksi</Text>
                                <Text>{data.id}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Nama Pembeli</Text>
                                <Text>{data.user.name}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>No Telpon Pembeli</Text>
                                <Text>{data.user.phone}</Text>
                            </View>
                            <View style={{ padding: 5 }}>
                                <Text>Alamat Pengiriman :</Text>
                                <View style={{ padding: 5, borderRadius: 5, borderColor: color.primary, borderWidth: 1 }}>
                                    <Text>{data.user.address} , kecamatan : {data.destination_details.subdistrict_name}, Kabupaten/Kota : {data.destination_details.city}, propinsi : {data.destination_details.province}</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Nama Produk</Text>
                                <Text>{data.detail[0].product.name}</Text>

                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Jumlah Beli</Text>
                                <Text>{data.detail[0].quantity}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Harga Produk</Text>
                                <Text>{autoCurency(data.detail[0].product.price)}</Text>

                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Ongkos Kirim</Text>
                                <Text>{autoCurency(data.postal_fee)}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                <Text>Total Pembayaran</Text>
                                <Text>{autoCurency(data.amount)}</Text>
                            </View>
                        </View>
                    </View>
                }




            </View>
        </>
    )
}
const status = [
    {
        id: '1',
        stat: 'Pesanan Diproses',
    },
    {
        id: '2',
        stat: 'Pesanan Dikirim',
    },
    {
        id: '3',
        stat: 'Batalkan Pesanan',
    }
]
const { width, height } = Dimensions.get('screen')
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        color: color.success
    },
    desc: {
        color: color.g700,
        textAlign: 'center',
        marginBottom: 20
    },
    footer: {
        padding: 10,
        paddingBottom: 20
    }
})

export default DetailTlMerchant;