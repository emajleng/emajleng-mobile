import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, Dimensions, View, TouchableOpacity, ScrollView, Alert, FlatList, Image } from 'react-native';
import { Button, HeaderTransparent, Input, EmptyList } from '_atoms'
import { color, style } from '_styles';
import { Loading } from '_molecules';
import { autoCurency } from '_utils';
import axios from 'axios';

const TransactionUser = (props) => {

    const status = props.route.params.status
    console.log({ status })
    const [product, setProduct] = useState([])
    const [isLoading, setisLoading] = useState(false)
    const [toko, setToko] = useState({})

    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', () => getProduct())
        return unsubscribe;
    }, [props.navigation])

    const getProduct = () => {
        setisLoading(true)
        axios.get(`/transaction?status=${status}`)
            .finally(() => { setisLoading(false) })
            .then(res => {
                console.log({ res })
                setProduct(res.data.data)
            })
    }


    const renderProduct = ({ item }) => {
        const tgl = (item.created_at).split('T')
        return (
            <TouchableOpacity style={[styles.productList, style.shadow]} onPress={() => props.navigation.navigate('DetailsTlUser', { data: item, status: status })}>
                <View>
                    <Text style={styles.productNameText}>{item.status}</Text>
                    <Text style={styles.tglText}>{tgl[0]}</Text>
                    <Text style={styles.priceText}>Total Bayar : {autoCurency(item.amount)}</Text>
                    <Text style={styles.text}>Ongkir : {autoCurency(item.postal_fee)}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    return (
        <>
            <HeaderTransparent title='Riwayat Transaksi' />
            <ScrollView style={style.container}>

                {
                    !toko ?
                        <View style={{ padding: 20 }}>
                            <View style={{ borderColor: color.failed, marginTop: 20, borderWidth: 2, width: 200, padding: 5, alignSelf: 'center', marginVertical: 5, borderRadius: 5 }}>
                                <Text>Anda belum Membuat Alamat Pengiriman, Silakan Buat sekarang !</Text>
                            </View>
                            <Button title="Buat Alamat Pengiriman" onPress={() => props.navigation.navigate('AddOriginAddress')} />
                        </View>
                        :
                        <View style={[style.body, { paddingTop: 10 }]}>
                            <FlatList
                                data={product}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={renderProduct}
                            />
                        </View>
                }
                {
                    product == '' &&
                    <EmptyList title='Opsss!' description='Riwayat Transaksi Tokomu Kosong' />
                }
                <Loading isLoading={isLoading} />
            </ScrollView>
        </>
    )
}

const { width, height } = Dimensions.get('screen')
const styles = StyleSheet.create({
    header: {
        backgroundColor: color.primary,
        height: Dimensions.get('screen').height / 8,
        justifyContent: 'center',
        paddingTop: Platform.OS == 'ios' && isIphoneX() ? 20 : 0
    },
    productList: {
        flexDirection: 'row',
        width: width - 20,
        marginVertical: 5,
        padding: 10,
        alignSelf: 'center'
    },
    productNameText: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    tglText: {
        fontSize: 10,
        color: color.g600,
        marginBottom: 2
    },
    priceText: {
        color: color.secondary,
        marginBottom: 2
    },
    text: {
        fontSize: 12,
        color: color.g600
    }
})

export default TransactionUser;