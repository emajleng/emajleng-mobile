import React, { useEffect, useState } from 'react';
import { View, StyleSheet, TouchableOpacity, Image, Dimensions, Alert } from 'react-native';
import { Button, HeaderTransparent, ButtonText } from '_atoms';
import ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/Ionicons';
import { color, style } from '_styles';
import axios from 'axios';
import { Loading } from '_molecules';

const Confirmation = (props) => {
    const { data, title } = props.route.params
    console.log({ data })
    const [image, setImage] = useState({})
    const [isLoading, setIsLoading] = useState(false)

    const confirmation = () => {
        const formData = new FormData()
        if (image.uri) {
            formData.append('photo', image)
        }
        setIsLoading(true)
        axios.post(`/transaction/status/confirmation/${data.id}`, formData)
            .finally(() => { setIsLoading(false) })
            .then(res => {
                console.log({ res })
                setIsLoading(false)
                Alert.alert('Suksess', 'Bukti Pembayaran sudah dikirim', [
                    {
                        text: 'OK',
                        onPress: () => props.navigation.popToTop()
                    }
                ])
            })
    }

    const selectImage = (type) => {
        const options = {
            quality: 0.8,
            maxWidth: 1280,
            maxHeight: 1280,
            storageOptions: {
                skipBackup: true
            }
        }
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
                console.log('caceled')
            } else if (response.error) {
                Toast.show(response.error, Toast.LONG)
                console.log('ImagePicker Error: ', response.error);
            } else {
                console.log({ response })
                let filename = response.uri.split('/').pop();
                // const img = [...image, { uri: response.uri, name: response.fileName ?? filename, type: response.type }]
                setImage({ uri: response.uri, name: response.fileName ?? filename, type: response.type })
                // console.log({ img })
            }
        })
    }

    const deleteImage = item => {
        const img = image.filter(x => x.uri != item.uri)
        setImage(img)
    }

    return (
        <>
            < HeaderTransparent title="Konfirmasi Pembayaran" />
            <View style={{ padding: 20 }}>
                <View style={{ height: 200, padding: 30, alignItems: 'center' }}>
                    <ButtonText title="Pilih Foto Bukti Pembayaran" color={color.primary} onPress={() => selectImage()} />
                    <TouchableOpacity style={styles.delete} onPress={() => deleteImage(item)}>
                        <Icon name='close' size={20} />
                    </TouchableOpacity>
                    <Image source={{ uri: image.uri }} style={{ width: 80, height: 80, marginRight: 5 }} />
                </View>
                <Button title="Kirim Butki pembayaran" onPress={() => confirmation()} />
                <Loading isLoading={isLoading} />
            </View>
        </>
    )
}


const styles = StyleSheet.create({
    header: {
        backgroundColor: color.primary,
        height: Dimensions.get('screen').height / 8,
        justifyContent: 'center',
        paddingTop: Platform.OS == 'ios' && isIphoneX() ? 20 : 0
    },
    body: {
        paddingTop: 10
    }
})

export default Confirmation;