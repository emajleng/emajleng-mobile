import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, Dimensions, View, TouchableOpacity, ScrollView, Alert, FlatList, Image } from 'react-native';
import { Button, HeaderTransparent, Input, EmptyList } from '_atoms'
import { color, style } from '_styles';
import { autoCurency } from '_utils';
import { Loading } from '_molecules';
import axios from 'axios';

const TransactionMerchant = (props) => {

    const { status, id } = props.route.params
    const [product, setProduct] = useState([])
    console.log({ product })
    const [isLoading, setisLoading] = useState(false)
    const [toko, setToko] = useState({})

    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', () => getProduct(), getOriginAddress())
        return unsubscribe;
    }, [props.navigation])

    const getOriginAddress = () => {
        setisLoading(true)
        axios.get(`/raja-ongkir/origin-address`)
            .finally(() => setisLoading(false))
            .then(res => { setToko(res.data.data) })
            .catch(err => { setToko(null) })
    }

    const getProduct = () => {
        setisLoading(true)
        axios.get(`/transaction/merchant?status=${status}`).then(res => {
            console.log({ res })
            setProduct(res.data.data)
            setisLoading(false)
        })
    }

    // const getProduct = () => {
    //     setisLoading(true)
    //     axios.get(`/transaction/merchant/${status}/${id}`).then(res => {
    //         console.log({ res })
    //         setProduct(res.data.data)
    //         setisLoading(false)
    //     })
    // }

    const renderProduct = ({ item }) => {
        const tgl = (item.created_at).split('T')
        return (
            <View style={[style.shadow, { marginVertical: 10, flexDirection: 'row', width: width - 25 }]}>
                <TouchableOpacity style={[styles.productList]} onPress={() => props.navigation.navigate('DetailTlMerchant', { data: item })}>
                    {/* <Image source={{ uri: item.photo }} style={{ width: 80, height: 90, marginRight: 5 }} /> */}
                    <View>
                        <Text style={styles.productNameText}>{item.status}</Text>
                        <Text style={styles.tglText}>{tgl[0]}</Text>
                        <Text style={styles.priceText}>Total Bayar : {autoCurency(item.amount)}</Text>
                        <Text style={styles.text}>Ongkir : {autoCurency(item.postal_fee)}</Text>
                        {/* <Text style={styles.text}>Produk : {item.Detail.map(val => val.name)}</Text> */}
                    </View>
                </TouchableOpacity>
                {/* <ButtonText title={"Hapus Produk"} color={color.failed} onPress={() => deleteProduct(item.id)} /> */}
            </View>
        )
    }

    return (
        <>
            <HeaderTransparent title='Riwayat Transaksi' />
            <ScrollView style={style.container}>

                {
                    !toko ?
                        <View style={{ padding: 20 }}>
                            <View style={{ borderColor: color.failed, marginTop: 20, borderWidth: 2, width: 200, padding: 5, alignSelf: 'center', marginVertical: 5, borderRadius: 5 }}>
                                <Text>Anda belum Membuat Alamat Pengiriman, Silakan Buat sekarang !</Text>
                            </View>
                            <Button title="Buat Alamat Pengiriman" onPress={() => props.navigation.navigate('AddOriginAddress')} />
                        </View>
                        :
                        <View style={[style.body, { padding: 10 }]}>
                            <FlatList
                                data={product}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={renderProduct}
                            />
                        </View>
                }
                {
                    product == '' &&
                    <EmptyList title='Opsss!' description='Riwayat Transaksi Tokomu Kosong' />
                }
                <Loading isLoading={isLoading} />
            </ScrollView>
        </>
    )
}

const { width, height } = Dimensions.get('screen')
const styles = StyleSheet.create({
    header: {
        backgroundColor: color.primary,
        height: Dimensions.get('screen').height / 8,
        justifyContent: 'center',
        paddingTop: Platform.OS == 'ios' && isIphoneX() ? 20 : 0
    },
    productList: {
        flexDirection: 'row',
        width: width - 130,
    },
    productNameText: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    tglText: {
        fontSize: 10,
        color: color.g600,
        marginBottom: 2
    },
    priceText: {
        color: color.secondary,
        marginBottom: 2
    },
    text: {
        fontSize: 12,
        color: color.g600
    }
})

export default TransactionMerchant;