import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, Dimensions, ScrollView } from 'react-native';
import axios from 'axios';
import { Button, Divider, Card } from '_atoms';
import { color } from '_styles';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
// import { ModalPin } from '_organisms'
import { autoCurency } from '_utils';
import { connect } from 'react-redux';
// import { getMerchant } from '_states/actions/user';
import moment from 'moment';
import 'moment/min/locales';

const CheckoutPostpaid = (props) => {

    const { data } = props.route.params
    const [loading, setLoading] = useState(false)
    const [loadingDenom, setLoadingDenom] = useState(false)
    const [modalPin, setModalPin] = useState(false)
    const [denom, setDenom] = useState([])

    const benefit = (data.selling_price - data.price) / data.desc.lembar_tagihan

    useEffect(() => {
        getDenom()
    }, [])

    const onComplete = (pin) => {
        setModalPin(false)
        setTimeout(() => {
            pay(pin)
        }, 500)
    }

    const getDenom = () => {
        setLoadingDenom(true)
        axios.get(`/public/provider?name=PLN Postpaid`)
            .finally(() => {
                setLoadingDenom(false)
            })
            .then(res => {
                setDenom(res.data.data.denom[0])
            })
    }

    const pay = () => {
        setLoading(true)
        axios.post(`/transaction-ppob/pasca`,
            {
                code: 'pln',
                customer_no: data.customer_no,
                denom_id: denom.id,
                type: "PAYMENT",
            }
            // ,
            // {
            //     headers: { 'Authorization-Pin': pin }
            // }
        ).finally(() => {
            setLoading(false)
        }).then(res => {
            console.log({ res })
            props.navigation.navigate('InvoicePln', { data: res.data.data, amount: countAmount(), type: 'POSTPAID', item: data })
            // props.dispatch(getMerchant())
        })
    }

    const countAmount = () => {
        let amount = 0
        data.desc.detail.map(v => {
            amount += parseInt(v.nilai_tagihan) + parseInt(v.denda) + (parseInt(v.admin) - benefit) + denom.admin_fee
        })
        return amount
    }

    const renderData = ({ item }) => {
        return (
            <View style={{ flexDirection: 'row', paddingVertical: 5, borderBottomWidth: 1, borderColor: color.g500 }}>
                <View style={{ flex: 1 }}>
                    <Text style={styles.name}>Periode</Text>
                    <Text style={styles.name}>Tagihan</Text>
                    <Text style={styles.name}>Biaya Admin</Text>
                    <Text style={styles.name}>Denda</Text>
                </View>
                <View style={{ flex: 0 }}>
                    <Text style={[styles.value]}>{moment(item.periode, 'YYYYMM').locale('id').format('MMMM YYYY')}</Text>
                    <Text style={[styles.value]}>{autoCurency(item.nilai_tagihan)}</Text>
                    <Text style={[styles.value]}>{autoCurency((parseInt(item.admin) - benefit) + denom.admin_fee)}</Text>
                    <Text style={[styles.value]}>{autoCurency(item.denda)}</Text>
                </View>
            </View>
        )
    }

    return (
        <View style={{ flex: 1, backgroundColor: color.bglight }}>
            <ScrollView style={{ flex: 1 }}>
                <Text style={styles.title}>Detail Transaksi</Text>
                <SkeletonContent
                    containerStyle={styles.skeletonContainer}
                    isLoading={loadingDenom}
                    layout={[styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton]}
                >
                    <View style={{ flexDirection: 'row', padding: 10 }}>
                        <View style={{ flex: 1 }}>
                            <Text style={styles.name}>ID Ref</Text>
                            <Text style={styles.name}>Nama Pelanggan</Text>
                            <Text style={styles.name}>Nomor Meter</Text>
                            <Text style={styles.name}>Tarif</Text>
                            <Text style={styles.name}>Daya</Text>
                            <Text style={styles.name}>Lembar Tagihan</Text>
                        </View>
                        <View style={{ flex: 2 }}>
                            <Text style={[styles.value]}>{data.ref_id}</Text>
                            <Text style={[styles.value]}>{data.customer_name}</Text>
                            <Text style={[styles.value]}>{data.customer_no}</Text>
                            <Text style={[styles.value]}>{data.desc.tarif}</Text>
                            <Text style={[styles.value]}>{data.desc.daya}</Text>
                            <Text style={[styles.value]}>{data.desc.lembar_tagihan}</Text>
                        </View>
                    </View>
                </SkeletonContent>
                <Divider />
                <Text style={styles.title}>Detail Tagian</Text>
                <SkeletonContent
                    containerStyle={styles.skeletonContainer}
                    isLoading={loadingDenom}
                    layout={[styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton]}
                >
                    <View style={{ padding: 10 }}>
                        <FlatList
                            data={data.desc.detail}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={renderData}
                        />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <Text style={[styles.name, { fontSize: 16 }]}>Total Pembayaran</Text>
                            </View>
                            <View style={{ flex: 0 }}>
                                <Text style={[styles.value, { fontSize: 16 }]}>{autoCurency(countAmount())}</Text>
                            </View>
                        </View>
                    </View>
                </SkeletonContent>
            </ScrollView>
            <View style={styles.footer}>
                <Button title='Bayar' loading={loading} disabled={loading} onPress={() => pay()} />
            </View>
            {/* <ModalPin visible={modalPin} toggle={() => setModalPin(false)} onComplete={onComplete} /> */}
        </View>
    );
}

const { width } = Dimensions.get('screen')

const styles = StyleSheet.create({
    title: {
        margin: 10,
        fontWeight: 'bold',
        color: color.g900
    },
    name: {
        color: color.g700,
        marginBottom: 5
    },
    flexRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        margin: 5
    },
    value: {
        color: color.g700,
        fontWeight: 'bold',
        textAlign: 'right',
        marginBottom: 5
    },
    skeleton: {
        width: width - 10,
        height: 17.5,
        margin: 5
    },
    skeletonContainer: {
        flex: 1,
    },
    footer: {
        padding: 10
    }
})

const mapStateToProps = state => {
    return {
        user: state.user.data,
    }
}

export default connect(mapStateToProps)(CheckoutPostpaid);