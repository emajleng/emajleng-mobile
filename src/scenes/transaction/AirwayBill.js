import React, { useEffect, useState } from 'react';
import { View, StyleSheet, TouchableOpacity, Image, Dimensions, Alert } from 'react-native';
import { Button, HeaderTransparent, ButtonText, Input } from '_atoms';
import ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/Ionicons';
import { color, style } from '_styles';
import axios from 'axios';
import { Loading } from '_molecules';

const AirwayBill = (props) => {
    const { data, title } = props.route.params
    console.log({ data })
    const [isLoading, setIsLoading] = useState(false)
    const [airwaybill, setAirwayBill] = useState({})

    const confirmation = () => {

        setIsLoading(true)
        axios.post(`/transaction/status/deliver/${data.id}`, { airway_bill: airwaybill })
            .finally(() => { setIsLoading(false) })
            .then(res => {
                console.log({ res })
                setIsLoading(false)
                Alert.alert('Sukses', 'Nomor Resi Pengiriman sudah dikirim, Pastikan Pembeli Mengkonfirmasi barang telah diterima', [
                    {
                        text: 'OK',
                        onPress: () => props.navigation.popToTop()
                    }
                ])
            })
    }

    return (
        <>
            < HeaderTransparent title="Konfirmasi Pengiriman" />
            <View style={{ padding: 20 }}>
                <View style={{ height: 200, padding: 30, alignItems: 'center' }}>
                    <Input
                        label='Nomor Resi Pengiriman '
                        iconLeft='mail-sharp'
                        placeholder='123xxxxxxxx'
                        onChangeText={(airwaybill => setAirwayBill(v => ({ ...v, airwaybill })))}
                    />
                </View>
                <Button title="Kirim Nomor Resi Pengiriman" onPress={() => confirmation()} />
                <Loading isLoading={isLoading} />
            </View>
        </>
    )
}


const styles = StyleSheet.create({
    header: {
        backgroundColor: color.primary,
        height: Dimensions.get('screen').height / 8,
        justifyContent: 'center',
        paddingTop: Platform.OS == 'ios' && isIphoneX() ? 20 : 0
    },
    body: {
        paddingTop: 10
    }
})

export default AirwayBill;