import React, { useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { useBackHandler } from '@react-native-community/hooks';
import { color } from '_styles';
import { Button } from '_atoms';
import { SendWhatsApp } from '_organisms';
import { autoCurency, saveToClipboard } from '_utils';

const InvoicePln = (props) => {

    const { product, data, type, amount, item } = props.route.params
    const [whatsAppModal, setWhatsAppModal] = useState(false)
    const isPrepaid = type == 'PREPAID'

    useBackHandler(() => {
        props.navigation.popToTop()
        return true
    })

    return (
        <>
            <View style={styles.container}>
                <Icon name='checkmark-circle-outline' color={color.success} size={100} />
                <Text style={styles.title}>Transaksi Berhasil.</Text>
                {
                    isPrepaid ?
                        <Text style={styles.desc}>{`Pembelian token PLN ${product.amount} seharga ${autoCurency(product.price + product.admin_fee)} ${data.status}. Terimakasih telah menggunakan layanan kami :)`}</Text> :
                        <Text style={styles.desc}>{`Pembayaran tagihan PLN senilai ${autoCurency(amount)} ${data.status}. Terimakasih telah menggunakan layanan kami :)`}</Text>
                }
                <View style={{ flexDirection: 'row', paddingVertical: 5 }}>
                    <View style={{ flex: 1 }}>
                        <Text style={styles.name}>Ref ID</Text>
                        {!isPrepaid && <Text style={styles.name}>Nama Pelanggan</Text>}
                        <Text style={styles.name}>ID Pel </Text>
                        <Text style={styles.name}>Pesan</Text>
                        {isPrepaid && <Text style={styles.name}>Nomor Token</Text>}
                    </View>
                    <View style={{ flex: 2 }}>
                        <Text style={[styles.value]}>{data.ref_id}</Text>
                        {!isPrepaid && <Text style={[styles.value]}>{item.customer_name}</Text>}
                        <Text style={[styles.value]}>{data.customer_no}</Text>
                        <Text style={[styles.value]}>{data.message}</Text>
                        {isPrepaid &&
                            <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'flex-end' }} onPress={() => saveToClipboard(data.sn)}>
                                <Text style={[styles.value, { marginRight: 5 }]}>{data.sn}</Text>
                                <Icon name='copy-outline' size={16} />
                            </TouchableOpacity>
                        }
                    </View>
                </View>
            </View>
            <View style={styles.footer}>
                {isPrepaid && <Button title='Kirim ke WhatsApp' icon='logo-whatsapp' outline color={color.success} onPress={() => setWhatsAppModal(true)} />}
                <Button outline color={color.secondary} title='Kembali' onPress={() => props.navigation.popToTop()} />
            </View>
            <SendWhatsApp isVisible={whatsAppModal} toggle={() => setWhatsAppModal(false)} data={data} />
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        backgroundColor: '#fff'
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        color: color.success
    },
    desc: {
        color: color.g700,
        textAlign: 'center'
    },
    footer: {
        padding: 10,
        paddingBottom: 20,
        backgroundColor: '#fff'
    },
    name: {
        color: color.g700,
        marginBottom: 5
    },
    value: {
        color: color.g700,
        fontWeight: 'bold',
        textAlign: 'right',
        marginBottom: 5
    },
})

export default InvoicePln;