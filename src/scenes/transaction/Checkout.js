import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import axios from 'axios';
import { Button, Divider } from '_atoms';
import { color } from '_styles';
// import { ModalPin } from '_organisms';
import { autoCurency } from '_utils';
import { connect } from 'react-redux';
// import { getMerchant } from '_states/actions/user';

const Checkout = (props) => {

    const { phoneNumber, product, provider, item } = props.route.params
    const [loading, setLoading] = useState(false)
    // const [modalPin, setModalPin] = useState(false)

    const onComplete = (pin) => {
        setModalPin(false)
        setTimeout(() => {
            pay(pin)
        }, 500)
    }

    const pay = () => {
        setLoading(true)
        axios.post(`/transaction-ppob/topup`,
            {
                customer_no: phoneNumber,
                denom_id: product.id,
                type: item.route
            }
            // ,
            // {
            //     headers: { 'Authorization-Pin': pin }
            // }
        ).finally(() => {
            setLoading(false)
        }).then(res => {
            props.navigation.navigate('Invoice', { data: res.data.data, product, provider, item })
            // props.dispatch(getMerchant())
        }).catch(err => {
            console.log({ err })
            alert('Maaf Sedang perbaikan service')
        })
    }


    return (
        <View style={{ flex: 1, padding: 10, backgroundColor: color.bglight }}>
            <View style={{ flex: 1 }}>
                <Text style={{ marginVertical: 10, fontWeight: 'bold', color: color.g900 }}>Detail Transaksi</Text>
                <View style={{ flexDirection: 'row', paddingVertical: 5 }}>
                    <View style={{ flex: 1 }}>
                        <Text style={styles.title}>Jenis Layanan</Text>
                        <Text style={styles.title}>Nomor Tujuan</Text>
                        <Text style={styles.title}>Nominal</Text>
                        <Text style={styles.title}>Harga</Text>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Text style={[styles.value]}>{provider.name}</Text>
                        <Text style={[styles.value]}>{phoneNumber}</Text>
                        <Text style={[styles.value]}>{product.amount}</Text>
                        <Text style={[styles.value]}>{autoCurency(parseInt(product.price) + parseInt(product.admin_fee))}</Text>
                    </View>
                </View>
                <Divider />
            </View>
            <Button title='Bayar' loading={loading} disabled={loading} onPress={() => pay()} />
            {/* <ModalPin visible={modalPin} toggle={() => setModalPin(false)} onComplete={onComplete} /> */}
        </View>
    );
}

const styles = StyleSheet.create({
    title: {
        color: color.g700,
        marginBottom: 5
    },
    value: {
        color: color.g700,
        fontWeight: 'bold',
        textAlign: 'right',
        marginBottom: 5
    },
})

const mapStateToProps = state => {
    return {
        user: state.user.data,
    }
}

export default connect(mapStateToProps)(Checkout);