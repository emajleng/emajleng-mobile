import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { useBackHandler } from '@react-native-community/hooks';
import { color } from '_styles';
import { Button } from '_atoms';
import { autoCurency } from '_utils';

const Invoice = (props) => {

    const { product, provider, item } = props.route.params

    useBackHandler(() => {
        props.navigation.popToTop()
        return true
    })

    return (
        <>
            <View style={styles.container}>
                <Icon name='checkmark-circle-outline' color={color.success} size={100} />
                <Text style={styles.title}>Transaksi Berhasil.</Text>
                {
                    item.type == 'EMONEY' ?
                        <Text style={styles.desc}>{`Pembelian ${item.name} ${product.amount} seharga ${autoCurency(parseInt(product.price) + parseInt(product.admin_fee))} berhasil. Terimakasih telah menggunakan layanan kami :)`}</Text> :
                        <Text style={styles.desc}>{`Pembelian ${item.type} ${provider.name} ${product.amount} seharga ${autoCurency(parseInt(product.price) + parseInt(product.admin_fee))} berhasil. Terimakasih telah menggunakan layanan kami :)`}</Text>
                }
            </View>
            <View style={styles.footer}>
                <Button outline color={color.secondary} title='Kembali' onPress={() => props.navigation.popToTop()} />
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        color: color.success
    },
    desc: {
        color: color.g700,
        textAlign: 'center'
    },
    footer: {
        padding: 10,
        paddingBottom: 20
    }
})

export default Invoice;