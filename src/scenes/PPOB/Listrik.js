import React, { useState, useRef } from 'react';
import { Text, View, StyleSheet, StatusBar, FlatList, Dimensions, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import { Divider } from '_atoms';
// import { WalletCard } from '_molecules';
import { Prepaid, Postpaid } from '_organisms';
// import ViewPager from '@react-native-community/viewpager';
import { color } from '_styles';

const Listrik = (props) => {

    const pagerRef = useRef(null)

    const [selected, setSelected] = useState(props.route.params.item.type)

    const renderMenu = ({ item }) => {
        const isSelected = selected == item.value
        return (
            <View>
                <TouchableHighlight onPress={() => pagerRef.current.setPage(item.value)} underlayColor='transparent' style={[styles.menu, { backgroundColor: isSelected ? color.secondary : '#fff' }]}>
                    <Text style={{ color: isSelected ? '#fff' : color.secondary, fontWeight: 'bold' }}>{item.name}</Text>
                </TouchableHighlight>
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <StatusBar barStyle='dark-content' />
            {/* <WalletCard /> */}
            <Postpaid key='1' />
            {/* <View style={{ flex: 0 }}>
                <FlatList
                    data={menu}
                    keyExtractor={item => item.value.toString()}
                    horizontal
                    renderItem={renderMenu} />
            </View>
            <Divider />
            <ViewPager ref={pagerRef} onPageScroll={v => setSelected(v.nativeEvent.position)} style={styles.viewPager} initialPage={parseInt(selected)} >
                {/* <Postpaid key='2' /> */}
            {/* </ViewPager> */}

        </View>
    )
}

const { width } = Dimensions.get('screen')

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    viewPager: {
        flex: 1
    },
    menu: {
        margin: 10,
        padding: 7,
        borderRadius: 20,
        width: width / 2 - 20,
        alignItems: 'center',
        borderWidth: 2,
        borderColor: color.secondary
    }
})

const menu = [
    {
        name: 'Token',
        value: 0
    },
    {
        name: 'Tagihan',
        value: 1
    }
]

const mapStateToProps = state => {
    return {
        user: state.user.data
    }
}

export default connect(mapStateToProps)(Listrik);