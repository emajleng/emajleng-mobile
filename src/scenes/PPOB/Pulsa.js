import React, { useState } from 'react';
import { View, StyleSheet, StatusBar, Dimensions } from 'react-native';
import Clipboard from '@react-native-community/clipboard';
import { connect } from 'react-redux';
import { InputProvider, Divider, Card, Button, EmptyList } from '_atoms';
import { WalletCard, Denom } from '_molecules';
import { Contact } from '_organisms';
import { autoProvider, autoCorectPhone } from '_utils';
import axios from 'axios';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
// import analytics from '@react-native-firebase/analytics';

const Pulsa = (props) => {

    const [phoneNumber, setPhoneNumber] = useState('')
    const [provider, setProvider] = useState({})
    // console.log({ provider })
    const [selected, setSelected] = useState({})
    const [denom, setDenom] = useState([])
    // console.log({ denom })
    const [data, setData] = useState({})
    // console.log({ data })
    const [loading, setLoading] = useState(false)
    const [modalContact, setModalContact] = useState(false)
    const [undifinedProvider, setUndifinedProvider] = useState(false)
    const item = props.route.params.item
    console.log({ item })

    const handleAutoProvider = async (v) => {
        let phone = autoCorectPhone(v)
        setPhoneNumber(phone)
        const copiedContent = await Clipboard.getString();
        const isPasted = v == copiedContent;
        if (isPasted) {
            handleAutoProviderFull(phone)
            return
        }
        if (phone.length == 4) {
            const data = autoProvider(phone)
            setProvider(data)
            getDenom(data.name)
        } else if (v.length < 4) {
            setProvider({})
        }
    }

    const handleAutoProviderFull = (v) => {
        const data = autoProvider(v)
        setProvider(data ?? {})
        getDenom(data.name)
        setPhoneNumber(v)
    }

    const getDenom = async (provider) => {
        let key = ''
        if (item.route == 'PULSA') {
            key = provider
        } else if (item.type == 'EMONEY') {
            key = item.name
        }
        // await analytics().logEvent('provider', {
        //     name: provider,
        // })
        setUndifinedProvider(false)
        setLoading(true)
        axios.get(`/public/provider?name=${key}`)
            .finally(() => {
                setLoading(false)
            })
            .then(res => {
                console.log({ res })
                const isData = res.data.data.id
                console.log({ isData })
                const denom = isData ? res.data.data.denom : []
                const data = isData ? res.data.data : {}
                setUndifinedProvider(provider ? false : true)
                setDenom(denom)
                setData(data)
            }).catch(error => {
                setUndifinedProvider(true)
                console.log({ error })
            })
    }

    return (
        <View style={styles.container}>
            <StatusBar barStyle='dark-content' />
            {/* <WalletCard /> */}
            <View style={{ padding: 10 }}>
                <InputProvider
                    material
                    keyboardType='phone-pad'
                    value={phoneNumber}
                    onChangeText={v => handleAutoProvider(v)}
                    label='Nomor Telepon'
                    logo={{ uri: data.logo }}
                    onIconPress={() => setModalContact(true)}
                    placeholder='082214XXXXXX' />
            </View>
            <Divider />
            <SkeletonContent
                containerStyle={styles.skeletonContainer}
                isLoading={loading}
                layout={[styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton]}
            >
                {
                    provider.name &&
                    <Denom data={denom} onSelect={(v) => setSelected(v)} selected={selected} />
                }
            </SkeletonContent>
            {
                undifinedProvider && <EmptyList title='Opsss!' description='Sepertinya kamu salah memasukan nomor.' />
            }
            {selected.amount && provider.name && phoneNumber.length > 10 && <Button title='Selanjutnya' style={{ margin: 10, marginBottom: 20 }} onPress={() => props.navigation.navigate('Checkout', { phoneNumber, product: selected, provider, item })} />}
            <Contact isVisible={modalContact} toggle={() => setModalContact(false)} onSelect={v => handleAutoProviderFull(v)} />
        </View>
    )
}

const { width } = Dimensions.get('screen')

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    providerIcon: {
        width: 100,
        height: 40,
        margin: 10
    },
    skeleton: {
        width: width / 2 - 10,
        height: 60,
        margin: 5
    },
    skeletonContainer: {
        flex: 1,
        flexWrap: 'wrap',
    }
})

const mapStateToProps = state => {
    return {
        user: state.user.data
    }
}

export default connect(mapStateToProps)(Pulsa);