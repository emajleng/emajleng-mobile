import React, { useState } from 'react';
import { StyleSheet, Text, TouchableHighlight, View, StatusBar, ScrollView } from 'react-native';
import { Button, ButtonText, Input, HeaderBanner } from '_atoms';
import { color, style } from '_styles'
import { getBrand, getSystemVersion } from 'react-native-device-info'

const ForgotPassword = (props) => {

    const [loading, setLoading] = useState(false)
    const [security, setSecurity] = useState(false)

    const sendEmail = () => {
        props.navigation.navigate('Login')
    }

    const isCeretHidden = () => {
        const brand = getBrand()
        const version = getSystemVersion()
        if (brand.toLocaleUpperCase == 'XIAOMI' && parseFloat(version) >= 10) {
            return true
        }
        return false
    }

    return (
        <ScrollView style={style.container}>
            <StatusBar translucent backgroundColor='transparent' />
            <HeaderBanner />
            <View style={[style.body, { padding: 16 }]}>

                <Text style={{ color: color.g900, fontWeight: 'bold', fontSize: 20 }}>Ubah Password </Text>
                <Text style={{ color: color.g600, paddingVertical: 8 }}>Masukan Password Baru</Text>
                <Input
                    label='Email '
                    iconLeft='mail-outline'
                    keyboardType='email-address'
                    caretHidden={() => isCeretHidden()}
                />
                <Button
                    title='Kirim Reset Password'
                    loading={loading}
                    disable={loading}
                    onPress={sendEmail}
                />
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {

    }
})

export default ForgotPassword;