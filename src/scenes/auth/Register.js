import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View, StatusBar, ScrollView, Alert, Image } from 'react-native';
import { Button, ButtonText, Input, HeaderBanner } from '_atoms';
import { SelectSearch, Loading } from '_molecules';
import { color, style } from '_styles';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/Ionicons';
import { getBrand, getSystemVersion } from 'react-native-device-info'

const Register = (props) => {

    const [loading, setLoading] = useState(false)
    const [security, setSecurity] = useState(true)
    const [isLoading, setisLoading] = useState(false)
    const [email, setEmail] = useState({})
    const [password, setPassword] = useState({})
    const [data, setData] = useState({})
    const [prov, setProv] = useState([])
    const [city, setCity] = useState([])
    const [district, setDistrict] = useState([])
    const [village, setVillage] = useState([])
    const [image, setImage] = useState({})


    useEffect(() => {
        getProv()
    }, [])

    const selectImage = (type) => {
        const options = {
            quality: 0.8,
            maxWidth: 1280,
            maxHeight: 1280,
            storageOptions: {
                skipBackup: true
            }
        }
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
                console.log('caceled')
            } else if (response.error) {
                Toast.show(response.error, Toast.LONG)
                console.log('ImagePicker Error: ', response.error);
            } else {
                console.log({ response })
                let filename = response.uri.split('/').pop();
                // const img = [...image, { uri: response.uri, name: response.fileName ?? filename, type: response.type }]
                setImage({ uri: response.uri, name: response.fileName ?? filename, type: response.type })
                // console.log({ img })
            }
        })
    }

    const deleteImage = item => {
        const img = image.filter(x => x.uri != item.uri)
        setImage(img)
    }

    const getProv = () => {
        setisLoading(true)
        axios.get(`/public/province`)
            .finally(() => setisLoading(false))
            .then(res => {
                console.log({ res })
                setProv(res.data.data.map(({ id: value, name: label }) => ({ value, label })))
            })

    }
    const getCity = (id) => {
        setisLoading(true)
        axios.get(`/public/city/${id}`)
            .finally(() => setisLoading(false))
            .then(res => {
                console.log({ res })
                setCity(res.data.data.map(({ id: value, name: label }) => ({ value, label })))
            })
    }
    const getDistrict = (id) => {
        setisLoading(true)
        axios.get(`/public/district/${id}`)
            .finally(() => setisLoading(false))
            .then(res => {
                console.log({ res })
                setDistrict(res.data.data.map(({ id: value, name: label }) => ({ value, label })))
            })
    }
    const getVillage = (id) => {
        setisLoading(true)
        axios.get(`/public/village/${id}`)
            .finally(() => setisLoading(false))
            .then(res => {
                console.log({ res })
                setVillage(res.data.data.map(({ id: value, name: label }) => ({ value, label })))
            })
    }

    const toRegister = () => {
        setLoading(true)
        const phones = '+62' + data.phone
        const formData = new FormData()
        formData.append('name', data.name)
        formData.append('phone', phones)
        formData.append('email', data.email)
        formData.append('address', data.address)
        formData.append('password', data.password)
        formData.append('password_confirmation', data.password_confirmation)
        formData.append('province_id', data.province_id)
        formData.append('city_id', data.city_id)
        formData.append('village_id', data.village_id)
        formData.append('district_id', data.district_id)
        // formData.append('photo', image)
        axios.post(`/auth/register`, formData)
            .finally(() => {
                setLoading(false)
            })
            .then(res => {
                Alert.alert('Berhasil', 'Selamat, kamu sudah berhasil Mendaftar. Selamat bertransaksi :)', [
                    {
                        text: 'OK',
                        onPress: () => props.navigation.popToTop()
                    }
                ])
            })
    }

    const isCeretHidden = () => {
        const brand = getBrand()
        const version = getSystemVersion()
        if (brand.toLocaleUpperCase == 'XIAOMI' && parseFloat(version) >= 10) {
            return true
        }
        return false
    }

    return (
        <ScrollView style={style.container}>
            <StatusBar translucent backgroundColor='transparent' />
            <HeaderBanner />
            <View style={[style.body, { padding: 16 }]}>

                <Text style={{ color: color.g900, fontWeight: 'bold', fontSize: 20 }}>Silakan Buat Akun</Text>
                <Text style={{ color: color.g600, paddingVertical: 8 }}>Nikmati Berbelanja Produk UMKM dengan mudah dari rumah</Text>
                <Input
                    label='Nama Lengkap'
                    iconLeft='person-outline'
                    caretHidden={() => isCeretHidden()}
                    value={data.name}
                    onChangeText={(name => setData(v => ({ ...v, name })))}
                />
                <Input
                    label='Nomor Telpon'
                    iconLeft='phone-portrait-sharp'
                    keyboardType='phone-pad'
                    prefix='+62'
                    value={data.phone}
                    onChangeText={(phone => phone != '0' && setData(v => ({ ...v, phone })))}
                />
                <Input
                    label='Email '
                    iconLeft='mail-sharp'
                    keyboardType='email-address'
                    onChangeText={(email => setData(v => ({ ...v, email })))}
                />
                <Input
                    label='Kata Sandi'
                    iconLeft='lock-closed-outline'
                    iconRight={security ? 'eye-off-outline' : 'eye-outline'}
                    iconPress={() => setSecurity(!security)}
                    password={security}
                    onChangeText={(password => setData(v => ({ ...v, password })))}
                    subtitle='Kata Sandi minimal 6 karakter'
                />
                <Input
                    label='Konfirmasi Kata Sandi'
                    iconLeft='lock-closed-outline'
                    iconRight={security ? 'eye-off-outline' : 'eye-outline'}
                    iconPress={() => setSecurity(!security)}
                    password={security}
                    onChangeText={(password_confirmation => setData(v => ({ ...v, password_confirmation })))}
                    subtitle='Ulangi Kata Sandi'
                />
                <SelectSearch
                    data={prov}
                    value={data.province_id}
                    label='Provinsi'
                    title='Pilih Provinsi'
                    onSelect={(val => { setData(v => ({ ...v, province_id: val.value })); getCity(val.value) })}
                />
                <SelectSearch
                    data={city}
                    value={data.city_id}
                    label='Kota'
                    title='Pilih Kota'
                    onSelect={(val => { setData(v => ({ ...v, city_id: val.value })); getDistrict(val.value) })}
                />
                <SelectSearch
                    data={district}
                    value={data.district_id}
                    label='Kecamatan'
                    title='Pilih Kecamatan'
                    onSelect={(val => { setData(v => ({ ...v, district_id: val.value })); getVillage(val.value) })}
                />
                <SelectSearch
                    data={village}
                    value={data.village_id}
                    label='Kelurahan'
                    title='Pilih Kelurahan'
                    onSelect={(val => setData(v => ({ ...v, village_id: val.value })))}
                />
                <Input
                    label='Alamat'
                    placeholder='Nama jalan, Nomor Rumah, Rt.Rw'
                    iconLeft='person-outline'
                    onChangeText={(address => setData(v => ({ ...v, address })))}
                />
                {/* <View style={{ height: 200, paddingTop: 20 }}> */}
                {/* <ButtonText title="Upload Surat Izin Usaha" color={color.primary} onPress={() => selectImage()} /> */}
                {/* <TouchableOpacity style={styles.delete} onPress={() => deleteImage(item)}>
                        <Icon name='close' size={20} />
                    </TouchableOpacity> */}
                {/* <Image source={{ uri: image.uri }} style={{ width: 80, height: 80, marginRight: 5 }} /> */}
                {/* </View> */}
                <Button
                    title='Daftar'
                    loading={loading}
                    disable={loading || !data.name || !data.phone || !data.email || !data.password || !data.password_confirmation || !data.address || !data.province_id || !data.city_id || !data.district_id || !data.village_id}
                    onPress={toRegister}
                />
                <View style={{ alignItems: 'center', flexDirection: 'row', alignSelf: 'center' }}>
                    <Text style={{ fontSize: 12, color: color.g500, paddingRight: 4 }}>Sudah Memiliki Akun ? Login disini </Text>
                    <ButtonText title='Login' color={color.primary} onPress={() => props.navigation.navigate('Login')} />
                </View>
                <Loading isLoading={isLoading} />
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {

    }
})

export default Register;