import React, { useState } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import { StyleSheet, Text, TouchableHighlight, View, StatusBar, ScrollView } from 'react-native';
import { Button, ButtonText, Input, HeaderBanner } from '_atoms';
import { color, style } from '_styles';
import { connect } from 'react-redux';
import { setLoading } from '_states/actions/user';
import { Loading } from '_molecules';
import messaging from '@react-native-firebase/messaging';
import firebase from '@react-native-firebase/app';
import { getBrand, getSystemVersion } from 'react-native-device-info'

class Login extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            security: true,
            email: '',
            password: ''
        }
    }

    requestPermission = async (type) => {
        await firebase.messaging().requestPermission();
        const token = await firebase.messaging().getToken();
        this.tologin(token)
        console.log({ token })
    }

    toLogin = async () => {
        await firebase.messaging().requestPermission();
        const token = await firebase.messaging().getToken();
        const fcm_token = token
        console.log({ token })

        const { email, password, loading } = this.state;
        this.setState({ loading: true })
        axios.post(`/auth/login`, {
            email, password, fcm_token
        }).finally(() => this.setState({ loading: false }))
            .then(async res => {
                console.log({ res })
                const data = res.data.data
                const token = res.data.data.token
                console.log({ token })
                await AsyncStorage.setItem('token', token)
                this.props.dispatch(setLoading(true))
            })
    }

    handleChangeText = (index, val) => {
        const { data } = this.state
        data[index] = val
        this.setState({ data })
    }

    isCeretHidden = () => {
        const brand = getBrand()
        const version = getSystemVersion()
        if (brand.toLocaleUpperCase == 'XIAOMI' && parseFloat(version) >= 10) {
            return true
        }
        return false
    }

    render() {
        const { email, password, security, loading } = this.state;
        return (
            <ScrollView style={style.container} >
                <StatusBar translucent backgroundColor='transparent' />
                <HeaderBanner />
                <View style={[style.body, { padding: 16 }]}>

                    <Text style={{ color: color.g900, fontWeight: 'bold', fontSize: 20 }}>Selamat Datang !</Text>
                    <Text style={{ color: color.g600, paddingVertical: 8 }}>Silakan Login Untuk Melanjutkan</Text>
                    <Input
                        label='Email '
                        caretHidden={this.isCeretHidden()}
                        iconLeft='mail-outline'
                        keyboardType='email-address'
                        onChangeText={(v => this.setState({ email: v }))}
                        value={email}
                    />
                    <Input
                        label='Kata Sandi'
                        iconLeft='lock-closed-outline'
                        iconRight={security ? 'eye-off-outline' : 'eye-outline'}
                        iconPress={() => this.setState({ security: !security })}
                        password={security == true}
                        onChangeText={(v => this.setState({ password: v }))}
                        value={password}

                    />
                    <View style={{ alignItems: 'flex-end', paddingVertical: 4 }}>
                        <ButtonText title='Lupa Passsword ?' color={color.primary} onPress={() => this.props.navigation.navigate('ForgotPassword')} />
                    </View>
                    <Button
                        title='Login'
                        loading={loading}
                        disable={loading || email < 5 || password < 6}
                        onPress={() => this.toLogin()}
                    />
                    <View style={{ alignItems: 'center', flexDirection: 'row', alignSelf: 'center' }}>
                        <Text style={{ fontSize: 12, color: color.g500, paddingRight: 4 }}>Belum Memiliki Akun ? Daftar disini </Text>
                        <ButtonText title='Daftar' color={color.primary} onPress={() => this.props.navigation.navigate('PrivacyPollice', { initial: 0 })} />
                    </View>
                </View>
                <Loading isLoading={loading} />
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {

    }
})

const mapStateToProps = state => {
    return {
        user: state.user.data
    }
}

export default connect(mapStateToProps)(Login)
