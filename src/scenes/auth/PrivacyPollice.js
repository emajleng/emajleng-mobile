import React from 'react'
import { View, Text, Button, Dimensions } from 'react-native'
import { WebView } from 'react-native-webview';
import { color } from '_styles';

const PrivacyPollice = (props) => {
    const data = props.route.params.initial
    // console.log({ data })

    return (
        <View style={{ paddingVertical: 10, marginBottom: 30, backgroundColor: color.white }}>
            <View style={{ width: width, height: height - 100 }}>
                <WebView source={{ uri: 'http://edukasipranikah.emsacode.xyz/profil.html' }} style={{ height: 300, width: width, marginBottom: 10 }} />
            </View>
            {
                data == 0 &&
                <View style={{ paddingHorizontal: 40 }}>
                    <Button title={'Setuju'} onPress={() => props.navigation.navigate('Register')} />
                </View>
            }
        </View>
    )
}

const { height, width } = Dimensions.get('screen');
export default PrivacyPollice
