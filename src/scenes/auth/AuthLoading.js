import React, { useEffect } from 'react';
import { View, Dimensions, Image, StyleSheet, StatusBar, Text } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import { saveToken, setLoading, getUser, getMerchant } from '_states/actions/user';
import { connect, batch } from 'react-redux';
import Toast from 'react-native-simple-toast';
import { color } from '_styles';
import { URI } from '_utils/environment';

const Authloading = (props) => {

    useEffect(() => {
        console.disableYellowBox = true;
        setAxiosInterceptor()
    }, [])

    const setAxiosInterceptor = () => {
        AsyncStorage.getItem('token')
            .then(val => {
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + val;
                axios.defaults.baseURL = URI;
                axios.interceptors.response.use(
                    (response) => {
                        if (response.data.success || response.data.status == 'OK') {
                            return response;
                        }
                        if (response.data.message) {
                            Toast.show(response.data.message, Toast.LONG)
                        }
                        console.log({ errorres: response })
                        props.dispatch(reportError(response))
                        return Promise.reject(response);
                    },
                    (error) => {
                        console.log({ error })
                        // props.dispatch(reportError(error))
                        Toast.show(error.response.data.message)
                        if (error.response.status == 401) {
                            console.log({ error404: error })
                            AsyncStorage.clear()
                            props.dispatch(setShopInfo({}))
                            props.dispatch(setUser())
                            props.dispatch(setUnauthLoading(true))
                            props.dispatch(setLoading(true))
                            return;
                        }
                        if (error.response.status == 400) {
                            Toast.show(error.response.data.errors[0])
                        }
                        if (error.response.data.message) {
                            Toast.show(error.response.data.errors[0])
                        } else {
                            Toast.show('Sistem sedang tidak tersedia, silahkan coba lagi nanti.')
                        }
                        return Promise.reject(error);
                    }
                );
                getData(val)
            })
    }

    const getData = async (val) => {
        const start = new Date()
        if (props.isAuthLoading) {
            await props.dispatch(saveToken(null))
            await props.dispatch(setUnauthLoading(false))
            return;
        }
        if (val) {
            await props.dispatch(getUser())
        }
        const finish = new Date()
        const totalTime = 2000 - (finish - start)
        const delay = totalTime > 0 ? totalTime : 0
        console.log({ delay })
        setTimeout(() => {
            props.dispatch(saveToken(val))
        }, delay)
    }

    return (
        <View style={styles.wrap}>
            <StatusBar translucent={true} backgroundColor='transparent' barStyle='light-content' />
            <Image source={require('_assets/images/icon1.png')} style={styles.icon} resizeMode='contain' />
            <Text style={styles.text}>Padi Raharja</Text>
        </View>
    )
}

const { width } = Dimensions.get('screen')

const styles = StyleSheet.create({
    wrap: {
        backgroundColor: color.white,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        width: width / 2
    },
    text: {
        fontSize: 26,
        marginTop: -20,
        fontFamily: "Rubik"
    }
})

const mapStateToProps = state => {
    return {
        user: state.user.data,
        token: state.user.token,
        merchant: state.user.merchant
    }
}

export default connect(mapStateToProps)(Authloading);