import React, { useEffect, useState, useRef } from 'react';
import { View, Text, FlatList, TouchableOpacity, StyleSheet } from 'react-native';
import axios from 'axios';
import { style, color } from '_styles';
import { Input } from '_atoms';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import { Loading } from '_molecules';
import { connect } from 'react-redux';


const chatRoom = (props) => {

    const type = props.route.params.type
    const userId = props.route.params.userId
    // console.log({ receiver })
    const merchantId = props.route.params.merchantId
    const senders = ''
    const receiver = ''
    // console.log({ senders })
    const [items, setItems] = useState({})
    const flatListRef = useRef(null)
    const [loading, setLoading] = useState(false)
    const [data, setData] = useState([])
    const [chat, setChat] = useState('')
    const [pending, setPending] = useState('')
    const [refresh, setRefresh] = useState(false)

    useEffect(() => {
        getList()
    }, [])

    const getList = () => {
        setLoading(true)
        axios.get(`/chat/message?merchant_id=${merchantId}&user_id=${userId}`)
            .finally(() => { setLoading(false) })
            .then(res => {
                console.log({ res })
                setData(res.data.data)
                setPending('')
            })
    }

    const sendChat = () => {
        const text = chat
        setPending(text)
        setChat('')
        axios.post(`/chat`, {
            user_id: userId,
            merchant_id: merchantId,
            message: chat,
            to: type == 'user' ? 'MERCHANT' : 'USER'
        })
            .finally(() => { getList() })
            .then(res => {
                console.log({ res })
                getList()
            })
    }
    const renderData = ({ item }) => {
        const types = type == 'user' ? 'MERCHANT' : 'USER'
        console.log(item.recipient)
        return (
            <TouchableOpacity style={[styles.containerList, { alignSelf: types == item.recipient ? 'flex-end' : 'flex-start', backgroundColor: types == item.recipient ? color.p100 : '#fff' }]}>
                <Text style={[styles.time, { textAlign: types == item.recipient ? 'right' : 'left' }]}>{moment(item.time).format('HH:mm')}</Text>
                <Text>{item.message} {types == item.recipient && <Icon name='checkmark-done' size={12} color={item.read ? color.secondary : color.g700} />}</Text>
            </TouchableOpacity>
        )
    }

    return (
        <View style={{ flex: 1 }}>
            <FlatList
                ref={flatListRef}
                data={data}
                extraData={refresh}
                inverted
                keyExtractor={(item, index) => index.toString()}
                showsVerticalScrollIndicator={false}
                renderItem={renderData} />
            {pending.length > 0 &&
                <TouchableOpacity style={[styles.containerList, { alignSelf: 'flex-end', backgroundColor: color.p100 }]}>
                    <Text style={[styles.time, { textAlign: 'right' }]}>{moment().format('HH:mm')}</Text>
                    <Text>{pending} <Icon name='time-outline' size={12} color={color.g700} /></Text>
                </TouchableOpacity>
            }
            <View style={styles.footer}>
                <View style={{ flex: 1 }}>
                    <Input material onChangeText={(v) => setChat(v)} value={chat} placeholder='Ketik pesan...' />
                </View>
                <Icon name='send' size={25} style={{ marginLeft: 10 }} onPress={sendChat} />
            </View>
            <Loading isLoading={loading} />
        </View>
    )
}

const styles = StyleSheet.create({
    containerList: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        margin: 5,
        borderRadius: 15,
        borderWidth: 1,
        borderColor: color.g300,
    },
    ava: {
        width: 50,
        height: 50,
        borderRadius: 25,
        marginRight: 10
    },
    name: {
        fontWeight: 'bold',
        color: color.g800
    },
    footer: {
        padding: 10,
        paddingHorizontal: 10,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center'
    },
    time: {
        fontSize: 10,
        color: color.g500
    }
})

const mapStateToProps = state => {
    return {
        user: state.user.data
        // merchant: state.merchant.data,
    }
}
export default connect(mapStateToProps)(chatRoom);
