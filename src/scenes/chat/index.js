import React, { useEffect } from 'react';
import { Text, View, StyleSheet, FlatList, Image } from 'react-native';
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';
import { style, color } from '_styles';
import { ChatList } from '_organisms';

const chat = (props) => {
    return (
        <View style={style.container}>
            <ScrollableTabView tabBarActiveTextColor={color.primary} tabBarInactiveTextColor={color.g700} tabBarUnderlineStyle={{ backgroundColor: color.primary }} >
                <ChatList tabLabel='Pembeli' type={'user'} />
                <ChatList tabLabel='Penjual' type={'seller'} />
            </ScrollableTabView>
        </View>
    )
}


export default chat;