import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { WebView } from 'react-native-webview';
import { HeaderNews } from '_atoms';
import { NewsListVertical, NewsListHorizontal, NewsListNew } from '_molecules';
import axios from 'axios'
import { color } from '_styles';


const news = (props) => {
    const [data, setData] = useState({})
    console.log({ data })

    useEffect(() => {
        getNews()
    }, [])

    const getNews = () => {
        axios.get(`/public/news`)
            .then(res => {
                console.log({ res })
                setData(res.data.data.map((val) => val))
            })
    }
    return (
        <>
            <HeaderNews color={color.g200} />
            <View style={styles.container}>
                {/* < NewsListHorizontal data={newsDummy} /> */}
                < NewsListNew
                    data={data}
                    goto={'NewsDetail'}
                />
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        // backgroundColor: '#fff'
    }
})

const newsDummy = [
    {
        id: '1',
        judul: 'UMKM Majalengka menyelengarakan pelatihan untuk pemasaran produk',
        title: 'majalengka terkini sudah bisa kamu lihat dimana saja dan kamu bisa melihatnya semaunya berdasarkan berita yang kamu mau, beritanya lengkap selengkapnyaa adalah berita seputar majalengka',
        img: require('../../assets/images/slider4.jpg'),

    },
    {
        id: '2',
        judul: 'Majalengka Terdepan',
        title: 'majalengka terkini sudah bisa kamu lihat dimana saja dan kamu bisa melihatnya semaunya berdasarkan berita yang kamu mau, beritanya lengkap selengkapnyaa adalah berita seputar majalengka',
        img: require('../../assets/images/slider1.jpg'),

    },
    {
        id: '1',
        judul: 'Majalengka Terbaik',
        title: 'majalengka terkini sudah bisa kamu lihat dimana saja dan kamu bisa melihatnya semaunya berdasarkan berita yang kamu mau, beritanya lengkap selengkapnyaa adalah berita seputar majalengka',
        img: require('../../assets/images/slider2.jpg'),

    }
]

const style = StyleSheet.create({
    container: {
        // flex: 1
    }
})
export default news;