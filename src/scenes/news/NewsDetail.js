import React from 'react';
import { View, Text, Image, Dimensions, ScrollView } from 'react-native';
import { HeaderTransparent } from '_atoms';
import { WebView } from 'react-native-webview';
import { style, color } from '_styles';


const NewsDetail = (props) => {

    const data = props.route.params.data
    console.log({ data })

    return (
        <>
            <HeaderTransparent title='Detail Berita' />
            <Image source={{ uri: data.image }} style={{ width: width, height: height / 3 }} />
            <Text style={{ fontSize: 18, fontWeight: 'bold', padding: 5, marginLeft: 10 }}>{data.title}</Text>
            <ScrollView style={[style.container, { padding: 10 }]}>
                <View>
                    <WebView source={{ html: data.content }} scalesPageToFit={false} style={{ paddingTop: 4, width: width - 32, height: height, alignSelf: 'center', resizeMode: 'cover', flex: 1 }} />
                </View>
            </ScrollView>
        </>
    )
}

const { width, height } = Dimensions.get('screen')

export default NewsDetail