import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, TouchableHighlight, View, Dimensions, ScrollView, Alert } from 'react-native';
import { Button, Divider, LineVertical, HeaderTransparent } from '_atoms';
import { SelectSearch, Loading, SelectBank } from '_molecules';
import { color, style } from '_styles';
import axios from 'axios';
import { autoCurency } from '_utils';

const BuyMethod = (props) => {
    const data = props.route.params.paymentData
    console.log({ data })
    const [paymentMethod, setPaymentMethod] = useState({})
    console.log({ paymentMethod })
    const amounts = ((data.data.product?.price ? data.data.product.price : data.data.price) * (data.quantity)) + parseInt(data.logistic.etd.map((val) => val.value))
    const amountss = (data.data.product?.price ? data.data.product.price : data.data.price) * (data.quantity)
    const product_id = data.data.product?.id ? data.data.product.id : data.data.id
    console.log({ product_id })
    const [loading, setLoading] = useState(false)

    const Buy = () => {
        const etds = data.logistic.etd.map(val => val.etd)
        const et = Object.assign({}, etds)
        console.log({ et })
        Alert.alert('Lanjutkan Membayar ?', ' ', [
            {
                text: 'Batal', onPress: () => { }
            },
            {
                text: 'Lanjut', onPress: () => {
                    setLoading(true)
                    axios.post(`/transaction`, {
                        payment_method_id: paymentMethod.id,
                        is_balance: false,
                        amount: amounts,
                        detail: [
                            {
                                product_id: product_id,
                                quantity: data.quantity
                            }
                        ],
                        logistic: {
                            courier_id: parseInt(data.courier.map(val => val.id)),
                            courier_service: data.logistic.service,
                            etd: et[0],
                            postal_fee: parseInt(data.logistic.etd.map(val => val.value)),
                        },
                        origin_details: {
                            subdistrict_id: data.originAddress.subdistrict.subdistrict_id,
                            province_id: data.originAddress.province.province_id,
                            province: data.originAddress.province.province,
                            city_id: data.originAddress.city.city_id,
                            city: data.originAddress.city.city,
                            type: data.originAddress.city.type,
                            subdistrict_name: data.originAddress.subdistrict.subdistrict_name,
                            address: data.originAddress.address
                        },
                        destination_details: {
                            subdistrict_id: data.destination.subdistrict_id,
                            province_id: data.destination.province_id,
                            province: data.destination.province,
                            city_id: data.destination.city_id,
                            city: data.destination.city,
                            type: data.initialAddress.type,
                            subdistrict_name: data.destination.subdistrict_name,
                            address: data.destination.address
                        }
                    }).then(res => {
                        console.log({ res })
                        // alert('Pembelian sukses, Silakan cek email untuk melanjutkan pembayaran')
                        props.navigation.navigate('Invoices', { data: res.data.data, payment: paymentMethod })
                        deleteFormCart()
                        setLoading(false)

                    })
                }
            }
        ])
    }

    const deleteFormCart = () => {
        let value = data.cart
        if (value != null) {
            axios.delete(`/cart/${value}`)
                .then(res => {
                    this.getCart()
                    alert('lakukan pembayaran secepatnya')
                    console.log({ res })
                }).catch(err => {
                    console.log({ err })
                })
        } else {
            alert('lakukan pembayaran secepatnya')
        }
    }

    return (
        <>
            <HeaderTransparent title="Metode Pembayaran" />
            <View style={[style.body]}>
                <View style={{ padding: 20, marginTop: 10 }}>
                    <Text>Pilih Metode Pembayaran</Text>
                    <SelectBank
                        onSelect={(v) => setPaymentMethod(v)}
                        selected={paymentMethod}
                    />
                </View>
                <Divider />
                <View style={{ padding: 20 }}>
                    <Text style={{ fontSize: 16 }}>Informasi Pembayaran</Text>
                    <View style={{ paddingHorizontal: 20, paddingVertical: 5, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text>Total Harga Produk</Text>
                        <Text>{autoCurency(amountss)}</Text>
                    </View>
                    <View style={{ paddingHorizontal: 20, paddingVertical: 5, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text>Total Biaya Ongkos Kirim</Text>
                        <Text>{autoCurency(data.logistic.etd.map((val) => val.value))}</Text>
                    </View>
                    <LineVertical height={2} width={Width} color={color.g600} />
                    <View style={{ paddingHorizontal: 20, paddingVertical: 5, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text>Total Biaya </Text>
                        <Text>{autoCurency(amounts)}</Text>
                    </View>
                </View>
                <Divider />
                <View style={{ padding: 10 }}>
                    <Text style={{ fontSize: 16, marginBottom: 5 }}>Informasi Pengiriman</Text>
                    <View style={{ borderColor: color.primary, borderWidth: 1, borderRadius: 5, padding: 5 }}>
                        <Text style={{ marginBottom: 5 }}>Alamat Pengiriman </Text>
                        <LineVertical height={2} width={Width} color={color.g600} />
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                            <Text> Provinsi </Text>
                            <Text>{data.destination.province}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                            <Text> Kabupaten </Text>
                            <Text>{data.destination.city}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                            <Text> Kecamatan </Text>
                            <Text>{data.destination.subdistrict}</Text>
                        </View>
                    </View>

                    <Button
                        title='Bayar '
                        // loading={loading}
                        // disabled={loading || !email || password.length < 8}
                        onPress={() => Buy()}
                    />
                </View>
                <Loading isLoading={loading} />
            </View >
        </>
    )
}

const { Height, Width } = Dimensions.get('screen')

const styles = StyleSheet.create({
    container: {

    }
})

export default BuyMethod;