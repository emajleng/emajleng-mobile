import React, { useState, useEffect } from 'react';
import { View, Text, Image, StyleSheet, Dimensions, ScrollView, TouchableOpacity, Alert, Linking, } from 'react-native';
import { HeaderTransparent, Divider, Button } from '_atoms';
import { Loading } from '_molecules';
import { color } from '_styles';
import { autoCurency } from '_utils';
import InputSpinner from "react-native-input-spinner";
import axios from 'axios';
import { connect } from 'react-redux';

const DetaiProduct = (props) => {
    const data = props.route.params.item
    const [quantity, setQuantity] = useState('1')
    const [sender, setSender] = useState('')
    const [loading, setLoading] = useState(false)
    const token = props.token

    useEffect(() => {
        getMyId()
    }, [])

    const getMyId = () => {
        setLoading(true)
        axios.get(`/user`)
            .finally(() => { setLoading(false) })
            .then(res => {
                setSender(res.data.data.id)
            })
    }
    const addToCart = () => {
        let tokens = token
        if (tokens == null) {
            Alert.alert('Anda Belum membuat akun', "Silakan Buat akun untuk melanjutkan", [
                {
                    text: 'OK',
                    onPress: () => props.navigation.navigate('Register')
                }
            ])
        } else {
            axios.post(`/cart`, {
                quantity: quantity,
                product_id: data.id
            }).then(res => {
                alert('Barang Ditambahkan ke Keranjang')
            })
        }
    }

    const chatShop = () => {
        let tokens = token
        if (tokens == null) {
            Alert.alert('Anda Belum membuat akun', "Silakan Buat akun untuk melanjutkan", [
                {
                    text: 'OK',
                    onPress: () => props.navigation.navigate('Register')
                }
            ])
        } else {
            props.navigation.navigate('chatRoom', {
                name: data.merchant.name,
                type: 'user',
                sender: sender,
                receiver: data.merchant.id,
            })
        }
    }

    const handlePress = (item) => {
        let tokens = token
        if (tokens == null) {
            Alert.alert('Anda Belum membuat akun', "Silakan Buat akun untuk melanjutkan", [
                {
                    text: 'OK',
                    onPress: () => props.navigation.navigate('Register')
                }
            ])
        } else {
            props.navigation.navigate('BuyProduct', { data, quantity })
        }
    }
    const shopProduct = () => {
        props.navigation.navigate('shopProduct', {
            data: data
        })
    }
    return (
        <>
            <HeaderTransparent title="Detail Produk" />
            <ScrollView>
                <Image source={{ uri: data.photo }} style={styles.image} />
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View style={{ padding: 10 }}>
                        <Text style={styles.price}>{autoCurency(data.price)}</Text>
                        <Text style={styles.title}>{data.name}</Text>
                    </View>
                    <View style={{ padding: 10, right: 0, alignSelf: 'flex-end', height: 50 }}>
                        <InputSpinner
                            max={data.stock}
                            step={1}
                            colorMax={"#f04048"}
                            rounded={false}
                            showBorder
                            value={quantity}
                            height={35}
                            width={130}
                            color={color.g500}
                            onChange={(num) => {
                                setQuantity(num)
                                // if (num == data.min_qty_diskon) {
                                //     Toast.show('Selamat Anda Mendapatkan Diskon 10%', Toast.LONG)
                                // }
                            }}
                        />
                    </View>
                </View>
                <Divider />
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ width: 210 }}>
                        <Text style={styles.subTitle}>Informasi Produk</Text>
                        <Text style={styles.informationText}>Berat : {data.weight} gr</Text>
                        <Text style={styles.informationText}>Stock: {data.stock}</Text>
                        <Text style={styles.informationText}>deskripsi: {data.description}</Text>
                        <Text style={[styles.informationText, { marginBottom: 20 }]}>Kategori: {data.productCategory.name}</Text>
                    </View>
                </View>
                <Divider />
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TouchableOpacity onPress={() => shopProduct()}>
                        <Text style={styles.subTitle}>Informasi Penjual</Text>
                        <Text style={styles.informationText}>{data.merchant.name}</Text>
                        <Image source={{ uri: data.merchant.logo }} style={{ height: 50, width: 70, padding: 5, marginHorizontal: 15 }} />
                        <Text style={{ fontSize: 10, paddingHorizontal: 10 }}>{data.merchant.city.name},</Text>
                        <Text style={{ fontSize: 10, paddingHorizontal: 10 }}>{data.merchant.province.name}</Text>
                    </TouchableOpacity>
                    <View style={{ padding: 10, right: 0, alignSelf: 'flex-end' }}>
                        <Button
                            title='Hubungi Penjual'
                            style={{ backgroundColor: color.primary }}
                            onPress={() => chatShop()}
                        />
                    </View>
                </View>
                <Divider />
                <Loading isLoading={loading} />
            </ScrollView>
            <View style={styles.footer}>
                <View style={{ flex: 1, paddingHorizontal: 10, justifyContent: 'center' }}>
                    <Button title='+ Keranjang' style={{ backgroundColor: color.secondary }} onPress={() => addToCart()} />
                </View>
                <View style={{ flex: 1, paddingHorizontal: 10, justifyContent: 'center' }}>
                    {/* <Button title='+ Keranjang' loading={loading} disabled={loading} style={{ backgroundColor: color.secondary }} onPress={addToCart} /> */}
                    <Button
                        title='Beli'
                        style={{ backgroundColor: color.primary }}
                        onPress={() => handlePress()}
                    />
                </View>
            </View>
        </>
    )
}

const { width, height } = Dimensions.get('screen')

const styles = StyleSheet.create({
    image: {
        resizeMode: 'cover',
        height: height / 2.5,
        width: width
    },
    title: {
        color: color.g800,
        paddingTop: 10
    },
    price: {
        color: color.g800,
        fontWeight: 'bold',
        fontSize: 22,
    },
    subTitle: {
        fontSize: 16,
        fontWeight: 'bold',
        color: color.g800,
        marginBottom: 0,
        padding: 10
    },
    informationText: {
        paddingHorizontal: 10,
        fontSize: 16,
        marginBottom: 5,
        fontWeight: 'bold'
    },
    footer: {
        height: 58,
        borderTopWidth: 1,
        borderColor: color.g300,
        flexDirection: 'row'
    },
})

const mapStateToProps = state => {
    return {
        user: state.user.data,
        token: state.user.token
    }
}

export default connect(mapStateToProps)(DetaiProduct);