import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { useBackHandler } from '@react-native-community/hooks';
import { color } from '_styles';
import { Button, HeaderTransparent } from '_atoms';
import { autoCurency } from '_utils';
import moment from 'moment';
import 'moment/min/locales';
import axios from 'axios';

const Invoices = (props) => {

    const { data, payment } = props.route.params
    console.log({ data })
    console.log({ payment })
    const [norek, setNorek] = useState({})
    console.log({ norek })

    useBackHandler(() => {
        props.navigation.popToTop()
        return true
    })

    return (
        <>
            <View style={styles.container}>
                <Icon name='checkmark-circle-outline' color={color.success} size={100} />
                <Text style={styles.title}>Transaksi Berhasil.</Text>
                <Text style={styles.desc}>Lanjutkan pembayaran dan kirim foto bukti pembayaran. "Untuk Keamanan, jangan menunjukan bukti pembayaran kepihak manapun selain pihak PadiRaharja melalui aplikasi"</Text>

                <Text>Status: {data.status}</Text>
                <Text>Total Pembayaran : {autoCurency(data.amount + data.unique_code)}</Text>
                <View style={{ padding: 5, borderColor: color.secondary, borderWidth: 2, borderRadius: 5, marginVertical: 10, width: width - 10 }}>
                    <Text>Segera Lakukan Pembayaran sebelum :  </Text>
                    <Text>       {moment().locale('id').add(3, 'hours').format('dddd, DD MMMM YYYY, HH:mm')}</Text>
                    <Text style={{ marginTop: 10 }}>Lakukan Pembayaran ke : </Text>
                    <View style={{ padding: 10, borderRadius: 5, borderColor: color.primary, borderWidth: 1, margin: 5 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text>Bank</Text>
                            <Text>{payment.bank_name}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text>Nomer Rekenig</Text>
                            <Text>{payment.account_number}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text>Atas Nama </Text>
                            <Text>{payment.account_name}</Text>
                        </View>
                    </View>
                </View>
                {/* {
                    item.type == 'EMONEY' ?
                        <Text style={styles.desc}>{`Pembelian ${item.name} ${product.amount} seharga ${autoCurency(product.price + product.admin_fee)} berhasil. Terimakasih telah menggunakan layanan kami :)`}</Text> :
                        <Text style={styles.desc}>{`Pembelian ${item.type} ${provider.name} ${product.amount} seharga ${autoCurency(product.price + product.admin_fee)} berhasil. Terimakasih telah menggunakan layanan kami :)`}</Text>
                } */}
            </View>
            <View style={styles.footer}>
                <Button outline color={color.secondary} title='Kembali' onPress={() => props.navigation.popToTop()} />
            </View>
        </>
    )
}

const { width, height } = Dimensions.get('screen')
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        color: color.success
    },
    desc: {
        color: color.g700,
        textAlign: 'center',
        marginBottom: 20
    },
    footer: {
        padding: 10,
        paddingBottom: 20
    }
})

export default Invoices;