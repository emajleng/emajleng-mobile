import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, Dimensions, View, StatusBar, ScrollView, Alert, FlatList, TouchableOpacity, Image } from 'react-native';
import { Button, HeaderTransparent, Input, ButtonText } from '_atoms'
import { color, style } from '_styles';
import { SelectSearch, Loading } from '_molecules'
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/Ionicons';

const AddProduct = (props) => {

    const [product, setProduct] = useState({})
    const [prov, setProv] = useState([])
    const [isLoading, setisLoading] = useState(false)
    const [image, setImage] = useState({})
    const [category, setCategory] = useState([])

    console.log({ product })
    useEffect(() => {
        getProv()
        getCategory()
    }, [])

    const getCategory = () => {
        axios.get(`/public/product_category`)
            .then(res => {
                console.log({ oki: res })
                let categories = res.data.data.map(({ id: value, name: label }) => ({ value, label }))
                setCategory(categories)
            })
    }

    const getProv = () => {
        setisLoading(true)
        axios.get(`/public/province`)
            .finally(() => setisLoading(false))
            .then(res => {
                console.log({ res })
                setProv(res.data.data.map(({ id: value, name: label }) => ({ value, label })))
            })
    }

    const selectImage = (type) => {
        const options = {
            quality: 0.8,
            maxWidth: 1280,
            maxHeight: 1280,
            storageOptions: {
                skipBackup: true
            }
        }
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
                console.log('caceled')
            } else if (response.error) {
                Toast.show(response.error, Toast.LONG)
                console.log('ImagePicker Error: ', response.error);
            } else {
                console.log({ response })
                let filename = response.uri.split('/').pop();
                // const img = [...image, { uri: response.uri, name: response.fileName ?? filename, type: response.type }]
                setImage({ uri: response.uri, name: response.fileName ?? filename, type: response.type })
                // console.log({ img })
            }
        })
    }

    const deleteImage = item => {
        const img = image.filter(x => x.uri != item.uri)
        setImage(img)
    }

    const renderImage = ({ item }) => {
        return (
            <View>
                <TouchableOpacity style={styles.delete} onPress={() => deleteImage(item)}>
                    <Icon name='close' size={20} />
                </TouchableOpacity>
                <Image source={{ uri: item.uri }} style={{ width: 80, height: 80, marginRight: 5 }} />
            </View>
        )
    }


    const AddProduct = () => {
        setisLoading(true)
        const formData = new FormData()
        // image.map(img => {
        // })

        formData.append('name', product.name)
        formData.append('product_category_id', product.product_category_id)
        formData.append('price', product.price)
        formData.append('stock', product.stock)
        formData.append('weight', product.weight)
        formData.append('description', product.description)
        // formData.append('province_id', product.province_id)
        formData.append('photo', image)

        axios.post(`/product`, formData)
            .finally(() => { setisLoading(false) })
            .then(res => {
                console.log({ res })
                Alert.alert('Suksess', 'Tambah Produk Berhasil', [
                    {
                        text: 'OK',
                        onPress: () => props.navigation.goBack()
                    }
                ])
            }).catch(err => {
                console.log({ err })
                alert('Tambah Produk Gagal')
            })
    }

    return (
        <>
            <HeaderTransparent title='Tambah Poduk' route={'AddProduct'} />
            <ScrollView style={style.container}>
                <View style={[style.body, { padding: 16 }]}>
                    <Input label="Nama Produk" onChangeText={(name => setProduct(v => ({ ...v, name })))} />
                    {/* <Input label="Kategori Produk" onChangeText={(product_category_id => setProduct(v => ({ ...v, product_category_id })))} /> */}
                    <SelectSearch
                        data={category}
                        value={product.product_category_id}
                        label='Kategori'
                        title='Pilih Kategori'
                        onSelect={(val => { setProduct(v => ({ ...v, product_category_id: val.value })) })}
                    />
                    <Input label="Harga" onChangeText={(price => setProduct(v => ({ ...v, price })))} keyboardType='phone-pad'
                    />
                    <Input label="Stok" onChangeText={(stock => setProduct(v => ({ ...v, stock })))} keyboardType='phone-pad'
                    />
                    <Input label="Berat" onChangeText={(weight => setProduct(v => ({ ...v, weight })))} keyboardType='phone-pad' placeholder="gram" />
                    <Input label="Deskripsi Produk" multiline={true} height={80} onChangeText={(description => setProduct(v => ({ ...v, description })))} multiline={true} />
                    <View style={{ height: 200, paddingTop: 20 }}>
                        <ButtonText title="Pilih Foto Product" color={color.primary} onPress={() => selectImage()} />
                        {/* <TouchableOpacity style={styles.delete} onPress={() => deleteImage(item)}>
                            <Icon name='close' size={20} />
                        </TouchableOpacity> */}
                        <Image source={{ uri: image.uri }} style={{ width: 80, height: 80, marginRight: 5 }} />
                    </View>
                    <Button title='Tambah' onPress={() => AddProduct()} />
                </View>
                <Loading isLoading={isLoading} />
            </ScrollView>
        </>
    )
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: color.primary,
        height: Dimensions.get('screen').height / 8,
        justifyContent: 'center',
        paddingTop: Platform.OS == 'ios' && isIphoneX() ? 20 : 0
    },
    body: {
        paddingTop: 10
    }
})

export default AddProduct;