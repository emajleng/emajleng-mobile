import React, { useEffect, useState } from 'react';
import { View, Text, Dimensions, StyleSheet, ScrollView } from 'react-native';
import { HeaderTransparent, Button, Input, LineVertical, Divider } from '_atoms';
import { SelectSearch, Loading } from '_molecules';
import { color, style } from '_styles';
import { connect } from 'react-redux';
import axios from 'axios';

const BuyProduct = (props) => {
    const data = props.route.params.data
    console.log({ data })
    const cart = props.route.params.cart
    console.log({ cart })
    const merch = data.product?.merchant ? data.product.merchant.id : data.merchant?.id
    // console.log({ merch })
    const quantity = props.route.params.quantity
    const [isLoading, setisLoading] = useState(false)
    const [couriers, setCourier] = useState([])
    // console.log({ couriers })
    const [destination, setDestination] = useState({})
    console.log({ destination })
    const [prov, setProv] = useState([])
    const [city, setCity] = useState([])
    const [district, setDistrict] = useState([])
    const [originAddress, setOriginAddress] = useState({})
    console.log({ originAddress })
    const [valLog, setValLog] = useState([])
    // console.log({ valLog })
    const [logistic, setLogistic] = useState([])
    // console.log({ logistic })
    const [resresh, setRefresh] = useState(false)
    const [initialAddress, setInnitialAddress] = useState({})

    useEffect(() => {
        courier()
        getProv()
    }, [])

    const getProv = () => {
        setisLoading(true)
        axios.get(`/raja-ongkir/province`)
            .finally(() => setisLoading(false))
            .then(res => {
                // console.log({ res })
                setProv(res.data.data.map(({ province_id: value, province: label }) => ({ value, label })))
            })

    }
    const getCity = (id) => {
        console.log({ id })
        setisLoading(true)
        axios.get(`/raja-ongkir/city/${id}`)
            .finally(() => setisLoading(false))
            .then(res => {
                // console.log({ res })
                setCity(res.data.data.map(({ city_id: value, city_name: label }) => ({ value, label })))
            })
    }
    const getDistrict = (id) => {
        console.log({ id })
        setisLoading(true)
        axios.get(`/raja-ongkir/subdistrict/${id}`)
            .finally(() => setisLoading(false))
            .then(res => {
                setDistrict(res.data.data.map(({ subdistrict_id: value, subdistrict_name: label }) => ({ value, label })))
            })
    }

    const courier = () => {
        setisLoading(true)
        axios.get(`/public/courier`)
            .finally(() => setisLoading(false))
            .then(res => {
                console.log({ res })
                setCourier(res.data.data.map(({ code: value, name: label, icon: icon, id: id }) => ({ value, label, icon, id })))
                getOriginAddress()
                getCost()
            }).catch(err => {
                alert('Isi terlebih dahulu alamat pengiriman')
            })
    }

    const getOriginAddress = () => {
        axios.get(`/raja-ongkir/origin-address/${merch}`)
            .then(res => {
                setOriginAddress(res.data.data)
                const logistics = (res.data.data)
                // alert('ll')
            }).catch(err => {
                // alert('ss')
                console.log({ err })
            })
    }

    const getCost = (value) => {
        console.log({ value })
        axios.post(`/raja-ongkir/cost`, {
            origin: originAddress.subdistrict.subdistrict_id,
            originType: 'subdistrict',
            destination: destination.subdistrict_id,
            destinationType: 'subdistrict',
            weight: data.weight ? data.weight : '2000',
            courier: 'jne'
        }).then(res => {
            console.log({ res })
            const y = res.data.data
            setLogistic(y.cost[0].costs.map(({ service: label, description: value, cost: etd }) => ({ value, label, etd })))
            setInnitialAddress(y.origin_details)
        })
    }

    const carts = () => {
        let value = cart

        if (value == 'undifined') {
            return null
        } else {
            return value
        }
    }

    return (
        <>
            <HeaderTransparent title="Alamat Pengiriman" />
            <ScrollView >

                <View style={[style.body, { paddingTop: 10 }]}>
                    <View style={styles.alamatBox}>
                        <Text style={{ color: color.g900, fontWeight: 'bold', fontSize: 20 }}>Masukan Alamat Pengiriman</Text>
                        <SelectSearch
                            data={prov}
                            value={destination.province_id}
                            label='Provinsi'
                            title='Pilih Provinsi'
                            onSelect={(val => { setDestination(v => ({ ...v, province_id: val.value, province: val.label })); getCity(val.value) })}
                        />
                        <SelectSearch
                            data={city}
                            value={destination.city_id}
                            label='Kota'
                            title='Pilih Kota'
                            onSelect={(val => { setDestination(v => ({ ...v, city_id: val.value, city: val.label })); getDistrict(val.value) })}
                        />
                        <SelectSearch
                            data={district}
                            value={destination.subdistrict_id}
                            label='Kecamatan'
                            title='Pilih Kecamatan'
                            onSelect={(val => { setDestination(v => ({ ...v, subdistrict_id: val.value, subdistrict: val.label })) })}
                        />
                        <Input
                            label='Alamat'
                            placeholder='Nama jalan, Nomor Rumah, Rt.Rw, Kelurahan'
                            iconLeft='person-outline'
                            onChangeText={(address => setDestination(v => ({ ...v, address })))}
                        />
                    </View>
                    <Divider />
                    {
                        destination.address ?
                        <View style={{ padding: 20 }}>
                            {
                                data.merchant &&
                                <Text style={[styles.address, { fontWeight: 'bold' }]}>Toko : {data.merchant.name}</Text>
                            }
                            <SelectSearch
                                data={couriers}
                                value={valLog.name}
                                label='Layanan Pengiriman'
                                title='Pilih Layanan'
                                onSelect={(val => { setValLog(v => ({ ...v, name: val.value, id: val.id })); getCost(val.value) })}
                            />
                            <SelectSearch
                                data={logistic}
                                value={valLog.layanan}
                                label='Jenis Layanan'
                                title='Pilih Jenis Layanan'
                                onSelect={val => { setValLog(v => ({ ...v, layanan: val.value, service: val.label, etd: val.etd })) }}
                            />
                            <Button title="Selanjutnya" onPress={() => props.navigation.navigate('BuyMethod', { paymentData: { destination: destination, data: data, quantity: quantity, courier: couriers, logistic: valLog, originAddress: originAddress, initialAddress: initialAddress, cart: carts() } })} 
                            disable={ !destination || !couriers }
                            />
                        </View>
                        :
                        <View>

                        </View>
                    }
                    <Loading isLoading={isLoading} />
                </View>
            </ScrollView>
        </>
    )
}

const { width, height } = Dimensions.get('screen')
const styles = StyleSheet.create({
    alamatBox: {
        padding: 10,
        width: width - 10,
        marginBottom: 20,
        alignSelf: 'center'
    },
    title: {
        fontSize: 18,
        alignSelf: 'center',
        marginVertical: 10
    },
    address: {
        padding: 5
    }
})
export default BuyProduct;