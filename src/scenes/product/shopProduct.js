import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, ScrollView, FlatList, TouchableHighlight, Dimensions } from 'react-native';
import { Divider, Button } from '_atoms';
import axios from 'axios';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import { color, style } from '_styles';
import { autoCurency } from '_utils';
import { Loading } from '_molecules';

const shopProduct = (props) => {
    const data = props.route.params.data
    const [product, setProduct] = useState([]);
    const [sender, setSender] = useState('');
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        getProduct();
        getMyId();
    }, [])

    const getMyId = () => {
        axios.get(`/user`)
            .then(res => {
                console.log({ res })
                setSender(res.data.data.id)
            })
    }
    const getProduct = () => {
        setLoading(true)
        axios.get(`/product/merchant/${data.merchant.id}`)
            .finally(() => { setLoading(false) })
            .then(res => {
                console.log({ res })
                setProduct(res.data.data)
            })
    }
    const renderProduct = ({ item }) => {
        return (
            <SkeletonContent
                containerStyle={styles.skeletonContainer}
                isLoading={false}
                layout={[styles.skeleton]}
            >
                <TouchableHighlight underlayColor='transparent' onPress={() => props.navigation.push('DetailProduct', { item })} >
                    <View style={[styles.itemContainer, style.shadow]}>
                        <Image source={{ uri: item.photo }} style={styles.image} />
                        <View style={styles.infoContainer}>
                            <Text style={{ color: color.g800, fontWeight: 'bold' }} numberOfLines={2}>{item.name}</Text>
                            <Text style={{ color: color.secondary }} numberOfLines={2}>{autoCurency(item.price)}</Text>
                        </View>
                        <Text style={{ color: color.failed, fontSize: 10, right: 0, position: 'absolute', bottom: 7, padding: 5 }} numberOfLines={2}>Stok: {item.stock}</Text>
                    </View>
                </TouchableHighlight>
            </SkeletonContent>
        )
    }

    return (
        <View>
            <View style={styles.shopInfo}>
                <View style={{ flexDirection: 'row' }}>
                    <Image source={{ uri: data.merchant.logo }} style={styles.logo} />
                    <Text style={styles.name}>{data.merchant.name}</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View >
                        <Text>Alamat Toko : </Text>
                        <Text style={styles.alamat}>{data.merchant.province.name}</Text>
                        <Text style={styles.alamat}>{data.merchant.city.name}</Text>
                        <Text style={styles.alamat}>{data.merchant.district.name}</Text>
                        <Text style={styles.alamat}>{data.merchant.village.name}</Text>
                    </View>
                    <Button title={'Hubungi Penjual'} onPress={() => props.navigation.navigate('chatRoom', {
                        name: data.merchant.name,
                        type: 'user',
                        sender: sender,
                        receiver: data.merchant.id,
                    })} />
                </View>
            </View>
            <Divider />
            <ScrollView style={{ marginBottom: 160 }}>
                <FlatList
                    data={product}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={renderProduct}
                    numColumns={2}
                    ListHeaderComponent={props.headerComponent}
                    refreshing={props.refreshing}
                    onRefresh={props.onRefresh}
                    onEndReached={props.loadMore}
                    onEndReachedThreshold={0.1}
                    ListEmptyComponent={props.empty}
                    showsVerticalScrollIndicator={false}
                    onScroll={props.onScroll} />
            </ScrollView>
            <Loading isLoading={loading} />
        </View>
    )
}

const { width, height } = Dimensions.get('screen')
const styles = StyleSheet.create({
    shopInfo: {
        padding: 10,
        backgroundColor: '#fff'
    },
    logo: {
        height: 60,
        width: 55,
        marginRight: 10
    },
    name: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    alamat: {
        fontSize: 10
    },
    image: {
        width: '100%',
        height: 150,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    },
    skeleton: {
        width: (width / 2) - 14,
        height: 220,
        margin: 7,
        marginVertical: 10
    },
    skeletonContainer: {
        flex: 1,
    },
    itemContainer: {
        width: (width / 2) - 15,
        margin: 10,
        marginRight: 0,
        borderRadius: 5
    },
    infoContainer: {
        padding: 10
    },
})

export default shopProduct