import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, Dimensions, View, StatusBar, ScrollView, Alert, FlatList, TouchableOpacity, Image } from 'react-native';
import { Button, HeaderTransparent, Input, ButtonText } from '_atoms'
import { color, style } from '_styles';
import { SelectSearch, Loading } from '_molecules'
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/Ionicons';

const EditProduct = (props) => {

    const data = props.route.params.data
    console.log({ data })
    const [product, setProduct] = useState({})
    const [prov, setProv] = useState([])
    const [isLoading, setisLoading] = useState(false)
    const [image, setImage] = useState({})
    const [category, setCategory] = useState([])

    console.log({ product })
    useEffect(() => {
        getProv()
        getCategory()
    }, [])

    const getCategory = () => {
        axios.get(`/public/product_category`)
            .then(res => {
                console.log({ oki: res })
                let categories = res.data.data.map(({ id: value, name: label }) => ({ value, label }))
                setCategory(categories)
            })
    }

    const getProv = () => {
        setisLoading(true)
        axios.get(`/public/province`)
            .finally(() => setisLoading(false))
            .then(res => {
                console.log({ res })
                setProv(res.data.data.map(({ id: value, name: label }) => ({ value, label })))
            })
    }

    const selectImage = (type) => {
        const options = {
            quality: 0.8,
            maxWidth: 1280,
            maxHeight: 1280,
            storageOptions: {
                skipBackup: true
            }
        }
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
                console.log('caceled')
            } else if (response.error) {
                Toast.show(response.error, Toast.LONG)
                console.log('ImagePicker Error: ', response.error);
            } else {
                console.log({ response })
                let filename = response.uri.split('/').pop();
                // const img = [...image, { uri: response.uri, name: response.fileName ?? filename, type: response.type }]
                setImage({ uri: response.uri, name: response.fileName ?? filename, type: response.type })
                // console.log({ img })
            }
        })
    }

    const deleteImage = item => {
        const img = image.filter(x => x.uri != item.uri)
        setImage(img)
    }

    const EditProduct = () => {
        const formData = new FormData()
        // image.map(img => {
        // })
        formData.append('name', product.name ? product.name : data.name)
        formData.append('product_category_id', product.product_category_id)
        formData.append('price', product.price ? product.price : data.price)
        formData.append('stock', product.stock ? product.stock : data.stock)
        formData.append('weight', product.weight ? product.weight : data.weight)
        formData.append('description', product.description ? product.description : data.description)
        // formData.append('province_id', product.province_id)
        if (image.uri) {
            formData.append('photo', image)
        }

        axios.post(`/product/${data.id}`, formData)
            .then(res => {
                console.log({ res })
                Alert.alert('Suksess', 'Mengubah Produk Berhasil', [
                    {
                        text: 'OK',
                        onPress: () => props.navigation.goBack()
                    }
                ])
                // alert('Tambah Produk Berhasil')

            }).catch(err => {
                console.log({ err })
                alert('Tambah Produk Gagal')
            })
    }

    return (
        <>
            <HeaderTransparent title='Ubah Produk' route={'EditProduct'} />
            <ScrollView style={style.container}>
                <View style={[style.body, { padding: 20 }]}>
                    <Input label="Nama Produk" onChangeText={(name => setProduct(v => ({ ...v, name })))} placeholder={data.name} />
                    <SelectSearch
                        data={category}
                        value={product.product_category_id}
                        label='Kategori'
                        title='Pilih Kategori'
                        onSelect={(val => { setProduct(v => ({ ...v, product_category_id: val.value })) })}
                    />
                    <Input label="Harga" onChangeText={(price => setProduct(v => ({ ...v, price })))} placeholder={data.price} />
                    <Input label="Stok" onChangeText={(stock => setProduct(v => ({ ...v, stock })))} placeholder={data.stock} />
                    <Input label="Berat" onChangeText={(weight => setProduct(v => ({ ...v, weight })))} placeholder={data.weight} />
                    <Input label="Deskripsi Produk" onChangeText={(description => setProduct(v => ({ ...v, description })))} placeholder={data.description} />
                    <View style={{ height: 200, paddingTop: 20 }}>
                        {/* <TouchableOpacity style={styles.delete} onPress={() => deleteImage(item)}>
                            <Icon name='close' size={20} />
                        </TouchableOpacity> */}
                        <ButtonText title="Ganti Foto Product" color={color.primary} onPress={() => selectImage()} />
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: 250, alignSelf: 'center', paddingTop: 20 }}>
                            <View style={{ alignItems: 'center' }}>
                                <Text>Foto Produk</Text>
                                <Image source={{ uri: data.photo }} style={{ width: 100, height: 100, marginRight: 5, marginTop: 5 }} />
                            </View>
                            {
                                image.uri &&
                                <View style={{ alignItems: 'center' }} >
                                    <Text style={{ color: color.secondary }}>Foto Baru</Text>
                                    <Image source={{ uri: image.uri }} style={{ width: 100, height: 100, marginRight: 5, marginTop: 5 }} />
                                </View>
                            }
                        </View>
                    </View>
                    <Button title='Ubah Produk' onPress={() => EditProduct()} />
                </View>
                <Loading isLoading={isLoading} />
            </ScrollView>
        </>
    )
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: color.primary,
        height: Dimensions.get('screen').height / 8,
        justifyContent: 'center',
        paddingTop: Platform.OS == 'ios' && isIphoneX() ? 20 : 0
    },
    body: {
        paddingTop: 10
    }
})

export default EditProduct;