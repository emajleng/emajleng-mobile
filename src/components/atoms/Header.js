import React from 'react'
import { View, Image, StyleSheet, Platform, StatusBar, Dimensions, Text } from 'react-native'
import { color, style } from '_styles'

const HeaderBanner = (props) => {
    return (
        <View style={styles.header}>
            <Image source={require('_assets/images/icon1.png')} style={{ alignSelf: 'center', marginTop: 30, height: 150, width: 220 }} resizeMode='contain' />
        </View>
    )
}

const HeaderNews = (props) => {
    return (
        <View style={[styles.headerNews, style.shadow, { backgroundColor: props.color }]}>
            <Image source={require('_assets/images/icon.png')} style={styles.logoNews} resizeMode='contain' />
            <Text style={styles.textNews}>Berita Harian Majalengka</Text>
        </View>
    )
}

const HeaderProfile = (props) => {
    return (
        <View style={[styles.headerNews, style.shadow, { backgroundColor: props.color, justifyContent: 'center' }]}>
            <Image source={require('_assets/images/icon.png')} style={styles.logoNews} resizeMode='contain' />
            <Text style={{ color: props.colorText, marginTop: 35, fontSize: 20, marginLeft: 30 }}>{props.title}</Text>
        </View>
    )
}

const { width, height } = Dimensions.get('screen')
const styles = StyleSheet.create({
    header: {
        // backgroundColor: color.p200,
        height: height / 2 - 100,
        justifyContent: 'center',
    },
    headerNews: {
        // backgroundColor: color.primary,
        height: height / 9,
        width: width + 10,
        justifyContent: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 40,
        shadowOpacity: 3,
        elevation: 3,
        alignSelf: 'center'
    },
    logoNews: {
        height: height / 10,
        width: width / 9,
        marginTop: 17
    },
    textNews: {
        marginTop: 35,
        fontSize: 20,
        color: color.g800
    }
})

export {
    HeaderBanner,
    HeaderNews,
    HeaderProfile
} 