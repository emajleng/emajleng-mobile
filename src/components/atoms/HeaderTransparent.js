import React from 'react'
import { Animated, Platform, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import { color } from '_styles'
import { withNavigation } from '@react-navigation/compat'

const HeaderTransparent = (props) => {
    return (
        <Animated.View style={[styles.wrapper, { backgroundColor: color.primary }]}>
            <Icon name='arrow-back-outline' size={24} color='#fff' onPress={props.backPress ?? (() => props.navigation.goBack())} />
            <Text style={styles.title}>{props.title}</Text>
            <Icon name={props.iconName} size={40} color='#fff' onPress={props.onPress} />
        </Animated.View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        height: 68,
        paddingTop: 20,
        flexDirection: 'row',
        padding: 10,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.3,
        shadowRadius: 3,
        elevation: 3,
    },
    title: {
        flex: 1,
        textAlign: 'center',
        color: '#fff',
        fontSize: 18,
    }
})

export default withNavigation(HeaderTransparent)