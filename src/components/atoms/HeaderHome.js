import React, { useEffect } from 'react';
import { Text, StyleSheet, View, Platform, Animated, StatusBar } from 'react-native';
import { color } from '_styles';
import { withNavigation } from '@react-navigation/compat';

const HeaderHome = (props) => {

    const animated = props.animated.interpolate({
        inputRange: [0, 30, 100],
        outputRange: ['rgba(255,255,255,0)', 'rgba(255,255,255,0)', 'rgba(255,255,255,1)'],
        extrapolateRight: 'clamp',
    })
    const elevation = props.animated.interpolate({
        inputRange: [0, 30, 100],
        outputRange: [0.01, 0.01, 3],
        extrapolateRight: 'clamp',
    })

    return (
        <Animated.View style={[styles.wraperTransparent, { backgroundColor: animated, elevation: elevation }]}>
            {props.children}
        </Animated.View>
    )
}

const styles = StyleSheet.create({
    wraperTransparent: {
        zIndex: 2,
        height: 58 + StatusBar.currentHeight,
        flexDirection: 'row',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 3,
        paddingTop: StatusBar.currentHeight,
        paddingHorizontal: 10,
        paddingBottom: 5,
        alignItems: 'center',
    }
})

export default withNavigation(HeaderHome)