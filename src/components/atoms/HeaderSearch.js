import React, { useEffect, useState } from 'react';
import { Text, StyleSheet, View, Platform, Animated, Image, StatusBar, Dimensions } from 'react-native';
import { color } from '../../styles';
import { withNavigation } from '@react-navigation/compat';
import Icon from 'react-native-vector-icons/Ionicons';
import { Icons, InputSearch } from '../atoms';


const HeaderSearch = (props) => {

    const [key, setKey] = useState('')

    return (
        <Animated.View style={[styles.wraper]}>
            <Icon name='chevron-back-outline' size={34} color={color.g800} onPress={() => props.navigation.goBack()} />
            <InputSearch placeholder='Cari...' onChangeText={(v) => setKey(v)} onSubmitEditing={() => props.onSubmitEditing(key)} style={{ flex: 1 }} />
        </Animated.View>
    )
}

const { width, height } = Dimensions.get('screen')

const styles = StyleSheet.create({
    wraper: {
        height: 58 + StatusBar.currentHeight,
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: StatusBar.currentHeight,
        paddingHorizontal: 10,
        paddingBottom: 5,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.3,
        shadowRadius: 3,
        elevation: 3,
        backgroundColor: '#fff',
        width: width
    },
    container: {
        padding: 10,
        justifyContent: 'center'
    },
    titleWraper: {
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    name: {
        fontSize: 16,
        fontWeight: 'bold',
        marginLeft: 10
    },
    subTitle: {
        fontSize: 12,
        color: color.g500,
        marginLeft: 10
    },
    date: {
        color: color.g500
    },
    logo: {
        height: 40,
        width: 150
    }
})

export default withNavigation(HeaderSearch)