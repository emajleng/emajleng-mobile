import React from 'react';
import { View } from 'react-native';
import { color, style } from '_styles';

export const Divider = (props) => {
    return (
        <View style={[{ height: 8, backgroundColor: color.g300, width: '100%' }, props.style]} />
    )
}