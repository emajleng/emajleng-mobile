import React from 'react';
import { TouchableHighlight, Text, StyleSheet, View } from 'react-native';
import { color } from '_styles';
import { ButtonText } from './Button';
import Icon from 'react-native-vector-icons/Ionicons';

const LineVertical = (props) => {
    return (
        <View style={[props.style, { width: props.width, height: props.height, backgroundColor: props.color }]}>

        </View>
    )
}

export {
    LineVertical
}