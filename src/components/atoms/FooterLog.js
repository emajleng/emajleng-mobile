import React from 'react';
import { TouchableHighlight, Text, StyleSheet, View } from 'react-native';
import { color } from '_styles';
import { ButtonText } from './Button';
import Icon from 'react-native-vector-icons/Ionicons';

const FooterLog = (props) => {
    return (
        <View style={[styles.wrapper, styles.flexRow]}>
            <View style={styles.flexRow}>
                <Text style={{ color: color.g700 }}>{props.text}</Text>
                <ButtonText title={props.textButton} onPress={props.onPress} color={color.primary} />
            </View>
            <Icon name='close-circle' color={color.g500} size={20} onPress={props.closePress} />
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        paddingHorizontal: 16,
        paddingVertical: 8,
        backgroundColor: color.p50,
        justifyContent: 'space-between',
        position: 'relative',
        bottom: 0,
    },
    flexRow: {
        flexDirection: 'row',
        alignItems: 'center'
    }
})


export {
    FooterLog
}