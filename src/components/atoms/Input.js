import React, { useState } from 'react';
import { StyleSheet, Text, TextInput, TouchableWithoutFeedback, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { color } from '_styles';
import { ButtonText } from './Button';

Icon.loadFont()

const Input = (props) => {
    const { material, materialColor, rounded, textColor, error } = props
    const height = 0
    return (
        <View style={{ paddingVertical: 4 }}>
            {
                props.label && <Text style={[styles.label, { paddingBottom: material && 0 }]}>{props.label}</Text>
            }
            <View onPress={props.onPress}>
                <View style={[styles.rounded, material && styles.material, styles.wrapper, props.style, { borderRadius: material ? 0 : 8, backgroundColor: material ? null : error ? color.r200 : props.backgroundColor ?? color.g200, borderColor: error ? color.r700 : materialColor ?? color.g500 }]}>
                    {
                        props.iconLeft &&
                        <Icon name={props.iconLeft} size={18} color={props.iconColor ?? color.g700} />
                    }
                    {
                        props.prefix && <Text style={{ color: color.g700 }}>{props.prefix} </Text>
                    }
                    <TextInput
                        ref={props.inputRef ?? null}
                        multiline={props.multiline}
                        value={props.value}
                        onChangeText={props.onChangeText}
                        secureTextEntry={props.password}
                        keyboardType={props.keyboardType}
                        style={[styles.input, { color: textColor ?? color.g800, paddingLeft: props.iconLeft ? 8 : 0, maxHeight: 80, height: props.height, paddingRight: props.iconRight && 8 }]}
                        placeholder={props.placeholder ?? props.label}
                    />
                    {
                        props.iconRight &&
                        <Icon name={props.iconRight} size={18} color={props.iconColor ?? color.g700} onPress={props.iconPress} />
                    }
                    {
                        props.buttonText &&
                        <ButtonText title={props.buttonText} onPress={props.buttonTextPress} />
                    }
                </View>
            </View>
            {
                props.subtitle &&
                <View style={{ paddingTop: 4 }}>
                    <Text style={[styles.subtitle, { color: error ? color.r700 : color.g600 }]} >{props.subtitle}</Text>
                </View>
            }
            {/* {
                props.error &&
                    props.errorText ?
                    <Text style={[styles.subtitle, { color: color.failed }]}>{props.errorText}</Text>
                    : props.subtitle ?
                        <Text style={styles.subtitle}>{props.subtitle}</Text>
                        : null
            } */}
        </View>
    )
}

const styles = StyleSheet.create({
    label: {
        paddingVertical: 6,
        fontWeight: 'bold',
        color: color.g800
    },
    material: {
        borderBottomWidth: 1,
        borderColor: color.g700,
    },
    rounded: {
        borderRadius: 8,
        padding: 10,
        paddingVertical: 4,
    },
    wrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    input: {
        flex: 1,
        color: color.g800,
        height: 40,
    },
    subtitle: {
        fontSize: 10,
    }
})

export {
    Input,
}