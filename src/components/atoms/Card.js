import React from 'react';
import { TouchableHighlight, Text, StyleSheet, View } from 'react-native';
import { style } from '_styles';


const Card = (props) => {
    return (
        <TouchableHighlight underlayColor='transparent' onPress={props.onPress} onLongPress={props.onLongPress} disabled={props.disabled} >
            <View style={[styles.wraper, style.shadow, props.style]}>
                {props.children}

            </View>
        </TouchableHighlight>
    )
}

const styles = StyleSheet.create({
    wraper: {
        borderRadius: 5,
        backgroundColor: '#fff',
        marginBottom: 10,
    }
})

export default Card