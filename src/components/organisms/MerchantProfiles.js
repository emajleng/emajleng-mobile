import React, { useEffect, useState } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { View, Dimensions, StyleSheet, ScrollView, StatusBar, Text, TouchableOpacity } from 'react-native';
import { Button, Card, LineVertical, HeaderProfile } from '_atoms';
import { CardProfile, ProfileMenu, ProfilMenuMerchant } from '_molecules';
import { setLoading, saveToken, setUser } from '_states/actions/user';
import { connect, batch } from 'react-redux';
import axios from 'axios';
import Icon from 'react-native-vector-icons/Ionicons';
import { color, style } from '_styles';

const MerchantProfiles = (props) => {

    const [toko, setToko] = useState({})
    console.log({ toko })
    const [active, setActive] = useState(false)


    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', () => getMerchant())
        return unsubscribe;
    }, [props.navigation])

    const getMerchant = () => {
        axios.get(`/user/merchant`)
            .then(res => {
                console.log({ res })
                setToko(res.data.data)
            }).catch(err => {
                console.log({ err })
                setToko(null)
            })
    }

    return (
        <>
            <ScrollView style={{ backgroundColor: color.g100 }}>
                <View >
                    {
                        toko ?
                            <>
                                <ProfilMenuMerchant
                                    data={Mmenu}
                                    merchant={toko.name}
                                    logo={toko.logo}
                                />
                                <View style={[style.shadow]}>
                                    <TouchableOpacity style={{ flexDirection: 'row', padding: 10, justifyContent: 'space-between' }} onPress={() => setActive(!active)}>
                                        <View style={{ flexDirection: 'row', paddingHorizontal: 5 }}>
                                            <Icon name={'stopwatch-outline'} size={18} color={props.iconColor ?? color.g700} />
                                            <Text style={{ marginLeft: 10 }}>Riwayat Transaksi</Text>
                                        </View>
                                        <Icon name={`chevron-${active ? 'down-' : 'forward-'}outline`} size={18} color={props.iconColor ?? color.g700} />
                                    </TouchableOpacity>

                                    {
                                        active == true &&
                                        <>
                                            <TouchableOpacity style={styles.container} onPress={() => props.navigation.navigate('TransactionListMerchant', { status: 'WAITING_PAYMENT' })} >
                                                <View style={{ flexDirection: 'row', paddingHorizontal: 5 }}>
                                                    {/* <Icon name={'timer-sharp'} size={18} color={props.iconColor ?? color.g700} /> */}
                                                    <Text style={{ marginLeft: 10 }}>Menunggu Pembayaran</Text>
                                                </View>
                                                <Icon name={`chevron-forward-outline`} size={18} color={props.iconColor ?? color.g700} />
                                            </TouchableOpacity>
                                            <TouchableOpacity style={styles.container} onPress={() => props.navigation.navigate('TransactionListMerchant', { status: 'REQUESTED' })} >
                                                <View style={{ flexDirection: 'row', paddingHorizontal: 5 }}>
                                                    {/* <Icon name={'hourglass-sharp'} size={18} color={props.iconColor ?? color.g700} /> */}
                                                    <Text style={{ marginLeft: 10 }}>Konfirmasi Pesanan</Text>
                                                </View>
                                                <Icon name={`chevron-forward-outline`} size={18} color={props.iconColor ?? color.g700} />
                                            </TouchableOpacity>
                                            <TouchableOpacity style={styles.container} onPress={() => props.navigation.navigate('TransactionListMerchant', { status: 'PROCESSED' })} >
                                                <View style={{ flexDirection: 'row', paddingHorizontal: 5 }}>
                                                    {/* <Icon name={'refresh-circle-sharp'} size={18} color={props.iconColor ?? color.g700} /> */}
                                                    <Text style={{ marginLeft: 10 }}>Proses Pesanan</Text>
                                                </View>
                                                <Icon name={`chevron-forward-outline`} size={18} color={props.iconColor ?? color.g700} />
                                            </TouchableOpacity>
                                            <TouchableOpacity style={styles.container} onPress={() => props.navigation.navigate('TransactionListMerchant', { status: 'DELIVERING' })} >
                                                <View style={{ flexDirection: 'row', paddingHorizontal: 5 }}>
                                                    {/* <Icon name={'walk-sharp'} size={18} color={props.iconColor ?? color.g700} /> */}
                                                    <Text style={{ marginLeft: 10 }}>Pesanan Diantar</Text>
                                                </View>
                                                <Icon name={`chevron-forward-outline`} size={18} color={props.iconColor ?? color.g700} />
                                            </TouchableOpacity>
                                            <TouchableOpacity style={styles.container} onPress={() => props.navigation.navigate('TransactionListMerchant', { status: 'FINISHED' })} >
                                                <View style={{ flexDirection: 'row', paddingHorizontal: 5 }}>
                                                    {/* <Icon name={'cloud-done-sharp'} size={18} color={props.iconColor ?? color.g700} /> */}
                                                    <Text style={{ marginLeft: 10 }}>Pesanan Sampai</Text>
                                                </View>
                                                <Icon name={`chevron-forward-outline`} size={18} color={props.iconColor ?? color.g700} />
                                            </TouchableOpacity>
                                            <TouchableOpacity style={styles.container} onPress={() => props.navigation.navigate('TransactionListMerchant', { status: 'REJECTED' })} >
                                                <View style={{ flexDirection: 'row', paddingHorizontal: 5 }}>
                                                    {/* <Icon name={'cloud-done-sharp'} size={18} color={props.iconColor ?? color.g700} /> */}
                                                    <Text style={{ marginLeft: 10 }}>Pesanan Dibatalkan</Text>
                                                </View>
                                                <Icon name={`chevron-forward-outline`} size={18} color={props.iconColor ?? color.g700} />
                                            </TouchableOpacity>
                                        </>
                                    }
                                </View>

                            </>

                            :
                            <Card style={{ marginTop: 10, marginLeft: 5, marginRight: 5, marginTop: 20 }}>
                                <TouchableOpacity style={styles.logout} onPress={() => props.navigation.navigate('MerchantProfile')}>
                                    <Text style={styles.logoutText}>Buat Toko </Text>
                                    <Icon name={'create-outline'} size={18} color={color.failed} />
                                </TouchableOpacity>
                            </Card>

                    }

                </View>
                <Text style={styles.version}>Version 0.0.1</Text>
                <Text style={[styles.version, { color: color.g500 }]}>25/november/2020</Text>
            </ScrollView>
        </>
    )
}

const { width, height } = Dimensions.get('screen')
const styles = StyleSheet.create({
    container: {
        marginLeft: 30,
        marginRight: 10,
        flexDirection: 'row',
        padding: 10,
        justifyContent: 'space-between',
        // borderColor: color.secondary,
        borderWidth: 2,
        borderRadius: 5,
        marginVertical: 5
    },
    button: {
        bottom: 0,
        position: 'relative',
        marginTop: 200
    },
    logout: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10
    },
    logoutText: {
        fontSize: 15,
        color: color.failed
    },
    version: {
        alignSelf: 'center'
    }
})

const Mmenu = [
    {
        name: 'Produk',
        icon: 'list-outline',
        iconRight: 'chevron-forward-outline',
        route: 'product'
    },
    {
        name: 'Edit Toko',
        icon: 'create-outline',
        iconRight: 'chevron-forward-outline',
        route: 'UpdateMerchant'
    },
    {
        name: 'Alamat Pengiriman',
        icon: 'list-outline',
        iconRight: 'chevron-forward-outline',
        route: 'AddOriginAddress'
    }
]

const mapStateToProps = state => {
    return {
        ava: state.user.data.profile_image,
        user: state.user.data.user,
        pin: state.user.data.pin,
        credential: state.user.data.credential,
        isAuthLoading: state.user.isAuthLoading,
        merchant: state.user.merchant,
    }
}

export default connect(mapStateToProps)(MerchantProfiles);
