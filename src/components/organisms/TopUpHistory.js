import React, { useEffect, useState } from 'react';
import { Text, StatusBar, View, FlatList, StyleSheet, TouchableHighlight, Dimensions } from 'react-native';
import { Divider } from '_atoms';
import { color, style } from '_styles';
import Icon from 'react-native-vector-icons/Ionicons';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import { withNavigation } from '@react-navigation/compat';
import { autoCurency } from '_utils';
import { connect } from 'react-redux';
import axios from 'axios';
import moment from 'moment';
import 'moment/min/locales';

const TopUpHistory = (props) => {

    const [histories, setHistories] = useState([])
    const [loading, setLoading] = useState(false)
    const [refresh, setRefresh] = useState(false)
    const [page, setPage] = useState(0)
    const [totalPage, setTotalPage] = useState(0)

    useEffect(() => {
        getHistories()
    }, [])

    const getHistories = () => {
        setLoading(true)
        axios.get(`topup?page=${0}`)
            .finally(() => {
                setLoading(false)
            }).then(res => {
                console.log({ res })
                setHistories(res.data.data)
                setPage(parseInt(res.data.paging.page) + 1)
                setTotalPage(res.data.paging.total_page)
            })
    }

    const getMoreHistories = () => {
        setRefresh(true)
        axios.get(`/topup?type=${props.type}&page=${page}`)
            .finally(() => {
                setRefresh(false)
            }).then(res => {
                setPage(v => v + 1)
                setHistories(v => v.concat(res.data.data))
            })
    }

    const handleStatus = (v) => {
        switch (v) {
            case 'PAID':
                return <Icon name='checkmark-circle' color={color.success} />
            case 'PENDING':
                return <Icon name='remove-circle' color={color.g700} />
            case 'CANCEL':
                return <Icon name='close-circle' color={color.failed} />
        }
    }
    const handleStatusColor = (v) => {
        switch (v) {
            case 'PAID':
                return color.success
            case 'PENDING':
                return color.g700
            case 'CANCEL':
                return color.failed
        }
    }

    const renderHistory = ({ item }) => {
        return (
            <>
                <TouchableHighlight underlayColor='transparent' onPress={() => props.navigation.navigate('TopUpInvoice', { data: item, paymentMethod: item.payment_method })}>
                    <View style={[styles.history]}>
                        <View>
                            <Text style={styles.title}>{item.payment_method?.bank_name}</Text>
                            <Text style={styles.price}>{autoCurency(item.amount + item.unique_code)}</Text>
                        </View>
                        <View style={{ alignItems: 'flex-end' }}>
                            <Text style={[styles.status, { color: handleStatusColor(item.status) }]}>{item.status} {handleStatus(item.status)}</Text>
                            <Text style={styles.date}>{moment(item.payment_expired).locale('id').format('DD MMM YY HH:mm')}</Text>
                        </View>
                    </View>
                </TouchableHighlight>
                <Divider />
            </>
        )
    }

    return (
        <SkeletonContent
            containerStyle={styles.skeletonContainer}
            isLoading={loading}
            layout={[styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton]}
        >
            <View style={{ flex: 1, width: width }}>
                <FlatList
                    data={histories}
                    renderItem={renderHistory}
                    refreshing={refresh}
                    onEndReached={(page < totalPage) && getMoreHistories}
                    onEndReachedThreshold={0.1}
                    onRefresh={getHistories}
                    // ListEmptyComponent={<EmptyList title='Ooops!' description='Belum ada riwayat untuk tipe transaksi ini, mulai transaksi yuk!' />}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        </SkeletonContent>
    )
}

const { width } = Dimensions.get('screen')

const styles = StyleSheet.create({
    history: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10
    },
    title: {
        fontWeight: 'bold',
        color: color.g800,
        marginBottom: 5,
    },
    customerNumber: {
        fontSize: 12,
        color: color.g700

    },
    date: {
        fontSize: 12,
        color: color.g500
    },
    price: {
        fontWeight: 'bold',
        color: color.secondary
    },
    status: {
        fontWeight: 'bold',
        marginBottom: 10,
    },
    skeleton: {
        width: width,
        height: 65,
        marginVertical: 3
    },
    skeletonContainer: {
        flex: 1,
    },
})

const mapStateToProps = state => {
    return {
        user: state.user.data
    }
}

export default connect(mapStateToProps)(withNavigation(TopUpHistory));