// import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, Image, StyleSheet, Dimensions, TouchableHighlight, Animated, StatusBar, Platform } from 'react-native';
import { color, style } from '_styles';
import { autoCurency } from '_utils';
import { BallIndicator } from 'react-native-indicators';
import { withNavigation } from '@react-navigation/compat';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';

const HomeProductList = (props) => {

    const renderProduct = ({ item }) => {
        return (
            <SkeletonContent
                containerStyle={styles.skeletonContainer}
                // isLoading={props.isLoading}
                isLoading={false}
                layout={[styles.skeleton]}
            >
                <TouchableHighlight underlayColor='transparent' onPress={() => props.navigation.push('DetailProduct', { item })} >
                    {/* <TouchableHighlight underlayColor='transparent' onPress={() => props.navigation.push('DetailProduct', { item })}> */}
                    <View style={[styles.itemContainer, style.shadow]}>
                        {/* <Image source={{ uri: item.product_img_url ?? item.product_display }} style={styles.image} /> */}
                        <Image source={{ uri: item.photo }} style={styles.image} />
                        <View style={styles.infoContainer}>
                            <Text style={{ color: color.g800, fontWeight: 'bold' }} numberOfLines={2}>{item.name}</Text>
                            <Text style={{ color: color.secondary }} numberOfLines={2}>{autoCurency(item.price)}</Text>
                        </View>
                        <Text style={{ color: color.failed, fontSize: 10, right: 0, position: 'absolute', bottom: 7, padding: 5 }} numberOfLines={2}>Stok: {item.stock}</Text>
                    </View>
                </TouchableHighlight>
            </SkeletonContent>
        )
    }

    return (
        <View style={{ marginTop: -(58 + StatusBar.currentHeight), flex: 1 }}>
            <FlatList
                data={props.data}
                keyExtractor={(item, index) => index.toString()}
                renderItem={renderProduct}
                numColumns={2}
                ListHeaderComponent={props.headerComponent}
                refreshing={props.refreshing}
                onRefresh={props.onRefresh}
                onEndReached={props.loadMore}
                onEndReachedThreshold={0.1}
                ListEmptyComponent={props.empty}
                showsVerticalScrollIndicator={false}
                onScroll={props.onScroll} />
            {/* {props.loadingMore && <BallIndicator color={color.primary} size={18} style={{ position: 'absolute', bottom: 10, alignSelf: 'center' }} />} */}
        </View>
    )
}

const { width } = Dimensions.get('screen')


const styles = StyleSheet.create({
    filter: {
        padding: 10,
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderColor: color.g300
    },
    footer: {
        flexDirection: 'row',
        padding: 10,
        borderTopWidth: 1,
        borderColor: color.g300
    },
    container: {
        backgroundColor: color.bglight,
        flex: 1,
    },
    itemContainer: {
        width: (width / 2) - 15,
        margin: 10,
        marginRight: 0,
        borderRadius: 5
    },
    infoContainer: {
        padding: 10
    },
    image: {
        width: '100%',
        height: 150,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    },
    priceText: {
        fontWeight: 'bold',
        color: color.secondary,
        marginTop: 5
    },
    moreText: {
        fontWeight: 'bold',
        color: color.primary,
        marginRight: 15
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 15,
        marginBottom: 5
    },
    icon: {
        marginRight: 10,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: 'red',
        width: 34,
        height: 34,
        textAlignVertical: 'center',
        textAlign: 'center'
    },
    priceDisc: {
        fontSize: 12
    },
    skeleton: {
        width: (width / 2) - 14,
        height: 220,
        margin: 7,
        marginVertical: 10
    },
    skeletonContainer: {
        flex: 1,
    },
})

const arrayColor = [
    {
        color: 'red'
    },
    {
        color: 'blue'
    },

]

export default withNavigation(HomeProductList);