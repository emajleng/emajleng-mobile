import React, { useEffect, useState } from 'react';
import { View, Text, FlatList, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { EmptyList } from '_atoms'
import { Loading } from '_molecules'
import { color } from '_styles';
import { withNavigation } from '@react-navigation/compat';
import { connect } from 'react-redux';
import axios from 'axios';
import { URI } from '_utils/environment';


const ChatFirebaseList = (props) => {

    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)
    const [directInfo, setDirectInfo] = useState({})
    const type = props.type
    // console.log({ type })

    useEffect(() => {
        getListUser()
    }, [])

    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', () => getListUser())
        return unsubscribe;
    }, [props.navigation])

    const getListUser = () => {
        setLoading(true)
        axios.get(`/chat/${type == 'user' ? 'user' : 'merchant'}`)
            .finally(() => { setLoading(false) })
            .then(res => {
                // console.log({ res })
                setData(res.data.data)
            })

    }

    const handleEmpty = () => {
        return (
            <EmptyList title='Opsss!' description='Belum Ada Pesan' />
        )
    }

    const renderData = ({ item }) => {
        // console.log({ item })
        return (
            <TouchableOpacity style={styles.containerList} onPress={() => props.navigation.navigate('chatRoom', {
                name: item.name,
                type: type,
                userId: item.user_id,
                merchantId: item.merchant_id
            })
            }>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image source={{ uri: item.logo ? `${URI}/${item.logo}` : 'https://atos.net/wp-content/uploads/2017/05/Profile_gray.png' }} style={styles.ava} />
                    <Text style={styles.name}>{item.name}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    return (
        <>
            <FlatList
                style={{ backgroundColor: '#fff', flex: 1 }}
                data={data}
                ListEmptyComponent={handleEmpty}
                keyExtractor={(item, index) => index.toString()}
                renderItem={renderData} />
            <Loading isLoading={loading} />
        </>
    )
}

const styles = StyleSheet.create({
    containerList: {
        padding: 10,
        borderBottomWidth: 1,
        borderColor: color.g300,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    ava: {
        width: 50,
        height: 50,
        borderRadius: 25,
        marginRight: 10
    },
    name: {
        fontWeight: 'bold',
        color: color.g800
    },
    label: {
        backgroundColor: color.failed,
        width: 20,
        height: 20,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    labelText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 12
    }
})

const mapStateToProps = state => {
    return {
        // user: state.user.data,
        // merchant: state.merchant.data,
    }
}
export default connect(mapStateToProps)(withNavigation(ChatFirebaseList));
