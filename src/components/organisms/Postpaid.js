import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { Input, Button } from '_atoms';
import { withNavigation } from '@react-navigation/compat';
import axios from 'axios';

const Prepaid = (props) => {

    const [customerNumber, setCustomerNumber] = useState('538311276829')
    const [loading, setLoading] = useState(false)

    const inquiry = () => {
        setLoading(true)
        axios.post(`/transaction-ppob/pasca`, {
            customer_no: customerNumber,
            type: "INQUIRY"
        })
            .finally(() => {
                setLoading(false)
            })
            .then(res => {
                console.log({ res })
                props.navigation.navigate('CheckoutPostpaid', { data: res.data.data })
            })
    }

    return (
        <View style={styles.container}>
            <Input
                material
                keyboardType='phone-pad'
                value={customerNumber}
                onChangeText={v => setCustomerNumber(v)}
                label='No. Meter/ID Pelanggan'
                placeholder='5300XXXXXXX' />
            <Button title='Selanjutnya' loading={loading} disabled={loading} onPress={inquiry} />
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        backgroundColor: '#fff'
    }
})

export default withNavigation(Prepaid);