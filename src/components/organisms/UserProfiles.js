import React, { useEffect, useState } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { View, Dimensions, StyleSheet, ScrollView, Linking, Text, TouchableOpacity } from 'react-native';
import { Button, Card, LineVertical, HeaderProfile } from '_atoms';
import { CardProfile, ProfileMenu, ProfilMenuMerchant } from '_molecules';
import { setLoading, saveToken, setUser } from '_states/actions/user';
import { connect, batch } from 'react-redux';
import axios from 'axios';
import Icon from 'react-native-vector-icons/Ionicons';
import { color, style } from '_styles';

const UserProfiles = (props) => {

    const user = props.user
    const [toko, setToko] = useState({})
    const [active, setActive] = useState(false)
    console.log({ user })

    const onLogout = () => {
        AsyncStorage.clear().then(() => {
            batch(() => {
                props.dispatch(saveToken(null))
                props.dispatch(setLoading(true))
            })
        })
        axios.post(`/auth/logout`)
    }

    const admin = () => {
        let msg = 'halo admin ' + ' saya member di aplikasi Padi raharja, ingin menanyakan beberapa hal';
        let phoneWithCountryCode = '+6282240517705';
        let mobile = Platform.OS == 'ios' ? phoneWithCountryCode : '+' + phoneWithCountryCode;
        if (mobile) {
            if (msg) {
                let url = 'whatsapp://send?text=' + msg + '&phone=' + mobile;
                Linking.openURL(url).then((data) => {
                    console.log('WhatsApp Opened');
                }).catch(() => {
                    alert('Make sure WhatsApp installed on your device');
                });
            } else {
                alert('Please insert message to send');
            }
        } else {
            alert('Please insert mobile no');
        }
    }

    return (
        <>
            <ScrollView style={{ backgroundColor: color.g100 }}>
                <View >
                    <ProfileMenu
                        data={Umenu}
                        user={user.name}
                        hp={user.phone}
                        email={user.email}
                    />
                    <View style={[style.shadow]}>
                        <TouchableOpacity style={{ flexDirection: 'row', padding: 10, justifyContent: 'space-between' }} onPress={() => setActive(!active)}>
                            <View style={{ flexDirection: 'row', paddingHorizontal: 5 }}>
                                <Icon name={'stopwatch-outline'} size={18} color={props.iconColor ?? color.g700} />
                                <Text style={{ marginLeft: 10 }}>Riwayat Transaksi</Text>
                            </View>
                            <Icon name={`chevron-${active ? 'down-' : 'forward-'}outline`} size={18} color={props.iconColor ?? color.g700} />
                        </TouchableOpacity>

                        {
                            active == true &&
                            <>
                                <TouchableOpacity style={styles.container} onPress={() => props.navigation.navigate('TransactionListUser', { status: 'WAITING_PAYMENT' })} >
                                    <View style={{ flexDirection: 'row', paddingHorizontal: 5 }}>
                                        {/* <Icon name={'timer-sharp'} size={18} color={props.iconColor ?? color.g700} /> */}
                                        <Text style={{ marginLeft: 10 }}>Menunggu Pembayaran</Text>
                                    </View>
                                    <Icon name={`chevron-forward-outline`} size={18} color={props.iconColor ?? color.g700} />
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.container} onPress={() => props.navigation.navigate('TransactionListUser', { status: 'REQUESTED' })} >
                                    <View style={{ flexDirection: 'row', paddingHorizontal: 5 }}>
                                        {/* <Icon name={'hourglass-sharp'} size={18} color={props.iconColor ?? color.g700} /> */}
                                        <Text style={{ marginLeft: 10 }}>Menunggu Konfirmasi</Text>
                                    </View>
                                    <Icon name={`chevron-forward-outline`} size={18} color={props.iconColor ?? color.g700} />
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.container} onPress={() => props.navigation.navigate('TransactionListUser', { status: 'PROCESSED' })} >
                                    <View style={{ flexDirection: 'row', paddingHorizontal: 5 }}>
                                        {/* <Icon name={'refresh-circle-sharp'} size={18} color={props.iconColor ?? color.g700} /> */}
                                        <Text style={{ marginLeft: 10 }}>Pesanan Diproses</Text>
                                    </View>
                                    <Icon name={`chevron-forward-outline`} size={18} color={props.iconColor ?? color.g700} />
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.container} onPress={() => props.navigation.navigate('TransactionListUser', { status: 'DELIVERING' })}>
                                    <View style={{ flexDirection: 'row', paddingHorizontal: 5 }}>
                                        {/* <Icon name={'walk-sharp'} size={18} color={props.iconColor ?? color.g700} /> */}
                                        <Text style={{ marginLeft: 10 }}>Pesanan Diantar</Text>
                                    </View>
                                    <Icon name={`chevron-forward-outline`} size={18} color={props.iconColor ?? color.g700} />
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.container} onPress={() => props.navigation.navigate('TransactionListUser', { status: 'FINISHED' })} >
                                    <View style={{ flexDirection: 'row', paddingHorizontal: 5 }}>
                                        {/* <Icon name={'cloud-done-sharp'} size={18} color={props.iconColor ?? color.g700} /> */}
                                        <Text style={{ marginLeft: 10 }}>Pesanan Sampai</Text>
                                    </View>
                                    <Icon name={`chevron-forward-outline`} size={18} color={props.iconColor ?? color.g700} />
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.container} onPress={() => props.navigation.navigate('TransactionListUser', { status: 'REJECTED' })} >
                                    <View style={{ flexDirection: 'row', paddingHorizontal: 5 }}>
                                        {/* <Icon name={'cloud-done-sharp'} size={18} color={props.iconColor ?? color.g700} /> */}
                                        <Text style={{ marginLeft: 10 }}>Pesanan Dibatalkan</Text>
                                    </View>
                                    <Icon name={`chevron-forward-outline`} size={18} color={props.iconColor ?? color.g700} />
                                </TouchableOpacity>

                            </>
                        }
                    </View>

                    <Card style={{ marginTop: 10 }}>
                        <TouchableOpacity style={styles.logout} onPress={() => admin()}>
                            <Text style={styles.logoutText}> Hubungi admin </Text>
                            <Icon name={'chatbubble-ellipses-outline'} size={18} color={color.failed} />
                        </TouchableOpacity>
                    </Card>
                    <Card style={{ marginTop: 10 }}>
                        <TouchableOpacity style={styles.logout} onPress={onLogout}>
                            <Text style={styles.logoutText}> Logout </Text>
                            <Icon name={'power'} size={18} color={color.failed} />
                        </TouchableOpacity>
                    </Card>
                </View>
                <Text style={styles.version}>Version 0.0.1</Text>
                <Text style={[styles.version, { color: color.g500 }]}>25/november/2020</Text>
            </ScrollView>
        </>
    )
}

const { width, height } = Dimensions.get('screen')
const styles = StyleSheet.create({
    container: {
        marginLeft: 30,
        marginRight: 10,
        flexDirection: 'row',
        padding: 10,
        justifyContent: 'space-between',
        // borderColor: color.secondary,
        borderWidth: 2,
        borderRadius: 5,
        marginVertical: 5
    }
    ,
    button: {
        bottom: 0,
        position: 'relative',
        marginTop: 200
    },
    logout: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10
    },
    logoutText: {
        fontSize: 15,
        color: color.failed
    },
    version: {
        alignSelf: 'center'
    }
})

const Umenu = [
    {
        name: 'Profil',
        icon: 'create-outline',
        iconRight: 'chevron-forward-outline',
        route: 'UserProfile'
    },
    // {
    //     name: 'Ganti Password',
    //     icon: 'key'
    // },
    {
        name: 'Kebijalan Privasi',
        icon: 'document',
        iconRight: 'chevron-forward-outline',
        route: 'PrivacyPollice'
    },
    {
        name: 'Tambah Bank',
        icon: 'stopwatch-outline',
        iconRight: 'chevron-forward-outline',
        route: 'UserBank'
    },

]

const mapStateToProps = state => {
    return {
        user: state.user.data,
    }
}

export default connect(mapStateToProps)(UserProfiles);
