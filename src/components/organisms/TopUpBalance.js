import React, { useState } from 'react';
import { View, Text, ScrollView, StyleSheet, FlatList, TouchableHighlight, Image, Alert, Dimensions } from 'react-native';
import { SelectBank } from '_molecules';
import { withNavigation } from '@react-navigation/compat';
import { Card, Input, Button, Divider } from '_atoms';
import { deCurency, autoCurency } from '_utils';
import { style, color } from '_styles';
import Icon from 'react-native-vector-icons/Ionicons';
import axios from 'axios';
import Toast from 'react-native-simple-toast';

const TopUpBalance = (props) => {

    const [amount, setAmount] = useState(0)
    const [paymentMethod, setPaymentMethod] = useState({})
    const [loading, setLoading] = useState(false)

    const topUp = () => {
        setLoading(true)
        axios.post(`topup`,
            {
                amount,
                payment_method_id: paymentMethod.id
            }
        ).finally(() => {
            setLoading(false)
        }).then(res => {
            // console.log({ res })
            Toast.show('TopUp berhasil', Toast.LONG)
            props.navigate({ data: res.data.data, paymentMethod })
        })
    }

    const renderPayment = ({ item }) => {
        return (
            <TouchableHighlight underlayColor='transparent' onPress={() => Alert.alert('', 'Akan tersedia dalam waktu dekat.')}>
                <View style={[style.flexRow, styles.paymentList]}>
                    <View style={styles.paymentName}>
                        <Image source={item.icon} style={{ height: 30, width: 50 }} resizeMode='contain' />
                        <Text style={styles.paymentText}>{item.name}</Text>
                    </View>
                    <Icon name='chevron-forward' size={20} color={color.g700} />
                </View>
            </TouchableHighlight>
        )
    }

    return (
        <ScrollView style={styles.container}>
            <Card style={{ margin: 10, padding: 10 }}>
                <Text style={styles.title}>Transfer Bank</Text>
                <Input
                    material
                    value={autoCurency(amount)}
                    label='Nominal'
                    subtitle='*Minimal Rp. 50.000'
                    keyboardType='phone-pad'
                    onChangeText={(v) => setAmount(deCurency(v))}
                />
                <SelectBank
                    onSelect={(v) => setPaymentMethod(v)}
                    selected={paymentMethod}
                />
                <Button title='Top Up' loading={loading} disable={loading || !(amount > 49000) || !paymentMethod.id} theme={color.secondary} onPress={topUp} />
            </Card>
            {/* <Divider />
            <Text style={styles.otherPaymentText}>Metode Pembayaran Lainnya</Text>
            <FlatList
                data={payments}
                keyExtractor={(item, index) => index.toString()}
                renderItem={renderPayment}
            /> */}
        </ScrollView>
    )
}

const { width } = Dimensions.get('screen')

const styles = StyleSheet.create({
    container: {
        width: width
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold',
        color: color.g800,
        marginBottom: 10,
        borderBottomWidth: 1,
        borderColor: color.g300,
        paddingBottom: 5
    },
    paymentList: {
        borderBottomWidth: 1,
        borderColor: color.g300,
        alignItems: 'center'
    },
    paymentName: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    paymentText: {
        color: color.g800,
        marginLeft: 10
    },
    otherPaymentText: {
        fontWeight: 'bold',
        color: color.g800,
        margin: 10
    }
})

const payments = [
    {
        'icon': require('_assets/icons/gopay.png'),
        'name': 'Gopay',
        'code': 'ALFAMART',
    },
    {
        'icon': require('_assets/icons/ovo.png'),
        'name': 'OVO',
        'code': 'INDOMARET',
    },
    {
        'icon': require('_assets/icons/alfamart.png'),
        'name': 'Alfamart',
        'code': 'ALFAMART',
    },
    {
        'icon': require('_assets/icons/indomaret.png'),
        'name': 'Indomaret',
        'code': 'INDOMARET',
    },
]

export default withNavigation(TopUpBalance);