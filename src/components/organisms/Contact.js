import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, Image, FlatList, TouchableHighlight, PermissionsAndroid, Alert } from 'react-native';
import { color, style } from '_styles';
import Icon from 'react-native-vector-icons/Ionicons';
import { Button, Input } from '_atoms';
import { useHeaderHeight } from '@react-navigation/stack';
import { ModalScene } from '_molecules';
import { autoCorectPhone } from '_utils';
import Contacts from 'react-native-contacts';

const Contact = (props) => {

    const headerHeight = useHeaderHeight();
    const [contacts, setContacts] = useState([])
    const [selected, setSelected] = useState({})
    const [key, setKey] = useState('')

    useEffect(() => {
        getContact()
    }, [])

    const getContact = () => {
        PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
            {
                'title': 'Contacts',
                'message': 'Aplikasi memerlukan izin untuk mengakses contact anda.',
                'buttonPositive': 'Izinkan'
            }
        ).then((permission) => {
            if (permission == 'granted') {
                Contacts.getAll((err, contacts) => {
                    console.log({ contacts })
                    if (err === 'denied') {
                        Alert.alert('', 'Jika izin tidak diberikan, kamu tidak dapat membeli produk dengan nomor dari kontak kamu.')
                    } else {
                        setContacts(contacts)
                    }
                })
            }
        })
    }

    const hanldeOnPress = () => {
        let phone = autoCorectPhone(selected.phoneNumbers[0].number)
        props.onSelect(phone)
        props.toggle()
    }

    const renderContact = ({ item }) => {
        const isSelected = (item.rawContactId ?? item.recordID) == (selected.rawContactId ?? selected.recordID)
        return (
            <TouchableHighlight underlayColor='transparent' onPress={() => setSelected(item)}>
                <View style={styles.renderContact}>
                    <Image source={require('_assets/icons/contact.png')} style={{ height: 40, width: 40 }} resizeMode='contain' />
                    <View>
                        <Text style={{ marginLeft: 10, color: isSelected ? color.g900 : color.g800, fontWeight: isSelected ? 'bold' : 'normal' }}>{item.displayName ?? item.givenName}</Text>
                        {item.phoneNumbers.length != 0 && <Text style={{ marginLeft: 10, fontSize: 12, color: isSelected ? color.g900 : color.g700, fontWeight: isSelected ? 'bold' : 'normal' }}>{item.phoneNumbers[0].number}</Text>}
                    </View>
                </View>
            </TouchableHighlight>
        )
    }

    return (
        <ModalScene isVisible={props.isVisible} toggle={props.toggle} title='Daftar Kontak'>
            <View style={{ flex: 1 }}>
                <View style={{ padding: 10 }}>
                    <Input placeholder='Cari...' icon='search' onChangeText={(v) => setKey(v)} />
                </View>
                <FlatList
                    data={contacts.filter(v => {
                        const name = v.displayName ?? v.givenName
                        return name?.toLowerCase().match(key.toLowerCase()) && v.phoneNumbers.length > 0
                    })}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={renderContact} />
            </View>
            <View style={styles.footer}>
                <Button title='Pilih' onPress={hanldeOnPress} />
            </View>
        </ModalScene>
    )
}


const styles = StyleSheet.create({
    wraper: {
        flex: 1,
        backgroundColor: '#fff',
    },
    header: {
        height: 56,
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderColor: color.g300,
        padding: 10,
        alignItems: 'center',
        flexDirection: 'row'
    },
    textHeader: {
        marginLeft: 20,
        fontSize: 16,
        fontWeight: 'bold',
    },
    renderContact: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
        borderBottomWidth: 1,
        borderColor: color.g300,
        backgroundColor: '#fff'
    },
    bankName: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    selected: {
        flexDirection: 'row',
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderColor: color.g300,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    label: {
        fontSize: 12,
        fontWeight: 'bold',
        color: color.g800,
        marginTop: 5,
    },
    footer: {
        padding: 10,
        borderTopWidth: 1,
        borderColor: color.g300
    }
})

export default Contact;