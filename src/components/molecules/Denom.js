import React from 'react';
import { Text, StyleSheet, FlatList, Dimensions, View } from 'react-native';
import { Card } from '_atoms';
import { color } from '_styles';
import { autoCurency } from '_utils';

const Denom = (props) => {

    const renderDenom = ({ item }) => {
        // console.log({ item })
        const isSelected = props.selected.id == item.id
        const active = item.seller_product_status && (item.unlimited_stock ? true : item.stock > 0)
        const adm = parseInt(item.admin_fee)
        return (
            <Card style={[styles.denom, { backgroundColor: !active ? color.g300 : isSelected ? color.p50 : '#fff' }]} onPress={() => props.onSelect(active ? item : {})}>
                {
                    item.multi &&
                    <View style={styles.label}>
                        <Text style={styles.multiText}>Multi</Text>
                    </View>
                }
                <Text style={styles.nominal}>{item.amount}</Text>
                <Text style={styles.price}>{autoCurency(item.price + adm)}</Text>
            </Card>
        )
    }

    return (
        <View style={{ paddingHorizontal: 5, flex: 1 }}>
            <View style={styles.infoContainer}>
                <Text style={styles.infoText}>Multi adalah dapat bertransaksi lebih dari 1 kali untuk 1 nomor yang sama, nominal yang sama, di hari yang sama.</Text>
            </View>
            <FlatList
                data={props.data.filter(v => v.status)}
                keyExtractor={(item, index) => index.toString()}
                numColumns={2}
                renderItem={renderDenom}
                showsVerticalScrollIndicator={false}
                ListEmptyComponent={props.onEmpty} />
        </View>
    );
}

const { width } = Dimensions.get('screen')

const styles = StyleSheet.create({
    denom: {
        padding: 10,
        margin: 5,
        width: width / 2 - 15,
        marginBottom: 10
    },
    nominal: {
        fontSize: 16,
        fontWeight: 'bold',
        color: color.g800
    },
    price: {
        fontSize: 12,
        fontWeight: 'bold',
        color: color.secondary,
        textAlign: 'right'
    },
    label: {
        backgroundColor: color.primary,
        position: 'absolute',
        right: 0,
        borderTopRightRadius: 5,
        borderBottomLeftRadius: 5,
        paddingHorizontal: 5
    },
    multiText: {
        color: '#fff',
        fontSize: 12
    },
    infoContainer: {
        margin: 5,
        borderColor: color.p100,
        backgroundColor: color.p50,
        borderWidth: 2,
        padding: 5,
        height: 50,
        justifyContent: 'center'
    },
    infoText: {
        color: color.g800,
        fontSize: 12,
        textAlign: 'center'
    }
})

export default Denom;