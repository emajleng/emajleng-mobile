import React from 'react';
import { View, Text, Image, StyleSheet, Modal } from 'react-native';
import { color } from '_styles';
import { BallIndicator } from 'react-native-indicators';

const Loading = (props) => {
    return (
        <Modal visible={props.isLoading} style={{ margin: 0 }} transparent={true} animationType='fade' >
            <View style={styles.wrap}>
                <BallIndicator color={'#fff'} size={30} />
                {/* <Image source={require('_assets/gold.gif')} style={{ width: 50, height: 50 }} /> */}
                {/* <LottieView
                    source={require('_assets/animations/loading.json')}
                    colorFilters={[{
                        keypath: "button",
                        color: "#F00000"
                    }, {
                        keypath: "Sending Loader",
                        color: "#F00000"
                    }]}
                    style={{
                        width: 200,
                        height: 200,
                        alignSelf: 'center'
                    }}
                    autoPlay
                    loop
                /> */}
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    wrap: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
})
export default Loading