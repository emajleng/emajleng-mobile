import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, Image, FlatList, TouchableHighlight, StatusBar, Dimensions } from 'react-native';
import Modal from 'react-native-modal';
import { color, style } from '_styles';
import Icon from 'react-native-vector-icons/Ionicons';
import { Button } from '_atoms';
import { getBankLogo } from '_utils';
import { useHeaderHeight } from '@react-navigation/stack';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import axios from 'axios';

const SelectUserBank = (props) => {

    const headerHeight = useHeaderHeight();
    const [modalVisible, setModalVisible] = useState(false)
    const [loading, setLoading] = useState(false)
    const [data, setData] = useState([])

    useEffect(() => {
        getPaymentMethod()
    }, [])

    const getPaymentMethod = () => {
        setLoading(true)
        axios.get(`/user_bank`)
            .then(res => {
                setLoading(false)
                setData(res.data.data)
                props.onSelect(res.data.data[0])

            })
    }

    const renderBank = ({ item }) => {
        const selected = props.selected.id == item.id
        return (
            <TouchableHighlight underlayColor='transparent' onPress={() => props.onSelect(item)}>
                <View style={styles.renderBank}>
                    <Image source={getBankLogo(item.bank_name)} style={{ height: 30, width: 50 }} resizeMode='contain' />
                    <Text style={{ marginLeft: 10, color: selected ? color.g900 : color.g700, fontWeight: selected ? 'bold' : 'normal' }}>Bank {item.bank_name}</Text>
                </View>
            </TouchableHighlight>
        )
    }
    return (
        <>
            <Text style={styles.label}>Bank Tujuan</Text>
            <TouchableHighlight underlayColor='transparent' onPress={() => setModalVisible(true)}>
                <View style={styles.selected}>
                    <View style={styles.bankName}>
                        <Image source={getBankLogo(props.selected.bank_name)} style={{ height: 30, width: 50 }} resizeMode='contain' />
                        <Text style={{ marginLeft: 10, color: color.g900, fontWeight: 'bold' }}>{props.selected.bank_name}</Text>
                    </View>
                    <Icon name='chevron-down-outline' size={22} color={color.g700} />
                </View>
            </TouchableHighlight>
            <Modal
                isVisible={modalVisible}
                style={{ margin: 0 }}
                swipeDirection='right'
                animationIn='slideInRight'
                animationOut='slideOutRight'
                hasBackdrop={false}
                onBackButtonPress={() => setModalVisible(false)}
                onSwipeComplete={() => setModalVisible(false)}
            >
                <View style={styles.wraper}>
                    <View style={[style.header, style.shadow, { height: headerHeight - StatusBar.currentHeight }]}>
                        <Icon name='close-outline' size={38} onPress={() => setModalVisible(false)} />
                        <Text style={styles.textHeader}>Pilih Bank</Text>
                    </View>
                    <SkeletonContent
                        containerStyle={styles.skeletonContainer}
                        isLoading={loading}
                        layout={[styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton, styles.skeleton]}
                    >
                        <FlatList
                            data={data}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={renderBank}
                        />
                        <Button title='Selesai' onPress={() => setModalVisible(false)} style={{ margin: 10, marginBottom: 10 }} />
                    </SkeletonContent>
                </View>
            </Modal>
        </>
    )
}

const { width } = Dimensions.get('screen')

const styles = StyleSheet.create({
    wraper: {
        flex: 1,
        backgroundColor: '#fff',
    },
    header: {
        height: 56,
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderColor: color.g300,
        padding: 10,
        alignItems: 'center',
        flexDirection: 'row'
    },
    textHeader: {
        marginLeft: 20,
        fontSize: 16,
        fontWeight: 'bold',
    },
    renderBank: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
        borderBottomWidth: 1,
        borderColor: color.g300,
        backgroundColor: '#fff'
    },
    bankName: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    selected: {
        flexDirection: 'row',
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderColor: color.g300,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    label: {
        fontSize: 12,
        fontWeight: 'bold',
        color: color.g800,
        marginTop: 5,
    },
    skeleton: {
        width: width,
        height: 48,
        marginVertical: 1
    },
    skeletonContainer: {
        flex: 1,
    },
})

export default SelectUserBank;