import React from 'react';
import { Dimensions, Text, StyleSheet, View, Image } from 'react-native';
import { style, color } from '_styles';
import { LineVertical } from '_atoms';
import { autoCurency } from '_utils';
import { Main } from './Menu';


const CardInfo = (props) => {
    return (
        <View style={[styles.wraper, style.shadow, props.style]}>
            <Image source={require('_assets/images/icon.png')} style={styles.logo} />
            <View style={styles.child}>
                <Text style={styles.title}>Saldo PadiRaharja</Text>
                <Text>{autoCurency(props.balance)}</Text>
            </View>
            <LineVertical width={1} height={50} color={color.secondary} />
            <Main onPress={props.navigate} />
        </View>

    )
}

const { width, height } = Dimensions.get('screen');

const styles = StyleSheet.create({
    wraper: {
        // paddingHorizontal: 5,
        borderRadius: 5,
        backgroundColor: '#fff',
        marginBottom: 10,
        width: width - 60,
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center'
    },
    child: {
        flexDirection: 'column',
        padding: 5
    },
    logo: {
        width: 40,
        height: 50,
        borderBottomLeftRadius: 10,
        borderBottomLeftRadius: 10
    },
    title: {
        fontSize: 12,
        color: color.g600
    }
})

export default CardInfo