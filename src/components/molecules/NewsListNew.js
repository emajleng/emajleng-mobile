import { useLinkProps } from '@react-navigation/native';
import React from 'react';
import { Text, View, FlatList, Image, StyleSheet, Dimensions, Animated, TouchableOpacity } from 'react-native';
import { color, style } from '_styles';
import { withNavigation } from '@react-navigation/compat';
import moment from 'moment'


const NewsListNew = (props) => {

    const renderItem = ({ item }) => {
        // console.log({ item })
        return (
            <TouchableOpacity style={[styles.containerV, style.shadow]} onPress={() => props.navigation.navigate(props.goto, { data: item })}>
                <Image source={{ uri: item.image }} style={styles.imgV} />
                <View style={styles.containerChildV}>
                    <Text style={{ fontSize: 12 }}>{moment(item.created_at).locale('id').add(3, 'hours').format('dddd, DD MMMM YYYY, HH:mm')}</Text>
                    <Text style={styles.judulV} numberOfLines={3}>{item.title}</Text>
                </View>
            </TouchableOpacity >
        )
    }

    return (
        <View style={[style.containerV, { marginBottom: 80 }]}>
            {/* <Text style={[styles.judulH, { paddingHorizontal: 10, fontSize: 18, paddingTop: 10 }]}>Berita Terbaru</Text> */}
            <Animated.FlatList
                data={props.data}
                keyExtractor={(item, index) => index.toString()}
                renderItem={renderItem}
            />
        </View>
    )

}

const { width, height } = Dimensions.get('screen')

const styles = StyleSheet.create({
    containerV: {
        marginVertical: 10,
        flexDirection: 'row',
        marginHorizontal: 5,
    },
    containerChildV: {
        padding: 5
    },
    imgV: {
        height: height / 7.5,
        width: width / 3
    },
    titleV: {
        width: width / 1.7,
        marginLeft: 10
    },
    judulV: {
        fontSize: 15,
        fontWeight: 'bold',
        color: color.g800,
        marginLeft: 10,
        width: width / 1.7
    }
})

export default withNavigation(NewsListNew)