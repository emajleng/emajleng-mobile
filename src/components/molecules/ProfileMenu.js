import { useLinkProps } from '@react-navigation/native';
import React from 'react';
import { Text, View, FlatList, Image, StyleSheet, Dimensions, Animated, TouchableOpacity } from 'react-native';
import { color, style } from '_styles';
import { Card, LineVertical, Divider } from '_atoms';
import Icon from 'react-native-vector-icons/Ionicons';
import { withNavigation } from '@react-navigation/compat';

const ProfileMenu = (props) => {

    const renderItem = ({ item }) => {
        return (
            <>
                <TouchableOpacity style={styles.containerChild} onPress={() => props.navigation.navigate(item.route, { data: props.data, initial: 1 })}>
                    <View style={{ flexDirection: 'row' }}>
                        <Icon name={item.icon} size={18} color={props.iconColor ?? color.g700} />
                        <Text style={styles.textMenu}> {item.name} </Text>
                    </View>
                    <Icon name={item.iconRight} size={18} color={props.iconColor ?? color.g700} />
                </TouchableOpacity>
                <LineVertical width={width} height={1} color={color.g400} />
            </>
        )
    }
    return (
        <View style={{ marginVertical: 5, width: width, alignSelf: 'center' }}>
            <Card>
                <View style={{ paddingHorizontal: 20, paddingVertical: 20 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.title}>Nama </Text>
                        <Text style={styles.title}>{props.user}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.title}>Nomor Hp </Text>
                        <Text style={styles.title}>{props.hp}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.title}>Email </Text>
                        <Text style={styles.title}>{props.email}</Text>
                    </View>
                </View>
                <Divider />
                <Animated.FlatList
                    data={props.data}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={renderItem}
                />
            </Card>
        </View>
    )
}

const { width, height } = Dimensions.get('screen')

const styles = StyleSheet.create({
    container: {
        width: width / 3,
        margin: 5,
    },
    containerChild: {
        padding: 15,
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    icon: {

    },
    textMenu: {
        fontSize: 15,
        marginLeft: 10
    },
    title: {
        fontSize: 16,
        color: color.g700
    }
})

export default withNavigation(ProfileMenu)