import React from 'react'
import { Dimensions, FlatList, Image, StyleSheet, Text, TouchableHighlight, View } from 'react-native'
import SkeletonContent from 'react-native-skeleton-content-nonexpo'
import { color, style } from '_styles'
import { autoCurency } from '_utils'
import { EmptyList, Underline } from '_atoms'
import moment from 'moment'
import { withNavigation } from '@react-navigation/compat';

const ProductSearchList = (props) => {

    const empty = () => {
        return (
            <EmptyList title={props.title ? props.title : 'Silakan Cari Produk Pilihanmu'} />
        )
    }

    const renderItem = ({ item }) => {
        // console.log({ item })
        return (
            // <SkeletonContent
            //     containerStyle={{ flex: 1 }}
            //     layout={[styles.skeletonOrder, styles.skeletonOrder, styles.skeletonOrder, styles.skeletonOrder]}
            //     isLoading={props.isLoading}>
            // </SkeletonContent>
            <TouchableHighlight onPress={() => props.navigation.navigate('DetailProduct', { item })}>
                <View style={[style.shadow, { borderRadius: 8, padding: 10, margin: 3, marginHorizontal: 16, marginBottom: 10 }]}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingVertical: 8 }}>
                        <View>
                            <Text style={{ color: color.g900 }}>{item.name}</Text>
                            <Text style={{ fontSize: 12, color: color.g500 }}>{moment(item.created_at).format('DD MMM YYYY')}</Text>
                        </View>
                        <View style={{ padding: 8, backgroundColor: color.primary, paddingVertical: 6, borderRadius: 6 }}>
                            <Text style={{ color: '#fff', fontSize: 12 }}>{item.merchant.name}</Text>
                        </View>
                    </View>
                    {/* <Underline /> */}
                    <View style={{ paddingTop: 12 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Image style={{ backgroundColor: color.g300, height: 50, width: 50, borderRadius: 8 }} source={{ uri: item.photo }} />
                            <View style={{ flex: 1, paddingLeft: 8 }}>
                                <Text style={{ color: color.g800 }}>{autoCurency(item.price)}</Text>
                                <Text style={{ color: color.g900 }} numberOfLines={2}>{item.description}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableHighlight>
        )
    }

    return (
        <View style={{ backgroundColor: '#fff', flex: 1 }}>
            <FlatList
                data={props.data}
                keyExtractor={(item, index) => index.toString()}
                renderItem={renderItem}
                ListEmptyComponent={empty}
                onEndReached={props.loadMore}
                onEndReachedThreshold={0.1}
                style={{ paddingTop: 16 }} />
        </View>
    )
}

const { width, height } = Dimensions.get('screen')

const styles = StyleSheet.create({
    skeletonOrder: {
        height: width / 2.4,
        width: width - 32,
        margin: 16,
        marginVertical: 4
    },
})

export default withNavigation(ProductSearchList)
