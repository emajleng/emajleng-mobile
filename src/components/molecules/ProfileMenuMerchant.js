import { useLinkProps } from '@react-navigation/native';
import React from 'react';
import { Text, View, FlatList, Image, StyleSheet, Dimensions, Animated, TouchableOpacity } from 'react-native';
import { color, style } from '_styles';
import { Card, LineVertical } from '_atoms';
import Icon from 'react-native-vector-icons/Ionicons';
import { withNavigation } from '@react-navigation/compat';

const ProfilMenuMerchant = (props) => {

    const renderItem = ({ item }) => {
        return (
            <>
                <TouchableOpacity style={styles.containerChild} onPress={() => props.navigation.navigate(item.route, { data: props.data })}>
                    <View style={{ flexDirection: 'row' }}>
                        <Icon name={item.icon} size={18} color={props.iconColor ?? color.g700} />
                        <Text style={styles.textMenu}> {item.name} </Text>
                    </View>
                    <Icon name={item.iconRight} size={18} color={props.iconColor ?? color.g700} />
                </TouchableOpacity>
                <LineVertical width={width} height={1} color={color.g400} />
            </>
        )
    }
    return (
        <View style={{ marginVertical: 5, width: width, alignSelf: 'center' }}>
            <Card>
                <View style={{ flexDirection: 'row', padding: 10 }}>
                    <Image source={{ uri: props.logo }} style={{ height: 50, width: 60, resizeMode: 'cover' }} />
                    <Text style={styles.title}>{props.merchant}</Text>
                </View>
                <Animated.FlatList
                    data={props.data}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={renderItem}
                />
            </Card>
        </View>
    )
}

const { width, height } = Dimensions.get('screen')

const styles = StyleSheet.create({
    container: {
        width: width / 3,
        margin: 5,
    },
    containerChild: {
        padding: 15,
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    icon: {

    },
    textMenu: {
        fontSize: 15,
        marginLeft: 10
    },
    title: {
        fontSize: 18,
        padding: 10
    }
})

export default withNavigation(ProfilMenuMerchant)