import React, { useState } from 'react';
import { TextInput, StyleSheet, View, Text, TouchableOpacity, FlatList, Image, TouchableHighlight } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'
import Modal from 'react-native-modal';
import { color, style } from '_styles';
import { Button, Input, Divider, Empty } from '_atoms';
import { withNavigation } from '@react-navigation/compat';
import { autoCurency } from '_utils';


const SelectSeacrh = (props) => {

    const [modalVisible, setModalVisible] = useState(false)
    const [selected, setSelected] = useState(props.value)
    const [key, setKey] = useState('')
    const val = props.data.filter(v => v.value == props.value)
    // console.log({ val })

    const handleOnSelect = () => {
        setModalVisible(false)
        props.onSelect(selected)
    }

    const renderSelect = ({ item }) => {
        const curent = selected?.value ?? selected
        const value = item.value
        const isSelected = value == curent
        return (
            <TouchableHighlight underlayColor='transparent' onPress={() => setSelected(item)}>
                <View style={styles.renderBank}>
                    <View style={{ flexDirection: 'row' }}>
                        {
                            item.icon &&
                            <Image source={{ uri: item.icon }} style={{ height: 30, width: 50 }} resizeMode='contain' />
                        }
                        <Text style={{ marginLeft: 10, color: isSelected ? color.g900 : color.g700, fontWeight: isSelected ? 'bold' : 'normal' }}>{item.label}</Text>
                    </View>
                    {
                        item.etd &&
                        <View style={{ alignItems: 'flex-end' }}>
                            <Text >Waktu {item.etd.map(val => val.etd)} Hari</Text>
                            <Text>{autoCurency(item.etd.map(val => val.value))}</Text>
                        </View>
                    }
                </View>
            </TouchableHighlight>
        )
    }

    return (
        <>
            {
                props.label ? <Text style={styles.label}>{props.label}</Text> : null
            }
            <View style={{ flexDirection: 'row' }}>
                <TouchableOpacity style={styles.wraper} onPress={() => setModalVisible(true)}>
                    <TextInput value={val[0]?.label} onChangeText={props.onChangeText} editable={false} placeholder={props.placeholder ?? props.label} placeholderTextColor={color.g400} style={styles.input} />
                    <Icon name='chevron-down-outline' onPress={props.onIconPress} color={color.g400} size={20} style={styles.icon} />
                </TouchableOpacity>
            </View>
            {
                props.subtitle ? <Text style={styles.subtitle}>{props.subtitle}</Text> : null
            }
            <Modal
                isVisible={modalVisible}
                style={{ margin: 0 }}
                animationInTiming={500}
                animationOutTiming={400}
                onBackdropPress={() => setModalVisible(false)}
                onBackButtonPress={() => setModalVisible(false)}
            >

                <View style={styles.wraperModal}>
                    <View style={[styles.header, style.shadow]}>
                        <Icon name='close-outline' size={32} onPress={() => setModalVisible(false)} />
                        <Text style={styles.textHeader}>{props.title}</Text>
                    </View>
                    <View style={{ padding: 10 }}>
                        <Input placeholder='Cari...' onChangeText={(v) => setKey(v)} />
                    </View>
                    <Divider />
                    <FlatList
                        data={props.data.filter(v => v.label.toLowerCase().match(key.toLowerCase()))}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={renderSelect}
                        onRefresh={props.onRefresh}
                        refreshing={props.refreshing}
                    />
                    <Button title='Pilih' disable={!selected} onPress={handleOnSelect} style={{ margin: 10, marginBottom: 10 }} />
                </View>
            </Modal>
        </>
    )
}

const styles = StyleSheet.create({
    wraper: {
        flexDirection: 'row',
        alignItems: 'center',
        borderColor: color.g300,
        borderBottomWidth: 1,
        marginVertical: 5,
        backgroundColor: '#fff',
        flex: 1
    },
    input: {
        flex: 1,
        borderRadius: 10,
        height: 40,
        color: color.g700,
    },
    icon: {
        padding: 10
    },
    label: {
        fontSize: 12,
        fontWeight: 'bold',
        color: color.g800,
        marginTop: 5
    },
    subtitle: {
        fontSize: 10,
        color: color.g500,
        marginLeft: 5
    },
    wraperModal: {
        flex: 1,
        backgroundColor: '#fff',
    },
    header: {
        height: 58,
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderColor: color.g300,
        padding: 10,
        paddingTop: 5,
        alignItems: 'center',
        flexDirection: 'row'
    },
    textHeader: {
        marginLeft: 20,
        fontSize: 16,
        fontWeight: 'bold',
    },
    renderBank: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 10,
        borderBottomWidth: 1,
        borderColor: color.g300,
        backgroundColor: '#fff'
    }
})

export default withNavigation(SelectSeacrh)