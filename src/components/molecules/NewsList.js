import { useLinkProps } from '@react-navigation/native';
import React from 'react';
import { Text, View, FlatList, Image, StyleSheet, Dimensions, Animated } from 'react-native';
import { color, style } from '_styles';

const NewsListVertical = (props) => {

    const renderItem = ({ item }) => {
        return (
            <View style={[styles.containerV, style.shadow]}>
                <Image source={item.img} style={styles.imgV} />
                <View style={styles.containerChildV}>
                    <Text style={styles.judulV} numberOfLines={2}>{item.judul}</Text>
                    <Text style={styles.titleV} numberOfLines={3}>{item.title}</Text>
                </View>
            </View >
        )
    }
    return (
        <View >
            <Text style={[styles.judulH, { paddingHorizontal: 10, fontSize: 18, paddingTop: 10 }]}>Berita Terbaru</Text>
            <Animated.FlatList
                data={props.data}
                keyExtractor={(item, index) => index.toString()}
                renderItem={renderItem}
            />
        </View>
    )

}

const NewsListHorizontal = (props) => {

    const renderItem = ({ item }) => {
        return (
            <View style={[styles.container, style.shadow]}>
                <View style={styles.containerChild}>
                    <Image source={item.img} style={styles.imgH} />
                    <Text style={styles.judulH} numberOfLines={2}>{item.judul}</Text>
                    <Text style={styles.titleH} numberOfLines={4}>{item.title}</Text>
                </View>
            </View >
        )
    }
    return (
        <View>
            <Text style={[styles.judulH, { paddingHorizontal: 10, fontSize: 18, paddingTop: 10 }]}>Berita Terpopuler</Text>
            <FlatList
                data={props.data}
                keyExtractor={(item, index) => index.toString()}
                renderItem={renderItem}
                horizontal
            />
        </View>
    )
}

const { width, height } = Dimensions.get('screen')

const styles = StyleSheet.create({
    container: {
        width: width / 3,
        margin: 5,
    },
    containerChild: {
        marginVertical: 2
    },
    imgH: {
        height: height / 10,
        width: width / 3
    },
    titleH: {
        width: width / 3,
        fontSize: 12,
        padding: 2
    },
    judulH: {
        fontSize: 14,
        fontWeight: 'bold',
        color: color.g800,
        padding: 2
    },
    containerV: {
        marginVertical: 10,
        flexDirection: 'row',
        marginHorizontal: 5,
    },
    containerChildV: {
        marginVertical: 2,
    },
    imgV: {
        height: height / 7.5,
        width: width / 3
    },
    titleV: {
        width: width / 1.7,
        marginLeft: 10
    },
    judulV: {
        fontSize: 15,
        fontWeight: 'bold',
        color: color.g800,
        marginLeft: 10,
        width: width / 1.7
    }
})

export {
    NewsListVertical,
    NewsListHorizontal
}