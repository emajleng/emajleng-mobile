import React from 'react';
import { View, Text, FlatList, Image, Dimensions, StyleSheet, TouchableHighlight } from 'react-native';
import { color, style } from '_styles';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import { withNavigation } from '@react-navigation/compat';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { autoCurency } from '_utils';


const ProductList = (props) => {

    const renderProduct = ({ item }) => {
        // console.log({ item })
        return (
            // <TouchableHighlight underlayColor='transparent' onPress={() => props.onPress} >
            <TouchableHighlight underlayColor='transparent' onPress={() => props.navigation.navigate('DetailProduct', { item })}>
                <View style={[styles.itemContainer, style.shadow]}>
                    {/* <Image source={{ uri: item.product_img_url ?? item.product_display }} style={styles.image} /> */}
                    <Image source={{ uri: item.photo }} style={[styles.image, { opacity: item.stock < 1 ? 0.6 : 2 }]} />
                    {
                        item.stock < 1 &&
                        <Image source={require('_assets/images/soldout.png')} style={[styles.image, { height: height / 11, width: width / 3, alignSelf: 'center', position: 'absolute', marginTop: 40 }]} resizeMode='center' />
                    }
                    <View style={styles.infoContainer}>
                        <Text style={{ color: color.g800, fontWeight: 'bold' }} numberOfLines={2}>{item.name}</Text>
                        <Text style={{ color: color.secondary }} numberOfLines={2}>{autoCurency(item.price)}</Text>
                    </View>
                    <Text style={{ color: color.failed, fontSize: 10, right: 0, position: 'absolute', bottom: 7, padding: 5 }} numberOfLines={2}>Stok: {item.stock}</Text>
                </View>
            </TouchableHighlight>
        )
    }

    const skeletonLoading = () => {
        return (
            <SkeletonContent
                containerStyle={styles.skeletonContainer}
                isLoading={true}
                layout={[styles.skeleton, styles.skeleton, styles.skeleton]}
            />
        )
    }

    return (
        <View style={props.containerStyle}>
            <View style={styles.flexRow}>
                <Text style={styles.title}>{props.title}</Text>
                <TouchableHighlight underlayColor='transparent' onPress={props.onSeeAll} >
                    <Text style={styles.moreText}>Lihat Semua</Text>
                </TouchableHighlight>
            </View>
            <FlatList
                data={props.data}
                keyExtractor={(item, index) => index.toString()}
                renderItem={renderProduct}
                ListEmptyComponent={skeletonLoading}
                showsHorizontalScrollIndicator={false}
                horizontal />
        </View>
    )
}

const { height, width } = Dimensions.get('screen')

const styles = StyleSheet.create({
    itemContainer: {
        width: width / 2.5,
        margin: 5,
        borderRadius: 5,
    },
    infoContainer: {
        padding: 10
    },
    image: {
        width: '100%',
        height: 150,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    },
    priceText: {
        fontWeight: 'bold',
        color: color.secondary,
        marginTop: 5
    },
    moreText: {
        fontWeight: 'bold',
        color: color.primary,
        marginRight: 15
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 15,
        marginBottom: 5,
    },
    flexRow: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    labelEmpty: {
        position: 'absolute',
        backgroundColor: 'rgba(244, 67, 54, 0.8)',
        padding: 10,
        width: 100,
        height: 100,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        top: 25,
    },
    emptyText: {
        fontWeight: 'bold',
        color: '#fff'
    },
    skeleton: {
        width: (width / 2.5),
        height: 220,
        margin: 7,
        marginVertical: 10
    },
    skeletonContainer: {
        flex: 1,
        flexDirection: 'row'
    },
})

export default withNavigation(ProductList)