import React from 'react';
import { View, StyleSheet, Text, StatusBar } from 'react-native';
import Modal from 'react-native-modal';
import { color, style } from '_styles';
import Icon from 'react-native-vector-icons/Ionicons';
import { useHeaderHeight } from '@react-navigation/stack';

const ModalScene = (props) => {

    const headerHeight = useHeaderHeight();

    return (
        <>
            <Modal
                isVisible={props.isVisible}
                style={{ margin: 0 }}
                swipeDirection='right'
                animationIn='slideInRight'
                animationOut='slideOutRight'
                hasBackdrop={false}
                onBackButtonPress={props.toggle}
                onSwipeComplete={props.toggle}
            >
                <View style={styles.wraper}>
                    <View style={[style.header, style.shadow, { height: headerHeight - StatusBar.currentHeight }]}>
                        <Icon name='close-outline' size={38} onPress={props.toggle} />
                        <Text style={styles.textHeader}>{props.title}</Text>
                    </View>
                    {props.children}
                </View>
            </Modal>
        </>
    )
}


const styles = StyleSheet.create({
    wraper: {
        flex: 1,
        backgroundColor: '#fff',
    },
    textHeader: {
        marginLeft: 20,
        fontSize: 16,
        fontWeight: 'bold',
    }
})

export default ModalScene;