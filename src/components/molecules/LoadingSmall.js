import React from 'react';
import { View, Text, Image, StyleSheet, Modal, Dimensions } from 'react-native';
import { color } from '_styles';
import { BallIndicator } from 'react-native-indicators';

const LoadingSmall = (props) => {
    return (
        <Modal visible={props.isLoading} style={{ margin: 0 }} transparent={true} animationType='fade' >
            <View style={styles.wrap}>
                <BallIndicator color={'#fff'} size={30} />
                <Text style={{alignSelf:'center', top:height /2 + 20, position:'absolute', color:color.g100}}>Mohon Tunggu ....</Text>
            </View>
        </Modal>
    )
}

const {height,width} = Dimensions.get('screen')
const styles = StyleSheet.create({
    wrap: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
})
export default LoadingSmall