import React from 'react';
import { View, TouchableHighlight, Dimensions, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { color } from '_styles';
import { withNavigation } from '@react-navigation/compat';

class Slider extends React.Component {
    constructor(props) {
        super(props),
            this.state = {
                activeIndex: 0,
            }
    }

    renderItem = ({ item }) => {
        return (
            <TouchableHighlight underlayColor='transparent' >
                <View style={styles.item}>
                    <Image
                        source={{ uri: item.image }}
                        // source={item.image}
                        style={styles.image}
                    />
                </View>
            </TouchableHighlight>
        );
    }

    render() {
        return (
            <>
                <View style={{ backgroundColor: color.bglight }}>
                    <Carousel
                        ref={(c) => { this.carousel = c; }}
                        data={this.props.slider}
                        loop
                        autoplay
                        autoplayInterval={5000}
                        sliderWidth={width}
                        itemWidth={width}
                        inactiveSlideOpacity={1}
                        inactiveSlideScale={1}
                        lockScrollWhileSnapping={true}
                        useScrollView={true}
                        renderItem={this.renderItem}
                        onSnapToItem={index => this.setState({ activeIndex: index })}
                    />
                    <View style={styles.pagination}>
                        <Pagination
                            dotsLength={slider.length}
                            activeDotIndex={this.state.activeIndex}
                            containerStyle={{ paddingVertical: 10, flex: 1, justifyContent: 'flex-start' }}
                            dotColor={color.primary}
                            inactiveDotScale={1}
                            inactiveDotColor={color.g300}
                        />
                    </View>
                </View>
            </>
        );
    }
}

const { width, height } = Dimensions.get('screen')


const styles = StyleSheet.create({
    item: {
        width: width,
        height: height / 4 + 70,
        backgroundColor: color.g200
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        height: height / 4 + 70,
        width: width
    },
    title: {
        fontSize: 16,
        margin: 20,
        marginBottom: 10,
        fontWeight: 'bold',
        color: color.g900
    },
    sliderTitle: {
        fontWeight: 'bold',
        fontSize: 12,
        color: color.g800,
        margin: 7
    },
    pagination: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    promoText: {
        fontWeight: 'bold',
        color: color.primary
    }
})


const slider = [
    {
        img: require('../../assets/images/benner1.png')
    },
    {
        img: require('../../assets/images/benner2.png')
    },
    {
        img: require('../../assets/images/benner3.png')
    },
    {
        img: require('../../assets/images/benner4.png')
    },
]

export default withNavigation(Slider);