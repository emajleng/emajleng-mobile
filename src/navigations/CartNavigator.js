import React from 'react';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import BuyProduct from '_scenes/product/BuyProduct';
import BuyMethod from '_scenes/product/BuyMethod';
import Invoices from '_scenes/product/Invoices';
import cart from '_scenes/cart';
import { color } from '_styles';

const Stack = createStackNavigator();

const CartNavigator = () => {
    return (
        <Stack.Navigator
            screenOptions={{
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                gestureDirection: 'horizontal'
            }}>
            <Stack.Screen name="Cart" options={{ title: 'Keranjang' }} component={cart} />
            <Stack.Screen name="BuyProduct" options={{ headerShown: false }} component={BuyProduct} />
            <Stack.Screen name="BuyMethod" options={{ headerShown: false }} component={BuyMethod} />
            <Stack.Screen name="Invoices" options={{ headerShown: false }} component={Invoices} />

        </Stack.Navigator>
    );
}

export default CartNavigator;