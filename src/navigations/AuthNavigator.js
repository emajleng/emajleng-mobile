import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '_scenes/auth/Login';
import home from '_scenes/home';
import Register from '_scenes/auth/Register';
import ForgotPassword from '_scenes/auth/ForgotPassword';
import PrivacyPollice from '_scenes/auth/PrivacyPollice';

const Stack = createStackNavigator();

const AuthNavigator = () => {
    return (
        <Stack.Navigator initialRouteName='Login' >
            <Stack.Screen options={{ headerShown: false }} name="Login" component={Login} />
            <Stack.Screen options={{ headerShown: false }} name="home" component={home} />
            <Stack.Screen options={{ headerShown: false }} name="Register" component={Register} />
            <Stack.Screen options={{ headerShown: false }} name="ForgotPassword" component={ForgotPassword} />
            <Stack.Screen options={{ headerShown: false }} name="PrivacyPollice" component={PrivacyPollice} />
        </Stack.Navigator>
    );
}

export default AuthNavigator;