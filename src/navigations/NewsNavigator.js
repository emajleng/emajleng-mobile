import React from 'react';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import news from '_scenes/news';
import NewsDetail from '_scenes/news/NewsDetail'
const Stack = createStackNavigator();

const NewsNavigator = () => {
    return (
        <Stack.Navigator
            screenOptions={{
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                gestureDirection: 'horizontal'
            }}>
            <Stack.Screen name="news" options={{ headerShown: false }} component={news} />
            <Stack.Screen name="NewsDetail" options={{ headerShown: false }} component={NewsDetail} />
        </Stack.Navigator>
    );
}

export default NewsNavigator;