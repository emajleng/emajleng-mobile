import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeStack from './HomeNavigator';
import AuthStack from './AuthNavigator';
import NewsStack from './NewsNavigator';
import CartStack from './CartNavigator';
import ProfileStack from './ProfileNavigator';
import Icon from 'react-native-vector-icons/Ionicons';
import { color, style } from '../styles';

const Tab = createBottomTabNavigator()

const AppNavigator = () => {

    const visibelTab = (route) => {
        let tabBarVisible = true;
        if (route.state && route.state.index > 0) {
            tabBarVisible = false;
        }
        return {
            tabBarVisible
        }
    }

    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName
                    if (route.name == 'Home') {
                        iconName = `home${focused ? '' : '-outline'}`
                    }
                    else if (route.name == 'Keranjang') {
                        iconName = `cart${focused ? '' : '-outline'}`
                    }
                    else if (route.name == 'Profile') {
                        iconName = `person-circle${focused ? '' : '-outline'}`
                    }
                    else if (route.name == 'News') {
                        iconName = `reader${focused ? '' : '-outline'}`
                    }
                    return <Icon name={iconName} size={24} color={color} style={{}} />
                }
            })}
            tabBarOptions={{
                activeTintColor: color.primary,
                inactiveTintColor: color.g600,
                showLabel: false,
                style: [style.shadow],
            }} >
            <Tab.Screen
                name='Home'
                component={HomeStack}
                options={({ route }) => { return visibelTab(route) }} />
            <Tab.Screen
                name='News'
                component={NewsStack}
                options={({ route }) => { return visibelTab(route) }}
            />
            <Tab.Screen
                name='Keranjang'
                component={CartStack}
                options={({ route }) => { return visibelTab(route) }} />
            <Tab.Screen
                name='Profile'
                component={ProfileStack}
                options={({ route }) => { return visibelTab(route) }} />
        </Tab.Navigator>
    )
}

export default AppNavigator