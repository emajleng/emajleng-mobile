import React from 'react';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import Login from '_scenes/auth/Login';
import Register from '_scenes/auth/Register';
import ForgotPassword from '_scenes/auth/ForgotPassword';
import PrivacyPollice from '_scenes/auth/PrivacyPollice';
import home from '_scenes/home';
import SearchScreen from '_scenes/home/SearchScreen';
import DetailProduct from '_scenes/product/DetailProduct';
import BuyProduct from '_scenes/product/BuyProduct';
import BuyMethod from '_scenes/product/BuyMethod';
import Invoices from '_scenes/product/Invoices';
import TopUp from '_scenes/profile/TopUp';
import Withdraw from '_scenes/profile/Withdraw';
import TopUpInvoice from '_scenes/profile/TopUpInvoice';
import Pulsa from '_scenes/PPOB/Pulsa';
import Checkout from '_scenes/transaction/Checkout';
import Invoice from '_scenes/transaction/Invoice';
import InvoicePln from '_scenes/transaction/InvoicePln';
import Listrik from '_scenes/PPOB/Listrik';
import CheckoutPostpaid from '_scenes/transaction/CheckoutPostpaid';
import UserBank from '_scenes/profile/UserBank';
import AddUserBank from '_scenes/profile/AddUserBank';
import AllProductCategories from '_scenes/product/AllProductCategories';
import chat from '_scenes/chat';
import chatRoom from '_scenes/chat/chatRoom';
import shopProduct from '_scenes/product/shopProduct';

import { color } from '_styles'


const Stack = createStackNavigator();

const HomeNavigator = () => {
    return (
        <Stack.Navigator
            screenOptions={{
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                gestureDirection: 'horizontal'
            }}>
            <Stack.Screen name="home" options={{ headerShown: false }} component={home} />
            <Stack.Screen name="SearchScreen" options={{ headerShown: false }} component={SearchScreen} />
            <Stack.Screen name="Login" options={{ headerShown: false }} component={Login} />
            <Stack.Screen options={{ headerShown: false }} name="Register" component={Register} />
            <Stack.Screen options={{ headerShown: false }} name="ForgotPassword" component={ForgotPassword} />
            <Stack.Screen options={{ headerShown: false }} name="PrivacyPollice" component={PrivacyPollice} />
            <Stack.Screen name="DetailProduct" options={{ headerShown: false }} component={DetailProduct} />
            <Stack.Screen name="BuyProduct" options={{ headerShown: false }} component={BuyProduct} />
            <Stack.Screen name="BuyMethod" options={{ headerShown: false }} component={BuyMethod} />
            <Stack.Screen name="Invoices" options={{ headerShown: false }} component={Invoices} />
            <Stack.Screen name="TopUp" options={{ title: 'Isi Saldo' }} component={TopUp} />
            <Stack.Screen name="Withdraw" options={{ title: 'Tarik Saldo' }} component={Withdraw} />
            <Stack.Screen name="TopUpInvoice" options={{ title: 'Cara Pembayaran', headerLeft: null }} component={TopUpInvoice} />
            <Stack.Screen name="PULSA" options={{ title: 'Pulsa' }} component={Pulsa} />
            <Stack.Screen name="Checkout" options={{ title: 'Transaksi' }} component={Checkout} />
            <Stack.Screen name="Invoice" options={{ title: 'Transaksi' }} component={Invoice} />
            <Stack.Screen name="InvoicePln" options={{ title: 'Transaksi' }} component={InvoicePln} />
            <Stack.Screen name="Listrik" options={{ title: 'Pembayaran Listrik' }} component={Listrik} />
            <Stack.Screen name="CheckoutPostpaid" options={{ title: 'Pembayaran Tagihan Listrik' }} component={CheckoutPostpaid} />
            <Stack.Screen name="UserBank" options={{ title: 'Daftar Bank', headerStyle: { backgroundColor: color.primary }, headerTintColor: '#fff' }} component={UserBank} />
            <Stack.Screen name="AddUserBank" options={{ title: 'Tambah Bank', headerStyle: { backgroundColor: color.primary }, headerTintColor: '#fff' }} component={AddUserBank} />
            <Stack.Screen name="AllProductCategories" options={{ headerShown: false }} component={AllProductCategories} />
            <Stack.Screen name="chat" options={{ title: 'Daftar Pesan' }} component={chat} />
            <Stack.Screen name='chatRoom' options={({ route }) => ({ title: route.params.name })} component={chatRoom} />
            <Stack.Screen name="shopProduct" options={{ title: 'Profil Toko' }} component={shopProduct} />

        </Stack.Navigator>
    );
}

export default HomeNavigator;