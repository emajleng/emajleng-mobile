import React, { useRef } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import AppNavigator from './AppNavigator';
import AuthNavigator from './AuthNavigator';
import PublicNavigator from './PublicNavigator';
import AuthLoading from '_scenes/auth/AuthLoading';
import { connect } from 'react-redux';
// import analytics from '@react-native-firebase/analytics';

const Stack = createStackNavigator();

AppContainer = (props) => {

    const routeNameRef = useRef();
    const navigationRef = useRef();

    if (props.isLoading) {
        return (
            <NavigationContainer>
                <Stack.Navigator
                    screenOptions={{
                        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
                    }}>
                    <Stack.Screen options={{ headerShown: false }} name="MiddleWare" component={AuthLoading} />
                </Stack.Navigator>
            </NavigationContainer>
        )
    }

    return (
        <NavigationContainer
            >
            <Stack.Navigator
                screenOptions={{
                    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
                }}>
                {
                    props.token ?
                        <Stack.Screen options={{ headerShown: false, gestureDirection: 'horizontal' }} name="Home" component={AppNavigator} /> :
                        <Stack.Screen options={{ headerShown: false, gestureDirection: 'horizontal' }} name="Public" component={PublicNavigator} />
                }
                <Stack.Screen options={{ headerShown: false, gestureDirection: 'horizontal' }} name="AuthLoading" component={AuthLoading} />
                {/* {props.token ? <AppNavigator /> : <AuthNavigator />} */}
            </Stack.Navigator>
        </NavigationContainer >
    );
}

const mapStateToProps = state => {
    return {
        user: state.user.data,
        isLoading: state.user.isLoading,
        token: state.user.token,
    }
}

export default connect(mapStateToProps)(AppContainer);