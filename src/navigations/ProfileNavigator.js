import React from 'react';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import profile from '_scenes/profile';
import UserProfile from '_scenes/profile/UserProfile';
import UpdateProfile from '_scenes/profile/UpdateProfile';
import product from '_scenes/product';
import AddProduct from '_scenes/product/AddProduct';
import EditProduct from '_scenes/product/EditProduct';
import MerchantProfile from '_scenes/profile/MerchantProfile';
import UserBank from '_scenes/profile/UserBank';
import AddUserBank from '_scenes/profile/AddUserBank';
import { color } from '_styles'
import UpdateMerchant from '_scenes/profile/UpdateMerchant';
import AddOriginAddress from '_scenes/profile/AddOriginAddress';
import TransactionListUser from '_scenes/transaction/TransactionListUser';
import TransactionListMerchant from '_scenes/transaction/TransactionListMerchant';
import InvoiceBuying from '_scenes/transaction/InvoiceBuying';
import Confirmation from '_scenes/transaction/Confirmation';
import DetailsTlUser from '_scenes/transaction/DetailsTlUser';
import DetailTlMerchant from '_scenes/transaction/DetailTlMerchant';
import AirwayBill from '_scenes/transaction/AirwayBill';
import UpdateOriginAddress from '_scenes/profile/UpdateOriginAddress';
import PrivacyPollice from '_scenes/auth/PrivacyPollice';
import chatRoom from '_scenes/chat/chatRoom';

const Stack = createStackNavigator();

const NewsNavigator = () => {
    return (
        <Stack.Navigator
            screenOptions={{
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                gestureDirection: 'horizontal'
            }}>
            <Stack.Screen name="profile" options={{ headerShown: false }} component={profile} />
            <Stack.Screen name="UserProfile" options={{ headerShown: false }} component={UserProfile} />
            <Stack.Screen name="UpdateProfile" options={{ headerShown: false }} component={UpdateProfile} />
            <Stack.Screen name="product" options={{ headerShown: false }} component={product} />
            <Stack.Screen name="UserBank" options={{ title: 'Daftar Bank', headerStyle: { backgroundColor: color.primary }, headerTintColor: '#fff' }} component={UserBank} />
            <Stack.Screen name="AddUserBank" options={{ title: 'Tambah Bank', headerStyle: { backgroundColor: color.primary }, headerTintColor: '#fff' }} component={AddUserBank} />
            <Stack.Screen name="AddProduct" options={{ headerShown: false }} component={AddProduct} />
            <Stack.Screen name="EditProduct" options={{ headerShown: false }} component={EditProduct} />
            <Stack.Screen name="MerchantProfile" options={{ headerShown: false }} component={MerchantProfile} />
            <Stack.Screen name="UpdateMerchant" options={{ headerShown: false }} component={UpdateMerchant} />
            <Stack.Screen name="AddOriginAddress" options={{ headerShown: false }} component={AddOriginAddress} />
            <Stack.Screen name="TransactionListUser" options={{ headerShown: false }} component={TransactionListUser} />
            <Stack.Screen name="TransactionListMerchant" options={{ headerShown: false }} component={TransactionListMerchant} />
            <Stack.Screen name="InvoiceBuying" options={{ headerShown: false }} component={InvoiceBuying} />
            <Stack.Screen name="Confirmation" options={{ headerShown: false }} component={Confirmation} />
            <Stack.Screen name="DetailsTlUser" options={{ headerShown: false }} component={DetailsTlUser} />
            <Stack.Screen name="DetailTlMerchant" options={{ headerShown: false }} component={DetailTlMerchant} />
            <Stack.Screen name="AirwayBill" options={{ headerShown: false }} component={AirwayBill} />
            <Stack.Screen name="UpdateOriginAddress" options={{ headerShown: false }} component={UpdateOriginAddress} />
            <Stack.Screen name="PrivacyPollice" component={PrivacyPollice} />
            <Stack.Screen name='chatRoom' options={{ title: 'Pesan' }} component={chatRoom} />

        </Stack.Navigator>
    );
}

export default NewsNavigator;