import React from 'react';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import product from '_scenes/product';
import AddProduct from '_scenes/product/AddProduct';
import EditProduct from '_scenes/product/EditProduct';
import BuyProduct from '_scenes/product/BuyProduct';
import Invoices from '_scenes/product/Invoices';
import chatRoom from '_scenes/chat/chatRoom';
import shopProduct from '_scenes/product/shopProduct';
const Stack = createStackNavigator();

const ProductNavigator = () => {
    return (
        <Stack.Navigator
            screenOptions={{
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                gestureDirection: 'horizontal'
            }}>
            <Stack.Screen name="product" options={{ headerShown: false }} component={product} />
            <Stack.Screen name="AddProduct" options={{ headerShown: false }} component={AddProduct} />
            <Stack.Screen name="EditProduct" options={{ headerShown: false }} component={EditProduct} />
            <Stack.Screen name="BuyProduct" options={{ headerShown: false }} component={BuyProduct} />
            <Stack.Screen name="Invoices" options={{ headerShown: false }} component={Invoices} />
            <Stack.Screen name='chatRoom' options={({ route }) => ({ title: route.params.name })} component={chatRoom} />
            <Stack.Screen name="shopProduct" options={{ titlae: 'Profil Toko' }} component={shopProduct} />

        </Stack.Navigator>

    );
}

export default ProductNavigator;