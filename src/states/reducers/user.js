let initialState = {
    data: {},
    merchant: {},
    provider: [],
    isLoading: true,
    token: null
}

export default user = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_USER_FULFILLED':
            return {
                ...state,
                data: action.payload.data.data,
            }
        case 'SAVE_USER':
            return {
                ...state,
                data: action.payload,
            }
        case 'GET_USER_REJECTED':
            return {
                ...state,
                isAuthLoading: false,
                isLoading: false,
            }
        case 'GET_MERCHANT_FULFILLED':
            return {
                ...state,
                merchant: action.payload.data.data,
            }
        case 'GET_MERCHANT_REJECTED':
            return {
                ...state,
                isAuthLoading: false,
                isLoading: false,
            }
        case 'SET_LOADING':
            return {
                ...state,
                isLoading: action.payload,
            }
        case 'SAVE_TOKEN':
            return {
                ...state,
                token: action.payload,
                isLoading: false
            }
        case 'SAVE_PROVIDER':
            return {
                ...state,
                provider: action.payload
            }
        default:
            return state
    }
}