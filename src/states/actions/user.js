import axios from 'axios';

export const getUser = () => {
    return {
        type: 'GET_USER',
        payload: axios.get(`/user`),
    }
}

export const saveUser = (data) => {
    return {
        type: 'SAVE_USER',
        payload: data,
    }
}
export const getMerchant = () => {
    return {
        type: 'GET_MERCHANT',
        payload: axios.get(`/user/merchant`),
    }
}

export const saveProvider = (data) => {
    return {
        type: 'SAVE_PROVIDER',
        payload: data,
    }
}

export const setLoading = (data) => {
    return {
        type: 'SET_LOADING',
        payload: data
    }
}

export const saveToken = (data) => {
    return {
        type: 'SAVE_TOKEN',
        payload: data
    }
}