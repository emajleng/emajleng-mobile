import React from 'react';
import { StatusBar } from 'react-native'
import AppContainer from '_navigations';
import { Provider } from 'react-redux';
import store from '_states/store';
import 'react-native-gesture-handler';
import 'intl';
import 'intl/locale-data/jsonp/id-ID'
import PushNotificationService from '_utils/PushNotificationService';
const App: () => React$Node = () => {

    return (
        <>
            <Provider store={store}>
                <StatusBar backgroundColor={'#fff'} barStyle='dark-content' />
                <AppContainer />
                <PushNotificationService />
            </Provider>
        </>
    );
};

export default App;